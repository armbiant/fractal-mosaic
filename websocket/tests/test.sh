#!/bin/bash

export DOCKER_COMPOSE="docker-compose --env-file test.env"

# rebuild integration tests
$DOCKER_COMPOSE build

# start stack
$DOCKER_COMPOSE up -d

# start integration tests, save error state
sleep 1
$DOCKER_COMPOSE exec -T test pytest --quiet test.py --asyncio-mode=auto
export RETVAL=$?

# show logs on error
if [ $RETVAL -ne 0 ]; then
    $DOCKER_COMPOSE logs
fi

# stop stack
$DOCKER_COMPOSE down

# return retval
exit $RETVAL
