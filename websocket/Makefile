IMAGE_TAG=local
DOCKER=docker
SHELL=bash
CONTAINER=registry.gitlab.com/fractalnetworks/hive-websocket
BUILD_TYPE=debug
CARGO=cargo

docker-build: target/$(BUILD_TYPE)/hive-websocket
	${DOCKER} build . -t ${CONTAINER}:${IMAGE_TAG} --build-arg BUILD_TYPE=$(BUILD_TYPE)

docker-push: target/release/hive-websocket
	${DOCKER} push ${CONTAINER}:${IMAGE_TAG}

target/debug/hive-websocket:
	$(CARGO) build

target/release/hive-websocket:
	$(CARGO) build --release

# run integration tests, automated
test:
	cargo test
	cargo build --release
	docker build . -t ${CONTAINER}:local
	cd tests && bash test.sh

# manually start integration test stack
test-start:
	cd tests && docker-compose --env-file test.env up

# manually stop integration test stack
test-stop:
	cd tests && docker-compose --env-file test.env down

# listen for events from docker to debug tests
test-events:
	cd tests && docker-compose --env-file test.env events

# manually run integration tests
test-run:
	cd tests && docker-compose --env-file test.env exec test pytest --quiet test.py --asyncio-mode=auto

.PHONY: test target/debug/hive-websocket target/release/hive-websocket
