//! # Hive Websocket
//!
//! Hive uses RabbitMQ as the message broker. This service exposes a Websocket that devices can
//! connect to to receive and send messages.
#![deny(missing_docs)]
use anyhow::Result;
use clap::Parser;
use fractal_auth_client::{key_store, AuthConfig, StaticToken};
use lapin::{Connection, ConnectionProperties, ExchangeKind};
use log::*;
use regex::Regex;
use reqwest::Client;
use std::{net::SocketAddr, sync::Arc};
use tokio::net::TcpListener;
use url::Url;

mod auth;
mod connection;
mod health;

/// Subcommands for the command-line interface
#[derive(Parser, Clone, Debug)]
pub enum Command {
    /// Run service
    Run(Options),
    /// Perform health check
    Health(Health),
}

/// Health check, attempt to connect to Websocket.
#[derive(Parser, Clone, Debug)]
pub struct Health {
    /// Address to try connecting to.
    address: Url,
}

/// Command-line options.
#[derive(Parser, Clone, Debug)]
pub struct Options {
    /// Address to listen on for incoming WebSocket connections.
    #[clap(
        long,
        short,
        env = "HIVE_WEBSOCKET_LISTEN",
        default_value = "0.0.0.0:8000"
    )]
    listen: SocketAddr,

    /// Keepalive interval. Sent to client to keep connection open.
    #[clap(
        long,
        short = 't',
        env = "HIVE_WEBSOCKET_KEEPALIVE",
        default_value = "30"
    )]
    interval: u64,

    /// AMQP server URL.
    #[clap(long, env = "HIVE_WEBSOCKET_AMQP", default_value = "amqp://localhost")]
    amqp: Url,

    /// AMQP exchange
    #[clap(long, env = "HIVE_WEBSOCKET_AMQP_EXCHANGE", default_value = "events")]
    amqp_exchange: String,

    /// JWT used to make requests to other APIs.
    ///
    /// This JWT is used in the Authorization header as a bearer token when contacting the
    /// authentication API that is used to validate device tokens and get the account and
    /// device UUIDs.
    #[clap(long, short, env = "HIVE_WEBSOCKET_JWT")]
    jwt: String,

    /// Endpoint to validate authentication tokens.
    #[clap(long, env = "HIVE_WEBSOCKET_AUTH")]
    auth: Url,

    /// List of allowed message types.
    #[clap(
        long,
        env = "HIVE_WEBSOCKET_ALLOW",
        use_delimiter = true,
        default_value = ".*"
    )]
    allow: Vec<Regex>,

    /// Disable device authentication (for development purposes).
    ///
    /// When set, an authentication token of the form
    /// c896f705-0d72-42a1-a32c-af210ec0086a,8abd655c-9f1b-4862-a585-270343a31d3b will
    /// be considered to be a valid device token for the account
    /// c896f705-0d72-42a1-a32c-af210ec0086a and the device
    /// 8abd655c-9f1b-4862-a585-270343a31d3b. When enabled, no requests to the API
    /// will be made to verify tokens.
    #[cfg(feature = "insecure-auth-stub")]
    #[clap(long)]
    insecure_auth_stub: bool,

    /// Where to fetch JWKS from (used to validate JWTs).
    #[clap(long, env = "HIVE_WEBSOCKET_JWKS")]
    pub jwks: Option<Url>,

    /// Adds a static user token. Supply it in the format `token:uuid`.
    #[clap(long, env = "HIVE_WEBSOCKET_STATIC_USER", use_delimiter = true)]
    pub static_user: Vec<StaticToken>,

    /// Adds a static system token. Supply it in the format `token:uuid`.
    #[clap(long, env = "HIVE_WEBSOCKET_STATIC_SYSTEM", use_delimiter = true)]
    pub static_system: Vec<StaticToken>,
}

impl Options {
    /// Generate an AuthConfig from the command-line options
    pub async fn auth_config(&self) -> Result<AuthConfig> {
        let mut auth_config = AuthConfig::new();

        if let Some(jwks) = &self.jwks {
            let key_store = key_store(&jwks.to_string()).await?;
            auth_config = auth_config.with_keystore(key_store);
        }

        #[cfg(feature = "insecure-auth-stub")]
        if self.insecure_auth_stub {
            auth_config = auth_config.with_insecure_stub(self.insecure_auth_stub);
            error!("Disabling authentication verification because of the auth-insecure-stub flag");
        }

        for user in &self.static_user {
            info!("Adding static user token for {}", user.account);
            auth_config.add_static_user(&user.token, &user.account);
        }

        for system in &self.static_system {
            info!("Adding static system token for {}", system.account);
            auth_config.add_static_system(&system.token, &system.account);
        }

        Ok(auth_config)
    }

    /// Run Hive Websocket
    pub async fn run(&self) -> Result<()> {
        #[cfg(feature = "insecure-auth-stub")]
        if self.insecure_auth_stub {
            error!("Authentication is disabled because insecure-auth-stub is enabled");
        }

        let global = Global {
            auth_config: self.auth_config().await?,
            options: Arc::new(self.clone()),
            client: Client::new(),
        };

        let connection = Connection::connect(
            &global.options.amqp.to_string(),
            ConnectionProperties::default()
                .with_executor(tokio_executor_trait::Tokio::current())
                .with_reactor(tokio_reactor_trait::Tokio),
        )
        .await?;

        let socket = TcpListener::bind(&global.options.listen).await?;

        info!("Opened TCP socket for incoming WebSocket connections");

        loop {
            let conn = socket.accept().await;
            match conn {
                Ok((stream, addr)) => {
                    let global = global.clone();
                    let channel = connection.create_channel().await?;
                    channel
                        .exchange_declare(
                            &global.options.amqp_exchange,
                            ExchangeKind::Headers,
                            Default::default(),
                            Default::default(),
                        )
                        .await?;
                    tokio::spawn(async move {
                        match connection::handle_connection(global, stream, addr, channel).await {
                            Ok(()) => {}
                            Err(e) => error!("Error: {:#}", e),
                        }
                    });
                }
                Err(error) => {
                    error!("Error accepting socket: {:?}", error);
                }
            }
        }
    }
}

/// Global data
#[derive(Clone, Debug)]
pub struct Global {
    /// Command-line options
    pub options: Arc<Options>,
    /// Auth configuration
    pub auth_config: AuthConfig,
    /// HTTP Client
    pub client: Client,
}

#[tokio::main]
async fn main() -> Result<()> {
    env_logger::init();
    match Command::from_args() {
        Command::Health(health) => health::check(&health).await,
        Command::Run(options) => options.run().await,
    }
}
