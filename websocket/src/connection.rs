//! # Connection handler module
//!
//! Handler for connections.
use crate::{Global, Options};
use amqp_value_json::{ToAmqp, ToJson};
use anyhow::{anyhow, Context as AnyhowContext, Result};
use async_tungstenite::tokio::{accept_hdr_async, TokioAdapter};
use async_tungstenite::tungstenite::handshake::server::{
    Callback, ErrorResponse, Request, Response,
};
use async_tungstenite::tungstenite::protocol::Message;
use async_tungstenite::WebSocketStream;
use fractal_auth_client::UserContext;
use futures::{select, FutureExt, SinkExt, StreamExt};
use lapin::{
    message::Delivery,
    options::*,
    types::{AMQPValue, FieldTable},
    BasicProperties, Channel,
};
use log::*;
use serde_json::{Map, Value};
use std::{
    net::SocketAddr,
    result::Result as StdResult,
    sync::{Arc, Mutex},
};
use thiserror::Error as ThisError;
use tokio::net::TcpStream;
use tokio::time::{interval, Duration};
use uuid::Uuid;

const RABBITMQ_QUEUE_LENGTH: u16 = 1000;
const WEBSOCKET_SEND_TIMEOUT: Duration = Duration::from_secs(10);
const READ_TOKEN_TIMEOUT: Duration = Duration::from_secs(10);

/// Errors the can be generated for Websocket connections.
#[derive(Clone, Debug, ThisError)]
pub enum Error {
    #[error("Invalid device message type: {0:}")]
    InvalidDeviceMessageType(String),
    #[error("Missing device message type")]
    MissingDeviceMessageType,
    #[error("Got invalid JSON from device")]
    InvalidDeviceMessageJson,
}

/// The TokenExtractor gets a callback whenever an incoming connection is accepted
/// containing the HTTP headers associated with the request. From the headers,
/// it pulls the JWT, validates it, and depending on the result of the validation,
/// accepts the connection or not. When the connection is accepted, it pulls the
/// UUID of the users from the JWT to use in subrequests to other APIs.
#[derive(Clone, Debug)]
struct TokenExtractor {
    context: Arc<Mutex<Context>>,
}

impl TokenExtractor {
    /// Create new token extractor.
    pub fn new() -> Self {
        TokenExtractor {
            context: Arc::new(Mutex::new(Default::default())),
        }
    }

    /// Get JWT or ApiKey, if exists.
    pub fn context(&self) -> Context {
        self.context.lock().unwrap().clone()
    }
}

#[derive(Clone, Copy, Debug)]
pub enum ClientType {
    Events,
    Device,
}

impl Default for ClientType {
    fn default() -> Self {
        ClientType::Device
    }
}

/// Request Context
///
/// This is information pulled from the request, such as the authentication token
/// and the client type (events or device).
#[derive(Clone, Debug, Default)]
struct Context {
    client: ClientType,
    token: Option<String>,
}

impl Callback for TokenExtractor {
    fn on_request(
        self,
        request: &Request,
        response: Response,
    ) -> StdResult<Response, ErrorResponse> {
        let mut context = self.context.lock().unwrap();

        // check if the client is trying to access the read-only events
        // stream rather than being a hive device.
        if request.uri().path().ends_with("/events") {
            context.client = ClientType::Events;
        }

        // fetch request headers
        let headers = request.headers();
        let auth_header = headers
            .get("Authorization")
            .and_then(|h| h.to_str().ok())
            .and_then(|h| h.split(" ").skip(1).nth(0));

        // If there was an authentication token, save it.
        if let Some(auth_header) = auth_header {
            context.token = Some(auth_header.to_string());
        }

        Ok(response)
    }
}

/// Handler for incoming Websocket connection.
pub async fn handle_connection(
    global: Global,
    stream: TcpStream,
    addr: SocketAddr,
    channel: Channel,
) -> Result<()> {
    info!("Handling connection from {}", addr);

    // accept websocket connection, extract authorization header
    let guard = TokenExtractor::new();
    let stream = accept_hdr_async(stream, guard.clone()).await?;

    // try to pull JWT/ApiKey from header, otherwise pull it from
    // websocket.
    let context = guard.context();

    match context.client {
        ClientType::Events => {
            handle_events_connection(global, stream, addr, channel, context).await
        }
        ClientType::Device => {
            handle_device_connection(global, stream, addr, channel, context).await
        }
    }
}

/// Handler for incoming events Websocket connections.
async fn handle_events_connection(
    global: Global,
    mut stream: WebSocketStream<TokioAdapter<TcpStream>>,
    addr: SocketAddr,
    channel: Channel,
    context: Context,
) -> Result<()> {
    let token = if let Some(token) = context.token {
        token
    } else {
        tokio::time::timeout(READ_TOKEN_TIMEOUT, read_token(&mut stream))
            .await
            .context("Waiting for device token")??
    };

    let context = UserContext::from_token(&global.auth_config, &token, addr.ip()).await?;
    let account = context.account();
    let id = Uuid::new_v4();
    info!("Authenticated events user {account}");

    // subscribe to messages for this device. this will create an exclusive, temporary
    // (auto-deleted) queue with a length limit.
    let mut tags = FieldTable::default();
    tags.insert(
        "x-max-length".into(),
        AMQPValue::ShortUInt(RABBITMQ_QUEUE_LENGTH),
    );
    tags.insert(
        "x-overflow".into(),
        AMQPValue::LongString("drop-head".to_string().into()),
    );
    let queue = channel
        .queue_declare(
            &format!("events-acc:{account}-id:{id}"),
            QueueDeclareOptions {
                exclusive: true,
                auto_delete: true,
                ..Default::default()
            },
            tags,
        )
        .await?;

    // create filter that will only allow messages for this account.
    let mut tags = FieldTable::default();
    tags.insert("x-match".into(), AMQPValue::LongString("all".into()));
    tags.insert(
        "account".into(),
        AMQPValue::LongString(context.account().to_string().into()),
    );

    // bind the queue to this channel
    channel
        .queue_bind(
            queue.name().as_str(),
            &global.options.amqp_exchange,
            "#",
            Default::default(),
            tags,
        )
        .await?;

    // create a consumer that lets us consume messages for this queue
    let mut consumer = channel
        .basic_consume(
            queue.name().as_str(),
            "hive-websocket-events",
            BasicConsumeOptions::default(),
            FieldTable::default(),
        )
        .await?;

    // wait for new events from rabbitmq or messages from the websocket and
    // handle them. if either of the connections fail, both are closed down.
    loop {
        select! {
            event = consumer.next().fuse() => {
                let event = event.ok_or(anyhow!("RabbitMQ closed stream"))??;
                publish_event(&event, &mut stream, &addr).await?;
            },
            message = stream.next().fuse() => {
                let message = message.ok_or(anyhow!("Websocket closed connection"))??;
                debug!("Got message: {message:?}");
            },
        }
    }
}

async fn read_token(stream: &mut WebSocketStream<TokioAdapter<TcpStream>>) -> Result<String> {
    loop {
        let message = stream
            .next()
            .await
            .ok_or(anyhow!("Stream closed while waiting for token"))??;
        match message {
            Message::Text(token) => return Ok(token),
            _ => continue,
        }
    }
}

/// Handler for incoming device Websocket connections.
async fn handle_device_connection(
    global: Global,
    mut stream: WebSocketStream<TokioAdapter<TcpStream>>,
    addr: SocketAddr,
    channel: Channel,
    context: Context,
) -> Result<()> {
    debug!("Got connection: {context:?}");
    let token = context.token.ok_or(anyhow!("Missing JWT or ApiKey"))?;

    // validate context, close connection if necessary.
    let context = global
        .options
        .check_device_token(&global.client, &token)
        .await?;

    let account = context.user();
    let device = context.device();

    info!(
        "Accepted connection from {} as {} device {}",
        addr, &account, &device
    );

    // subscribe to messages for this device
    let mut tags = FieldTable::default();
    tags.insert(
        "x-max-length".into(),
        AMQPValue::ShortUInt(RABBITMQ_QUEUE_LENGTH),
    );
    tags.insert(
        "x-overflow".into(),
        AMQPValue::LongString("drop-head".to_string().into()),
    );
    let queue = channel
        .queue_declare(
            &format!("device-acc:{account}-dev:{device}"),
            QueueDeclareOptions {
                exclusive: true,
                auto_delete: true,
                ..Default::default()
            },
            tags,
        )
        .await?;

    let mut tags = FieldTable::default();
    tags.insert("x-match".into(), AMQPValue::LongString("all".into()));
    tags.insert(
        "account".into(),
        AMQPValue::LongString(context.user().to_string().into()),
    );
    tags.insert(
        "device".into(),
        AMQPValue::LongString(context.device().to_string().into()),
    );

    channel
        .queue_bind(
            queue.name().as_str(),
            &global.options.amqp_exchange,
            "#",
            Default::default(),
            tags,
        )
        .await?;

    let mut consumer = channel
        .basic_consume(
            queue.name().as_str(),
            "hive-websocket-device",
            BasicConsumeOptions::default(),
            FieldTable::default(),
        )
        .await?;

    let mut interval = interval(Duration::from_secs(global.options.interval));

    // Main event loop for client connections. This will send a Ping message
    // every `options.interval` seconds to keep the connection alive, publish
    // an event when we get one from AMQP, and print out any messages received
    // on the WebSocket to the log for debugging purposes.
    loop {
        select! {
            _ = interval.tick().fuse() => {
                stream.send(Message::Ping(vec![])).await?;
            },
            event = stream.next().fuse() => {
                if let Some(event) = event {
                    debug!("Got some message: {:?}", event);
                    let event = event?;
                    publish_response(&global.options, &channel, event, &context.user(), &context.device(), &addr).await?;
                } else {
                    break;
                }
            }
            event = consumer.next().fuse() => {
                debug!("Got event for {}", addr);
                match event {
                    Some(Ok(event)) => publish_event(&event, &mut stream, &addr).await?,
                    _ => break,
                }
            },
        };
    }

    Ok(())
}

/// Check if the message type is allowed.
pub fn check_allowed_message_type(
    options: &Options,
    map: &Map<String, Value>,
) -> Result<(), Error> {
    match map.get("type") {
        Some(Value::String(typ)) => {
            if options.allow.iter().any(|p| p.is_match(&typ)) {
                Ok(())
            } else {
                Err(Error::InvalidDeviceMessageType(typ.clone()))
            }
        }
        _ => Err(Error::MissingDeviceMessageType),
    }
}

/// Insert some message fields that are overriden for messages from the device.
pub fn insert_message_fields(
    map: &mut Map<String, Value>,
    account: &Uuid,
    device: &Uuid,
    addr: &SocketAddr,
) {
    map.insert("account".to_string(), account.to_string().into());
    map.insert("device".to_string(), device.to_string().into());
    map.insert("service".to_string(), "hive-device".to_string().into());
    map.insert("address".to_string(), addr.to_string().into());
}

/// Send the device alive message in response to getting a pong message.
async fn send_alive_message(
    options: &Options,
    channel: &Channel,
    account: &Uuid,
    device: &Uuid,
    addr: &SocketAddr,
) -> Result<()> {
    let mut map = Map::new();
    insert_message_fields(&mut map, account, device, addr);
    map.insert("service".to_string(), "hive-websocket".to_string().into());
    map.insert("type".to_string(), "device-alive".to_string().into());
    send_json_message(options, channel, &Value::Object(map)).await?;
    Ok(())
}

/// Convert and send a JSON message.
async fn send_json_message(options: &Options, channel: &Channel, value: &Value) -> Result<()> {
    // convert the json to an AMQP value. this is what we will later use at the header
    // of the AMQP message.
    let message = value.to_amqp_value()?;

    // we already know that this JSON value is an object, so it will be turned into an
    // AMQP table. we extract that AMQP table (called a FieldTable).
    let header = match message {
        AMQPValue::FieldTable(table) => table,
        _ => Err(Error::InvalidDeviceMessageJson)?,
    };

    // construct a new AMQP message properties from the header.
    let properties = BasicProperties::default().with_headers(header);

    // publish a message to AMQP
    channel
        .basic_publish(
            &options.amqp_exchange,
            "",
            Default::default(),
            &[],
            properties,
        )
        .await?;

    Ok(())
}

/// Publish response to request
pub async fn publish_response(
    options: &Options,
    channel: &Channel,
    message: Message,
    account: &Uuid,
    device: &Uuid,
    addr: &SocketAddr,
) -> Result<()> {
    let message = match message {
        Message::Text(text) => text,
        Message::Pong(_) => {
            return send_alive_message(options, channel, account, device, addr).await
        }
        _ => return Ok(()),
    };

    // parse message (a string) into a generic JSON value. this can be anything.
    let mut json: Value = serde_json::from_str(&message)?;

    // peek inside the value: if it is an object, validate some fields (type field),
    // and set some fields (account and device UUID). if not, return an error, which
    // will close the connection.
    match &mut json {
        Value::Object(object) => {
            check_allowed_message_type(options, &object)?;
            insert_message_fields(object, account, device, addr);
        }
        _ => Err(Error::InvalidDeviceMessageJson)?,
    }

    send_json_message(options, channel, &json).await?;

    Ok(())
}

/// Publish event
pub async fn publish_event(
    event: &Delivery,
    stream: &mut WebSocketStream<TokioAdapter<TcpStream>>,
    addr: &SocketAddr,
) -> Result<()> {
    let headers = event
        .properties
        .headers()
        .as_ref()
        .ok_or(anyhow!("Missing headers"))?;
    let headers = AMQPValue::FieldTable(headers.clone());
    let message = headers.to_json_value()?;
    let event_str = serde_json::to_string(&message)?;
    debug!("Sending event to {}: {}", addr, event_str);
    tokio::time::timeout(
        WEBSOCKET_SEND_TIMEOUT,
        stream.send(Message::text(event_str)),
    )
    .await??;
    event.ack(BasicAckOptions::default()).await?;
    Ok(())
}
