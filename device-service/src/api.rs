use crate::types::{Error, Event, Request, Response};
use crate::Global;
use amqp_value_json::{ToAmqp, ToJson};
use fractal_auth_client::SystemContext;
use lapin::options::{BasicConsumeOptions, QueueDeclareOptions};
use lapin::types::{AMQPValue, FieldTable};
use lapin::BasicProperties;
use rocket::futures::StreamExt;
use rocket::serde::json::{self, Json};
use rocket::*;
use uuid::Uuid;

#[post("/event", data = "<data>")]
async fn publish_event(
    _context: SystemContext,
    shutdown: Shutdown,
    global: &State<Global>,
    data: Json<Event>,
) -> Result<(), Error> {
    // convert JSON event into AMQP headers
    let data_value = json::to_value(&*data)?;
    let amqp_headers = match data_value.to_amqp_value()? {
        AMQPValue::FieldTable(table) => table,
        _ => unreachable!(),
    };

    // publish AMQP message with headers
    let properties = BasicProperties::default().with_headers(amqp_headers);
    let result = global
        .channel
        .basic_publish(
            &global.options.amqp_exchange,
            "",
            Default::default(),
            &[],
            properties,
        )
        .await;

    // if there was any issue sending the event, shutdown this process so that
    // it can be restarted and reconnect to AMQP (fail quickly and restart).
    match result {
        Err(e) => {
            shutdown.notify();
            Err(e)?;
        }
        Ok(_) => {}
    }

    Ok(())
}

#[post("/request", data = "<data>")]
async fn send_request(
    _context: SystemContext,
    shutdown: Shutdown,
    global: &State<Global>,
    data: Json<Request>,
) -> Result<Json<Response>, Error> {
    // create channel for this request
    let channel = match global.connection.create_channel().await {
        Err(e) => {
            shutdown.notify();
            return Err(e.into());
        }
        Ok(channel) => channel,
    };

    // create queue for this request
    // we make sure this queue is automatically deleted when no longer subscribed to,
    // and we set an expiration on it.
    let mut queue_options = QueueDeclareOptions::default();
    queue_options.auto_delete = true;
    let mut options = FieldTable::default();
    options.insert(
        "x-expire".to_string().into(),
        (global.options.timeout.as_millis() as u64).into(),
    );
    let queue = channel.queue_declare("", queue_options, options).await?;

    // create request UUID if not exists
    let mut request = (&*data).clone();
    if request.request.is_none() {
        request.request = Some(Uuid::new_v4());
    }

    // convert JSON event into AMQP headers
    let data_value = json::to_value(&request)?;
    let amqp_headers = match data_value.to_amqp_value()? {
        AMQPValue::FieldTable(table) => table,
        _ => unreachable!(),
    };

    // subscribe to reply.
    let mut subscription = FieldTable::default();
    subscription.insert(
        "x-match".to_string().into(),
        AMQPValue::LongString("all".to_string().into()),
    );
    subscription.insert(
        "account".to_string().into(),
        AMQPValue::LongString(request.account.to_string().into()),
    );
    subscription.insert(
        "device".to_string().into(),
        AMQPValue::LongString(request.device.to_string().into()),
    );
    subscription.insert(
        "request".to_string().into(),
        AMQPValue::LongString(request.request.unwrap().to_string().into()),
    );
    subscription.insert(
        "service".to_string().into(),
        AMQPValue::LongString("hive-device".to_string().into()),
    );

    channel
        .queue_bind(
            queue.name().as_str(),
            &global.options.amqp_exchange,
            "#",
            Default::default(),
            subscription,
        )
        .await?;

    // publish AMQP message with headers
    let properties = BasicProperties::default().with_headers(amqp_headers);
    let result = channel
        .basic_publish(
            &global.options.amqp_exchange,
            "",
            Default::default(),
            &[],
            properties,
        )
        .await;

    // if there was any issue sending the event, shutdown this process so that
    // it can be restarted and reconnect to AMQP (fail quickly and restart).
    match result {
        Err(e) => {
            shutdown.notify();
            return Err(e.into());
        }
        Ok(_) => {}
    }

    // wait for response
    let mut consumer = channel
        .basic_consume(
            queue.name().as_str(),
            "hive-device-service-request",
            BasicConsumeOptions::default(),
            FieldTable::default(),
        )
        .await?;
    let message = consumer.next();

    // respect timeout
    let message = tokio::time::timeout(global.options.timeout, message);

    // handle response
    let message = match message.await {
        Ok(Some(Ok(message))) => message,
        Err(_timeout) => return Err(Error::Timeout),
        Ok(None) => unreachable!(),
        Ok(Some(Err(e))) => {
            shutdown.notify();
            return Err(e.into());
        }
    };

    // convert header
    let header = message
        .properties
        .headers()
        .as_ref()
        .ok_or(Error::MissingHeader)?;
    let header: json::Value = AMQPValue::FieldTable(header.clone()).to_json_value()?;
    let response = json::from_value(header)?;
    Ok(Json(response))
}

pub fn routes() -> Vec<Route> {
    routes![publish_event, send_request]
}
