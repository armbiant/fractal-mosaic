use log::*;
use rocket::http::Status;
use rocket::request;
use rocket::response::{self, Responder};
use rocket::serde::json::{self, Value};
use rocket::serde::uuid::Uuid;
use rocket::serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Error converting JSON Value to AMQPValue: {0:}")]
    JsonToAmqpError(#[from] amqp_value_json::ToAmqpError),
    #[error("Error converting AMQPValue to JSON Value: {0:}")]
    AmqpToJsonError(#[from] amqp_value_json::ToJsonError),
    #[error("Error encoding or decoding JSON: {0:}")]
    JsonError(#[from] json::serde_json::Error),
    #[error("AMQP error: {0:}")]
    AmqpError(#[from] lapin::Error),
    #[error("Timeout occured waiting for response")]
    Timeout,
    #[error("Missing headers in AMQP event")]
    MissingHeader,
}

impl<'r> Responder<'r, 'static> for Error {
    fn respond_to(self, _: &'r request::Request<'_>) -> response::Result<'static> {
        error!("Error: {:?}", self);
        response::Response::build().status(Status::BadRequest).ok()
    }
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct Event {
    pub service: String,
    pub r#type: String,
    pub account: Uuid,
    pub device: Uuid,
    #[serde(flatten)]
    pub other: BTreeMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct Request {
    pub service: String,
    pub r#type: String,
    pub account: Uuid,
    pub device: Uuid,
    #[serde(default)]
    pub request: Option<Uuid>,
    #[serde(flatten)]
    pub other: BTreeMap<String, Value>,
}

#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct Response {
    pub service: String,
    pub r#type: String,
    pub account: Uuid,
    pub device: Uuid,
    pub request: Uuid,
    #[serde(flatten)]
    pub other: BTreeMap<String, Value>,
}

#[test]
fn event_deserialize() {
    let json = json::json!({
        "service": "connectivity",
        "type": "link_up",
        "domain": "hasty-hopper.fractal.pub",
        "account": Uuid::new_v4().to_string(),
        "device": Uuid::new_v4().to_string(),
    });
    let event: Event = json::from_value(json).unwrap();
    assert_eq!(event.service, "connectivity");
    assert_eq!(event.r#type, "link_up");
    assert_eq!(event.other["domain"], "hasty-hopper.fractal.pub");
}

#[test]
fn event_serialize() {
    let account = Uuid::new_v4();
    let device = Uuid::new_v4();
    let event = Event {
        service: "connectivity".into(),
        r#type: "link_up".into(),
        other: {
            let mut map = BTreeMap::new();
            map.insert("domain".into(), "hasty-hopper.fractal.pub".into());
            map
        },
        account: account,
        device: device,
    };
    let json = json::to_value(&event).unwrap();
    let expected = json::json!({
        "service": "connectivity",
        "type": "link_up",
        "domain": "hasty-hopper.fractal.pub",
        "account": account.to_string(),
        "device": device.to_string(),
    });
    assert_eq!(json, expected);
}
