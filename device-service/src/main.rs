use anyhow::Result;
use fractal_auth_client::AuthConfig;
use lapin::types::{AMQPValue, FieldTable};
use lapin::{Channel, Connection, ConnectionProperties, ExchangeKind, Queue};
use log::*;
use reqwest::Client;
use rocket::config::Config;
use rocket::{Build, Rocket};
use std::net::SocketAddr;
use std::sync::Arc;
use std::time::Duration;
use structopt::StructOpt;
use tokio::sync::Notify;
use url::Url;

mod api;
mod callback;
#[cfg(test)]
mod tests;
mod types;

/// State that might be needed in all of the request handlers.
#[derive(Clone)]
pub struct Global {
    connection: Arc<Connection>,
    channel: Channel,
    options: Arc<Options>,
    client: Client,
    /// Async notification to shut down.
    shutdown: Arc<Notify>,
}

#[derive(StructOpt, Clone, Debug)]
pub struct Options {
    /// AMQP connection URL.
    #[structopt(long, short = "m", env = "HIVE_DEVICE_SERVICE_AMQP")]
    amqp: Url,

    /// AMQP exchange to publish to.
    #[structopt(
        long,
        env = "HIVE_DEVICE_SERVICE_AMQP_EXCHANGE",
        default_value = "events"
    )]
    amqp_exchange: String,

    /// Where to fetch JWKS from (used to validate JWTs).
    #[structopt(long, env = "HIVE_DEVICE_SERVICE_JWKS")]
    jwks: Option<Url>,

    /// JWT to use for callback requests.
    #[structopt(long, env = "HIVE_DEVICE_SERVICE_JWT")]
    jwt: String,

    /// URL to send callbacks to.
    #[structopt(long, env = "HIVE_DEVICE_SERVICE_CALLBACK_URL")]
    callback_url: Url,

    /// Queue for events for the callback.
    #[structopt(long, env = "HIVE_DEVICE_SERVICE_CALLBACK_QUEUE")]
    callback_queue: String,

    /// Disables authentication validation, enable only for debugging purposes.
    #[cfg(feature = "auth-insecure-stub")]
    #[structopt(long)]
    auth_insecure_stub: bool,

    /// Default timeout for waiting for command replies
    #[structopt(long, env = "HIVE_DEVICE_SERVICE_TIMEOUT", parse(try_from_str = parse_duration::parse))]
    timeout: Duration,

    /// Where to listen to for requests.
    #[structopt(long, env = "HIVE_DEVICE_SERVICE_LISTEN")]
    listen: SocketAddr,
}

/// Subscribe a particular queue to messages for a particular service.
pub async fn subscribe_service(
    options: &Options,
    channel: &Channel,
    queue: &Queue,
    service: &str,
) -> Result<()> {
    let mut subscription = FieldTable::default();
    subscription.insert(
        "x-match".to_string().into(),
        AMQPValue::LongString("all".to_string().into()),
    );
    subscription.insert(
        "service".to_string().into(),
        AMQPValue::LongString(service.to_string().into()),
    );

    channel
        .queue_bind(
            queue.name().as_str(),
            &options.amqp_exchange,
            "#",
            Default::default(),
            subscription,
        )
        .await?;
    Ok(())
}

impl Options {
    pub async fn auth_config(&self) -> Result<AuthConfig> {
        let mut auth_config = AuthConfig::new();

        if let Some(jwks) = &self.jwks {
            let key_store = fractal_auth_client::key_store(&jwks.to_string()).await?;
            auth_config = auth_config.with_keystore(key_store);
        }

        #[cfg(feature = "auth-insecure-stub")]
        if self.auth_insecure_stub {
            auth_config = auth_config.with_insecure_stub(self.auth_insecure_stub);
            error!("Disabling authentication verification because of auth-insecure-stub option");
        }

        Ok(auth_config)
    }

    pub async fn global(&self) -> Result<Global> {
        // establish connection
        let connection = Connection::connect(
            &self.amqp.to_string(),
            ConnectionProperties::default()
                .with_executor(tokio_executor_trait::Tokio::current())
                .with_reactor(tokio_reactor_trait::Tokio),
        )
        .await?;
        info!("Connected to AMQP");

        // open up channel, this will be used to send all of the events.
        let channel = connection.create_channel().await?;

        // declare the exchange. in case this service starts first, the
        // exchange might not yet exist. if it does, this is ignored.
        channel
            .exchange_declare(
                &self.amqp_exchange,
                ExchangeKind::Headers,
                Default::default(),
                Default::default(),
            )
            .await?;

        // declare queue for callbacks
        let queue = channel
            .queue_declare(&self.callback_queue, Default::default(), Default::default())
            .await?;

        // subscribe to all messages originating from hive devices
        // or the hive websocket (for the keepalive messages).
        subscribe_service(&self, &channel, &queue, "hive-device").await?;
        subscribe_service(&self, &channel, &queue, "hive-websocket").await?;
        info!("Connection to AMQP initialized");

        let global = Global {
            connection: Arc::new(connection),
            channel,
            options: Arc::new(self.clone()),
            client: Client::new(),
            shutdown: Arc::new(Notify::new()),
        };

        Ok(global)
    }

    pub async fn rocket(&self) -> Result<Rocket<Build>> {
        let config = Config::figment()
            .merge(("port", self.listen.port()))
            .merge(("address", self.listen.ip()));
        let rocket = rocket::custom(config)
            .mount("/api/v1", api::routes())
            .manage(self.clone());

        Ok(rocket)
    }

    pub async fn run(&self) -> Result<()> {
        let rocket = self.rocket().await?;
        let global = self.global().await?;
        tokio::spawn(callback::run(global.clone()));
        let auth_config = self.auth_config().await?;
        let rocket = rocket
            .manage(global.clone())
            .manage(auth_config)
            .ignite()
            .await?;

        // propagate shutdown notification to rocket
        let shutdown = rocket.shutdown();
        let notify = global.shutdown.clone();
        tokio::spawn(async move {
            notify.notified().await;
            shutdown.notify();
        });

        // launch REST service
        let _result = rocket.launch().await?;
        Ok(())
    }
}

#[rocket::main]
async fn main() -> Result<()> {
    env_logger::init();
    let options = Options::from_args();

    options.run().await?;
    Ok(())
}
