# Prepare Pages
#
# GitLab Pages is used to publish nightly builds and documentation
# that is produced during the pipeline. This script will generate
# that data.
set -eux

# given a job name ($1) and a path ($2), fetch the job artifacts from the most
# recent run if the path does not exist.
function fetch_job_artifacts() {
    if test ! -e "$2"; then
        curl -L https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/jobs/artifacts/main/download?job=$1 -o "$1.zip"
        unzip "$1.zip"
        rm "$1.zip"
    fi
}

# publish mkdocs documentation
fetch_job_artifacts mkdocs docs/site
mv docs/site public

# publish rust documentation
fetch_job_artifacts rustdoc target/doc
mv target/doc public/rustdoc

# create folder for nightly builds
mkdir -p public/nightly

# given a path ($1) and a binary name ($2), copy the file pointed to by the
# path to the public folder as nightly release
function nightly_release() {
    path="$1"
    binary="$2"

    # move binary
    mv "$path" "public/nightly/$binary"

    # sign binary, producing public/nightly/$binary.sig
    sign "public/nightly/$binary"
}

# fetch rust build job artifacts
fetch_job_artifacts build:amd64 target/release
fetch_job_artifacts build:arm32 target/arm-unknown-linux-gnueabihf/release
fetch_job_artifacts build:arm64 target/aarch64-unknown-linux-gnu/release

# release nightly builds for a rust component with the name $1
function nightly_release_rust() {
    nightly_release "target/release/$1" "$1-amd64"
    nightly_release "target/arm-unknown-linux-gnueabihf/release/$1" "$1-arm32"
    nightly_release "target/aarch64-unknown-linux-gnu/release/$1" "$1-arm64"
}

# release nightly builds of rust components
nightly_release_rust hive-websocket
nightly_release_rust hive-device-service
