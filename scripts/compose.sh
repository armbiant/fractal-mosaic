#!/bin/bash
# Runs a custom docker-compose command. This project uses docker-compose but
# has it split up into many different compose files. Using this script it is
# possible to tie these together and start or stop the entire stack.
docker compose -p fractal-mosaic -f docker-compose.yml -f docker-compose.dev.yml -f apps/hive-device/docker-compose.device.yml -f apps/hive-device/docker-compose.device2.yml -f docker-compose.flower.yml $@
