# Events

In general, events are JSON objects that have certain mandatory fields.
Here is an example:

```json
{
    "account": "8813b625-e219-4390-9a7b-fe0e22c9e50b",
    "command": "SchedulerAction.stop",
    "type": "device-request"
    "service": "hive-backend",
    "request": "5e5b0ee5-8774-4ebb-807c-48c9c26c16f8",
    "device": "d88f322f-d0aa-4a3c-8564-6ebe0f7ebb3b",
    "payload": {
        "account": "8813b625-e219-4390-9a7b-fe0e22c9e50b",
        "application": {
            "appstore": "Fractal App Store",
            "name": "wikijs",
            "version": "latest"
        },
        "encrypted_storage_apikeys": "DGJEKsFCYCkbzvlsmXivwvEZU5M0BRgROSjloYwF19w87b+Q1pE856jAHF9V4a5OYWo8GTvKIAiB32qlB+EpLp+uN8+qrhGAklAfa/CBoy2u8isbfiU4LD5kDupHzKbp2Tn2LnOAnuqc6Vi2Sia/LT+ehLkzo+CI4G2Z6uCNxGIEJGGR5xQAheBJQF55LhIAQejQuwbALA==",
        "instance": "ad51b914-688a-4a5f-a073-5c5121a010e2",
        "links": {
            "default": {
                "domain": "kerosene-freemason.fractalnetworks.co",
                "token": "3336a31e-dfa0-4681-ae4e-5f8c466f7024"
            }
        },
        "storage_apikeys": {}
    },
    "state": {
        "app_instance_id": "ad51b914-688a-4a5f-a073-5c5121a010e2",
        "device_id": "d88f322f-d0aa-4a3c-8564-6ebe0f7ebb3b",
        "force": false,
        "state": "AppInstanceState.stopped"
    },
}
```

Here's the standard event fields:

| Name | Description |
| --- | --- |
| `account` | UUID of the account that this event is from or for. |
| `service` | The service that sent the event. |
| `type`    | The message type, depends on the service. |

## Device Alive Event

Periodically, the WebSocket service will send all connected Devices a PING
message. When the devices respond to this message with a PONG, a Device Alive
Event is broadcast through RabbitMQ to indicate that the Device is still
connected.

| Name | Description |
| --- | --- |
| `device` | Device UUID |
| `account` | Account UUID |
| `service` | `hive-backend` |
| `address` | Remote address of Device |

## Device Events

Devices can send arbitary events on the WebSocket connection to the WebSocket
service.  The WebSocket service will relay these to RabbitMQ to deliver to the
Backend. The WebSocket service will insert some metadata (such as the device
UUID, account UUID) and will always set the service field to `hive-device`. All
other fields are taken over from the Device. These are called *Device Events*.
Device events always have the following fields:

| Name | Description |
| --- | --- |
| `device` | Device UUID |
| `account` | Account UUID |
| `service` | `hive-device` |
| `type` | Type of event |

These are the `type` of events that Devices will currently emit, and their semantics:

| Type | Description |
| --- | --- |
| `instances` | Array of applications that the device is aware of running. This message indicates to the Hive Backend the health state of the applications. |
| `instance-state` | Contains configuration changes about an App Instance. (Storage Key changes). |
| `device-version` | The reported version of the device. |
| `device-state` | Contains info on device state changes (device is running low on disk space). |

## Device Requests

The Backend can send requests to the Device. Unlike events, which are
transmitted unreliably, Requests expect a response (which can be an
acknowledgement of reciept or a result, such as when using this system to
implement remote procedural calls).

Device events always have the service set to `hive-backend` and the type set to
`device-request`. They also have a request UUID, which is used to correlate
responses. The actual kind of request is indicated by the `command` field.

In addition to the standard event fields, Device events have these fields:

|  Property | Description |
| -------- | --- |
| account | Account UUID. |
| device  | UUID for the associated device. |
| service | `hive-backend` |
| type    | `device-request` |
| request | Unique request UUID. Used to confirm acks. |
| command | The command for the device, see table below. |
| payload | JSON blob that contains context related to the command.  |
| state   | NOT USED. Indicates the expected state from Hive. |

<!--
Payload Structure:
- `account`: UUID for the associated user.
- `command`: Command for the Device
- `application`
  - `appstore`: The appstore the application is from
  - `name`: Name of the app.
  - `version`: Version of the app to use
- `encrypted_storage_apikeys`: The Device passphrase encrypted api keys if any.
- `instance`: The associated app instance UUID.
- `links`: The Fractal Links associated with the app.
  - `default`: The default Fractal Link to use
    - `domain`: The domain of the Fractal Link.
    - `token`: The authorization token for the Fractal Link.
- `storage_apikeys` (NOT USED): LEGACY storage apikeys from before
  passphrase encryption. This was used in an old version of the Hive Backend.
-->

These are the supported commands that can be sent to the Device:

| Name | Description |
| --- | --- |
| UpgradeDevice | Indicates to the device to update itself (using Watchtower). |
| DeviceCommand | Hardware commands (disk space check). |
| SchedulerAction | An action that indicates the the Device should start or stop an app. |
| AppInstanceCommand | Configuration specific commands (Creating an Element User). |

The Device will immediately ack any message it receives from the Hive Backend.

