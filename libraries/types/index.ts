import {IconPrefix, IconName, FlipProp} from '@fortawesome/fontawesome-svg-core'
import {DropdownItemProps} from 'react-bootstrap/esm/DropdownItem'
import {FormControlProps} from 'react-bootstrap'

// Central List of Types used in Data components
// If declaring interface, it should be called Props
// If declaring a type, it should be called Type'

// TODO: change actions label into title
export type LayoutChildrenType = React.ReactNode
export type DataChildrenType = JSX.Element

export type UuidType = string
export type TitleType = string
export type IcnType = [IconPrefix, IconName]
export type ImgType = string
export type HealthType = 'green' | 'red' | 'yellow' | boolean
export type UrlType = string
export type LoadingType = boolean
export type LinkToType = 'home' | 'logOut'
export type MonoNavActiveType = 'Home' | 'Store' | 'Docs' | 'Console'
export type MenuType = boolean
export type ToggleMenuType = any
export type SessionType = any
export type TargetState = 'stopped' | 'running'

// Should be an array of functions
export type ActionsType = {
	func: () => void
	icn: IcnType
	title: TitleType
}

// Data components
export interface DataProps {
	children: DataChildrenType
	uuid?: UuidType
}

// Layout Components
export interface LayoutProps {
	children?: LayoutChildrenType
	title?: TitleType
}
export interface LayoutMobileProps extends LayoutProps {
	// TODO: specify icon instead of using boolean
	setSidebar?: ToggleMenuType
	icn: IcnType
	loading?: LoadingType
}

// Modals
export interface ModalProps {
	uuid: UuidType
	show: boolean
}

// Custom Components
export interface CustomComponentProps {
	title?: TitleType
	uuid?: UuidType
}
export interface DisplayComponentProps extends CustomComponentProps {
	icn?: IcnType
	img?: ImgType
}

export interface CardProps extends DisplayComponentProps {
	app?: string
	health?: HealthType
	loading?: LoadingType
	actions?: ActionsType[]
	url?: UrlType
	noCircle?: boolean
	useSession?: SessionType
	subtitle?: string
	description?: string
	github?: DataChildrenType
	detailPage?: UrlType
	target_state?: 'stopped' | 'running'
	device_name?: string
	config?: string
	appPowerSwitch?: (uuid: string, targetState: TargetState) => Promise<any>
	addDeviceButton?: boolean
}
export interface PictureProps extends DisplayComponentProps {
	health?: HealthType
	flip?: FlipProp
	isActive?: boolean
	circle?: boolean
}
export interface CenterTitleProps extends CustomComponentProps {
	loading?: boolean
}
export interface NavItemProps extends DisplayComponentProps {
	title?: TitleType
	loading?: LoadingType
	linkTo?: LinkToType
	isActive?: boolean
	action?: () => any
}

export interface MyProfileProps {
	children?: DataChildrenType
	title?: TitleType
	icn?: IcnType
}

export interface TextInputProps extends LayoutProps, FormControlProps {
	code?: boolean
	rtl?: boolean // change the direction of text from left-to-right to right-to-left
	loading?: LoadingType
}

export interface DropdownItmProps extends DropdownItemProps {
	// Extends Bootstrap Component
	// Docs: https://react-bootstrap.netlify.app/components/dropdowns/#dropdown-item-props
	title: TitleType
	icn?: IcnType
}

export interface SidebarProps extends LayoutProps {
	sidebar?: MenuType
	setSidebar?: ToggleMenuType
}
export interface MonoNavProps {
	children?: LayoutChildrenType
	uuid?: UuidType
	currentDomain: MonoNavActiveType
	useSession?: SessionType
}
export interface MonoNavItemProps extends NavItemProps {
	menuPress?: ToggleMenuType
}
