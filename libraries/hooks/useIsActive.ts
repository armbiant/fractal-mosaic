import { useRouter } from "next/router";

const useIsActive = (uuid?: string): boolean => {
  const router = useRouter();
  if (router.pathname === "/" && !uuid) return true;
  if (router.query.uuid === uuid) return true;
  return false;
};

export default useIsActive;
