// based on https://dev.to/reedbarger/how-to-create-a-custom-usecopytoclipboard-react-hook-5g5p

import { useState, useCallback, useEffect } from "react";

export default function useCopyToClipboard(resetDelay: number) {
  const [isCopied, setCopied] = useState(false);

  const handleCopy = useCallback((text: string | undefined) => {
    if (text) {
      navigator.clipboard.writeText(text);

      setCopied(true);
    }
  }, []);

  useEffect(() => {
    let timeout: any;

    if (isCopied && resetDelay) {
      timeout = setTimeout(() => setCopied(false), resetDelay);
    }

    return () => clearTimeout(timeout);
  }, [isCopied, resetDelay]);

  return [isCopied, handleCopy] as const;
}
