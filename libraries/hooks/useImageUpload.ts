import {
  LegacyRef,
  MutableRefObject,
  useEffect,
  useRef,
  useState,
} from "react";
import { ImgType } from "../types";

// credit: https://stackoverflow.com/a/65518980

const useImageUpload = () => {
  // we are referencing the file input
  const inputRef = useRef<any>();

  // On each file selection update the default image
  const [selectedImg, setSelectedImg] = useState();
  const [base64Img, setBase64Img] = useState<ImgType>();

  // On click on camera icon open the dialog
  const showOpenFileDialog = () => {
    // @ts-ignore
    inputRef.current.click();
  };

  // On each change let user have access to a selected file
  const setImgFile = (event: any) => {
    let imgFile = event?.target?.files[0];

    if (imgFile) {
      const reader = new FileReader();

      reader.readAsDataURL(imgFile);

      reader.onload = () => setBase64Img(reader.result as ImgType);

      setSelectedImg(imgFile);

      // this resets the value,
      event.target.value = "";
    }
  };

  const resetImg = () => {
    setSelectedImg(undefined);
    setBase64Img(undefined);
  };

  // Clean up the selection to avoid memory leak
  useEffect(() => {
    if (selectedImg) {
      const objectURL = URL.createObjectURL(selectedImg);

      return () => URL.revokeObjectURL(objectURL);
    }
  }, [selectedImg]);

  return {
    selectedImg,
    base64Img,
    inputRef,
    showOpenFileDialog,
    setImgFile,
    resetImg,
  };
};

export default useImageUpload;
