// React
import React from 'react'

// SWR
import useSWR from 'swr'
import fetcher from 'util/fetcher'

// Fractal
import Group from './Group'
import LoadingSpinner from 'components/Spinners/LoadingSpinner'

// Util
import { DataProps } from 'types'

const GroupList = ({ children, uuid }: DataProps) => {
  // Get list of Groups from backend
  const { data, error } = useSWR('/api/v1/group', fetcher)
  // const data = false // for testing
  // const error = false // for testing

  const renderList = () => {
    // success
    if (data) {
      return data.map((uuid: string) => {
        return <Group uuid={uuid} children={children} key={uuid} />
      })

      // error
    } else if (error) {
      return console.error("Can't load Group List:", error)
    }

    // loading
    return <LoadingSpinner />
  }

  return <>{renderList()}</>
}

export default GroupList
