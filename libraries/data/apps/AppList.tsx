// React
import React from 'react'

// Fractal
import LoadingSpinner from 'components/Spinners/LoadingSpinner'
import App from './App'

// Util
import { DataProps } from 'types'
import useSwrAuth from 'hooks/useSwrAuth'

// Fractal Endpoints
import { APP_LIST_ENDPOINT } from 'util/endpoints'

const AppList = ({ children, uuid }: DataProps) => {
  // Get list of Apps from backend
  const { data, error } = useSwrAuth(APP_LIST_ENDPOINT)
  // const data = false // for testing
  // const error = false // for testing

  const renderList = () => {
    // success
    if (data) {
      return data.map((uuid: { uuid: string }) => {
        return <App uuid={uuid.uuid} children={children} key={uuid.uuid} />
      })

      // error
    } else if (error) {
      return console.error("Can't load App List:", error)
    }

    // loading
    return <LoadingSpinner />
  }

  return <>{renderList()}</>
}

export default AppList
