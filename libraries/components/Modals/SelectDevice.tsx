import React, {Dispatch, SetStateAction, useEffect, useState, useRef} from 'react'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

// Fractal
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'
import CardMd from 'components/Card/CardMd'
import DeviceList from 'data/devices/DeviceList'
import {HealthType} from 'types'

// Mosaic Api
import useMosaicApi from 'hooks/useMosaicApi'
import Device from 'data/devices/Device'
import CardSm from '../Card/CardSm'

// SWR
import {useSWRConfig} from 'swr'
import {APP_LIST_ENDPOINT, FRONTEND_APP_CHECK_ENDPOINT} from 'util/endpoints'
import {useSession} from 'next-auth/react'

interface ISelectDeviceProps {
	show: boolean
	onHide: () => void
	appUuid: string
	title: string
	showDeviceModal: boolean
	setShowDeviceModal: Dispatch<SetStateAction<boolean>>
	userUuid: string
}

export interface IInstallAppProps {
	title: string
	appUuid: string
	deviceUuid: string
}

const SelectDevice = ({
	show,
	onHide,
	appUuid,
	title,
	showDeviceModal,
	setShowDeviceModal,
	userUuid,
}: ISelectDeviceProps) => {
	const {data: session} = useSession()
	const {installApp} = useMosaicApi()
	const {mutate} = useSWRConfig()

	const ref = React.createRef()

	// Disable request state
	const [isFetching, setIsFetching] = useState(false)

	const handleOnDone = async () => {
		if (deviceSelected.deviceUuid == '41e35656-349f-4d69-9e5d-6bbc735cf85e'){
			// this is comment
			window.open('https://www.youtube.com/watch?v=SegAoSpHJck', '_blank');
			return;
		}
		setIsFetching(true)
		const response = await installApp(deviceSelected.appUuid, deviceSelected.deviceUuid)
		if (window.location.href.includes('console')) {
			if (response?.status == 201) {
				mutate([APP_LIST_ENDPOINT, session?.accessToken])
				mutate([FRONTEND_APP_CHECK_ENDPOINT(response?.data.app), session?.accessToken])
			}
		}
		setIsFetching(false)
		onHide()
	}

	const handleOnExited = () => {
		setDeviceSelected({
			title: 'Fractal Cloud Device',
			appUuid: appUuid,
			deviceUuid: '41e35656-349f-4d69-9e5d-6bbc735cf85e',
		})
	}

	const [deviceSelected, setDeviceSelected] = useState<IInstallAppProps>({
		title: 'Fractal Cloud Device',
		appUuid: appUuid,
		deviceUuid: '41e35656-349f-4d69-9e5d-6bbc735cf85e',
	})

	return (
		<>
			<Modal show={show} onHide={onHide} onExit={handleOnExited} centered>
				<LayoutModalHeader>
					Deploy {title} to device: &ensp;
					{deviceSelected.deviceUuid && deviceSelected.title != 'Fractal Cloud Device' ? (
						<Device uuid={deviceSelected.deviceUuid} children={<CardSm />} />
					) : (
						<CardSm title='Fractal Cloud Device' health='green' />
					)}
				</LayoutModalHeader>
				<Modal.Body className='border-top-0 rounded-0 d-flex flex-column'>
					<DeviceSelectButton
						ref={ref}
						setDeviceSelected={setDeviceSelected}
						appUuid={appUuid}
						deviceUuid='41e35656-349f-4d69-9e5d-6bbc735cf85e'
						title='Fractal Cloud Device'
						health='green'
					/>
					<DeviceList>
						<DeviceSelectButton setDeviceSelected={setDeviceSelected} appUuid={appUuid} />
					</DeviceList>
					<Button className='px-0' variant='secondary' onClick={() => setShowDeviceModal(!showDeviceModal)}>
						<CardMd addDeviceButton title={'Add device'} />
					</Button>
				</Modal.Body>
				<LayoutModalFooter>
					<Button onClick={() => onHide()} variant='secondary'>
						Cancel
					</Button>
					<Button disabled={isFetching && true} onClick={() => handleOnDone()} variant='primary'>
						Done
					</Button>
				</LayoutModalFooter>
			</Modal>
		</>
	)
}

export default SelectDevice

interface IDeviceSelectButtonProps {
	children?: any
	health?: HealthType
	title?: string
	deviceUuid?: string
	appUuid?: string
	uuid?: string
	setDeviceSelected: React.Dispatch<React.SetStateAction<IInstallAppProps>>
}
export const DeviceSelectButton = React.forwardRef(
	(
		{children, health, title, deviceUuid, appUuid, uuid, setDeviceSelected, ...props}: IDeviceSelectButtonProps,
		ref
	) => {
		useEffect(() => {
			// @ts-ignore
			if (ref) ref.current.focus()
		}, [])
		return React.cloneElement(
			<Button
				// @ts-ignore
				ref={ref}
				onClick={() =>
					setDeviceSelected({
						title: title as string,
						appUuid: appUuid as string,
						deviceUuid: deviceUuid ? deviceUuid : (uuid as string),
					})
				}
				className='px-1'
				variant='secondary'>
				{children}
			</Button>,
			{
				...props,
			},
			<CardMd title={title} health={health} />
		)
	}
)
