/* eslint-disable react/no-children-prop */
/* eslint-disable react/no-unescaped-entities */
// React
import React, {useState, useRef} from 'react'
import {useSession} from 'next-auth/react'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

// Font Awesome
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Fractal
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'
import CardSm from 'components/Card/CardSm'
import Member from 'data/members/Member'
import TextInput from 'components/TextInput'
import TextCopy from 'components/TextCopy'

// Fractal Client Api
import useMosaicApi from 'hooks/useMosaicApi'
import {saltPassphrase, verifyPassphrase} from 'util/passphrase'

// React Hook Form
import {useForm, SubmitHandler} from 'react-hook-form'
import {ErrorMessage} from '@hookform/error-message'

type Inputs = {
	passphrase: string
	confirmPassphrase: string
	fractalPassphrase: string
}

const ModalDeviceInstall = ({show, setShow}: {show: any; setShow: any}) => {
	// Passphrase Modal Logic
	const {data: session} = useSession()
	const [showPassPhraseEnterModal, setShowPassphraseEnterModal] = useState(true)
	const [deviceModalShow, setDeviceModalShow] = useState(false)

	const [saltedPassphrase, setSaltedPassphrase] = useState('')
	const [salt, setSalt] = useState('')

	const fetchPassphraseOnLoad = async () => {
		const response = await getPassphrase(session?.uuid as string)
		if (response?.data.salt && response?.data.salted_passphrase) {
			setShowPassphraseEnterModal(false)
			setSalt(response?.data.salt)
			setSaltedPassphrase(response?.data.salted_passphrase)
		}
	}

	// React Hook Forms
	const {
		register,
		handleSubmit,
		formState: {errors},
		watch,
		reset,
		getValues,
	} = useForm<Inputs>({
		criteriaMode: 'all',
	})

	// Disable button submit handler state
	const [isFetching, setIsFetching] = useState(false)

	const onSubmit: SubmitHandler<Inputs> = ({passphrase}) => {
		setIsFetching(true)
		setPassphrase(passphrase)
		setDeviceModalShow(true)
		setIsFetching(false)
	}

	const onPassphraseSubmit: SubmitHandler<Inputs> = ({fractalPassphrase}) => {
		return handleOnPassphraseSubmit(fractalPassphrase)
	}

	const handleOnPassphraseSubmit = async (passphrase: string) => {
		if (await verifyPassphrase(passphrase, salt, saltedPassphrase)) setDeviceModalShow(true)
	}

	const handleOnEnter = () => {
		fetchPassphraseOnLoad()
		handleTokenGeneration()
	}

	const [deviceToken, setDeviceToken] = useState<number | null | undefined>(undefined)
	const {generateDeviceToken, getPassphrase, setPassphrase} = useMosaicApi()
	const handleTokenGeneration = async () => {
		const token = await generateDeviceToken()
		if (token === null) {
			// console.log('IS NULL')
			setDeviceToken(null)
		} else {
			setDeviceToken(token.token)
		}
	}

	// Auto Updates state
	const [autoUpdate, setAutoUpdate] = useState(true)

	// Device name state
	const [deviceName, setDeviceName] = useState<string>('')
	const handleDeviceNameChange = (e: any) => {
		if (parseInt(e.target.value.length) !== 0) setDeviceName(e.target.value)
		if (parseInt(e.target.value.length) === 0) setDeviceName('')
	}
	let mosaicImage = 'fractalnetworks/hive-device:alpha'
	let dockerCommand: string =
		process.env.NEXT_PUBLIC_DEVICE_API_DOMAIN === 'fractalnetworks.co'
			? `docker run -v /var/run/docker.sock:/var/run/docker.sock -it -e API_TOKEN=${deviceToken} -e DEVICE_SECRET=${
					getValues('fractalPassphrase') ? getValues('fractalPassphrase') : getValues('passphrase')
			  } -e AUTO_UPDATES=${autoUpdate.toString()} --restart unless-stopped`
			: `docker run -v /var/run/docker.sock:/var/run/docker.sock -it -e API_TOKEN=${deviceToken} -e DEVICE_SECRET=${
					getValues('fractalPassphrase') ? getValues('fractalPassphrase') : getValues('passphrase')
			  } -e AUTO_UPDATES=${autoUpdate.toString()} -e LINK_GATEWAY_API=https://gateway.${process.env.NEXT_PUBLIC_DEVICE_API_DOMAIN} -e STORAGE_DEFAULT_TARGET=https://storage.${process.env.NEXT_PUBLIC_DEVICE_API_DOMAIN} -e HIVE_WEBSOCKET=wss://ws.${process.env.NEXT_PUBLIC_DEVICE_API_DOMAIN} -e FRACTAL_HIVE_API=https://hive-api.${process.env.NEXT_PUBLIC_DEVICE_API_DOMAIN} --restart unless-stopped`
	let nameOption: string = `-e DEVICE_NAME="${deviceName}"`
	let dockerCommandWithName: string = `${dockerCommand} ${nameOption} -d ${mosaicImage}`

	// Reset all state when closing
	const handleOnExited = () => {
		setDeviceModalShow(false)
		setShowPassphraseEnterModal(true)
		setDeviceName('')
		setDeviceToken(undefined)
		setAutoUpdate(true)
		reset()
	}

	const errorText: string = 'ERROR PLEASE TRY AGAIN'

	const handleDockerCommandText = (): string => {
		if (!deviceToken) return errorText
		if (deviceName == '') return `${dockerCommand} ${mosaicImage}`
		return dockerCommandWithName
	}

	return (
		<Modal
			show={show}
			onHide={() => setShow(!show)}
			onEnter={() => handleOnEnter()}
			onExited={handleOnExited}
			size='lg'
			centered>
			{deviceModalShow && (
				<div>
					<LayoutModalHeader>
						Add
						{deviceName ? (
							<>
								<span>&ensp;</span>
								<CardSm title={deviceName} />
								<span>&ensp;</span>
							</>
						) : (
							<span>&nbsp;</span>
						)}
						device to &ensp;
						<Member children={<CardSm />} />
					</LayoutModalHeader>

					<Modal.Body className='border-top-0 rounded-0'>
						<h3 className='mb-0'>Add One Device</h3>
						<Form>
							<Form.Group className='mb-3' controlId='formBasicEmail'>
								<Form.Label>Give this device a name (optional)</Form.Label>
								{/* @ts-ignore */}
								<TextInput title='Device Name' onChange={(e) => handleDeviceNameChange(e)} autoComplete='off' />
							</Form.Group>
							<Form.Group className='mb-3' controlId='formBasicEmail'>
								<Form.Label>Run this code in your device's terminal</Form.Label>
								<TextCopy
									title={handleDockerCommandText()}
									rtl={deviceName == '' ? false : true}
									code
									disabled={!deviceToken}
									loading={deviceToken === undefined}
								/>
							</Form.Group>
							<div className='d-flex align-items-center'>
								<input
									className='me-2'
									type='checkbox'
									id='auto-updates'
									name='auto-updates'
									onClick={() => setAutoUpdate(!autoUpdate)}
									checked={autoUpdate}
								/>
								<Form.Label className='mb-0' htmlFor='auto-updates'>
									Auto Updates
								</Form.Label>
								{/* {!autoUpdate && <span className='ms-2 text-danger'>(Not recommended)</span>} */}
							</div>
						</Form>
					</Modal.Body>

					<LayoutModalFooter>
						<Button onClick={() => setShow(!show)} variant='secondary'>
							Cancel
						</Button>
						<Button onClick={() => setShow(!show)} variant='primary'>
							Done
						</Button>
					</LayoutModalFooter>
				</div>
			)}
			{!deviceModalShow && (
				<div>
					{showPassPhraseEnterModal ? (
						<>
							<LayoutModalHeader>Set your System Passphrase:</LayoutModalHeader>
							<Modal.Body className='border-top-0 rounded-0 d-flex flex-column'>
								<Form onSubmit={(e) => e.preventDefault()}>
									<div className='d-flex flex-wrap align-items-center modal-title h4 pb-3'>
										Your System Passphrase is used to secure sensitive data such as encryption keys.
									</div>
									<Form.Group className='mb-3' controlId='formBasicEmail'>
										<Form.Label>Passphrase</Form.Label>
										<Form.Control
											{...register('passphrase', {required: 'This field is required'})}
											type='password'
											placeholder='Enter passphrase'
										/>
										<ErrorMessage
											errors={errors}
											name='passphrase'
											render={({messages}) =>
												messages &&
												Object.entries(messages).map(([type, message]) => {
													return (
														<span className='text-danger d-flex align-items-center' key={type}>
															{' '}
															<FontAwesomeIcon icon={['fas', 'triangle-exclamation']} fixedWidth />
															{message}
														</span>
													)
												})
											}
										/>
									</Form.Group>

									<Form.Group className='mb-3' controlId='formBasicPassword'>
										<Form.Label>Confirm Passphrase</Form.Label>
										<Form.Control
											{...register('confirmPassphrase', {
												required: true,
												validate: (value) => value === watch('passphrase') || 'The inputs do not match',
											})}
											type='password'
											placeholder='Confirm passphrase'
										/>
										<ErrorMessage
											errors={errors}
											name='confirmPassphrase'
											render={({messages}) =>
												messages &&
												Object.entries(messages).map(([type, message]) => {
													// console.log('type: ', type, 'message: ', message)
													return (
														<span className='text-danger d-flex align-items-center' key={type}>
															{' '}
															<FontAwesomeIcon icon={['fas', 'triangle-exclamation']} fixedWidth />
															{message}
														</span>
													)
												})
											}
										/>
									</Form.Group>
								</Form>
							</Modal.Body>
							<LayoutModalFooter>
								<Button onClick={() => setShow(!show)} variant='secondary'>
									Cancel
								</Button>
								<Button disabled={isFetching && true} onClick={handleSubmit(onSubmit)} variant='primary'>
									Submit
								</Button>
							</LayoutModalFooter>
						</>
					) : (
						<>
							<LayoutModalHeader>Enter your System Passphrase:</LayoutModalHeader>
							<Modal.Body className='border-top-0 rounded-0 d-flex flex-column'>
								<Form onSubmit={(e) => e.preventDefault()}>
									<Form.Group className='mb-3' controlId='formBasicEmail'>
										<Form.Label>Passphrase</Form.Label>
										<Form.Control
											{...register('fractalPassphrase', {
												required: 'This field is required',
												validate: async (value) =>
													(await verifyPassphrase(value, salt, saltedPassphrase)) || 'Incorrect System Passphrase',
											})}
											type='password'
											placeholder='Enter passphrase'
										/>
										<ErrorMessage
											errors={errors}
											name='fractalPassphrase'
											render={({messages}) =>
												messages &&
												Object.entries(messages).map(([type, message]) => {
													// console.log('type: ', type, 'message: ', message)
													return (
														<span className='text-danger d-flex align-items-center' key={type}>
															{' '}
															<FontAwesomeIcon icon={['fas', 'triangle-exclamation']} fixedWidth />
															{message}
														</span>
													)
												})
											}
										/>
									</Form.Group>
								</Form>
							</Modal.Body>
							<LayoutModalFooter>
								<Button onClick={() => setShow(!show)} variant='secondary'>
									Cancel
								</Button>
								<Button onClick={handleSubmit(onPassphraseSubmit)} variant='primary'>
									Submit
								</Button>
							</LayoutModalFooter>
						</>
					)}
				</div>
			)}
		</Modal>
	)
}

export default ModalDeviceInstall
