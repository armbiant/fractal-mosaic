import React, {Dispatch, SetStateAction, useState} from 'react'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

// Font Awesome
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Fractal Components
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from '../Layout/Modal/LayoutModalFooter'

// React Hook Forms
import {useForm, SubmitHandler} from 'react-hook-form'
import {ErrorMessage} from '@hookform/error-message'
import useMosaicApi from 'hooks/useMosaicApi'

interface IElementAddUserModal {
	show: boolean
	setShow: Dispatch<SetStateAction<boolean>>
	app_uuid: string
}

type Inputs = {
	username: string
	password: string
	confirmPassword: string
}

const ModalElementAddUser = ({show, setShow, app_uuid}: IElementAddUserModal) => {
	const {addElementUser} = useMosaicApi()
	// React Hook Forms
	const {
		register,
		handleSubmit,
		reset,
		watch,
		formState: {errors},
	} = useForm<Inputs>({
		criteriaMode: 'all',
	})

	const onSubmit: SubmitHandler<Inputs> = (data) => {
		return handleOnDoneClick(data.username, data.password)
	}

	const handleOnDoneClick = async (username: string, password: string) => {
		isAdmin ? addElementUser(username, password, app_uuid, isAdmin) : addElementUser(username, password, app_uuid)
		setShow(!show)
	}

	const [isAdmin, setIsAdmin] = useState<boolean>(false)

	return (
		<Modal
			show={show}
			onHide={() => setShow(!show)}
			onExited={() => {
				reset()
				setIsAdmin(false)
			}}
			centered>
			<Form onSubmit={handleSubmit(onSubmit)}>
				<LayoutModalHeader>Add user to element server.</LayoutModalHeader>
				<Modal.Body className='rounded-0 rounded-bottom'>
					<input type='checkbox' id='default-checkbox' onClick={() => setIsAdmin(!isAdmin)} />
					<label className='form-label ms-2'>Admin User</label>
					<Form.Group className='mb-3'>
						<Form.Label>Username</Form.Label>
						<Form.Control
							{...register('username', {
								required: 'This input is required',
							})}
							type='text'
							placeholder='username'
						/>
						<ErrorMessage
							errors={errors}
							name='username'
							render={({messages}) =>
								messages &&
								Object.entries(messages).map(([type, message]) => {
									return (
										<span className='text-danger d-flex align-items-center' key={type}>
											{' '}
											<FontAwesomeIcon icon={['fas', 'triangle-exclamation']} fixedWidth />
											{message}
										</span>
									)
								})
							}
						/>
					</Form.Group>
					<Form.Group className='mb-3'>
						<Form.Label>Password</Form.Label>
						<Form.Control
							{...register('password', {
								required: 'This input is required',
							})}
							type='password'
							placeholder='password'
						/>
						<ErrorMessage
							errors={errors}
							name='password'
							render={({messages}) =>
								messages &&
								Object.entries(messages).map(([type, message]) => {
									return (
										<span className='text-danger d-flex align-items-center' key={type}>
											{' '}
											<FontAwesomeIcon icon={['fas', 'triangle-exclamation']} fixedWidth />
											{message}
										</span>
									)
								})
							}
						/>
					</Form.Group>
					<Form.Group className='mb-3'>
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control
							{...register('confirmPassword', {
								required: 'This input is required',
								validate: (value) => value === watch('password') || 'The inputs do not match',
							})}
							type='password'
							placeholder='confirm password'
						/>
						<ErrorMessage
							errors={errors}
							name='confirmPassword'
							render={({messages}) =>
								messages &&
								Object.entries(messages).map(([type, message]) => {
									return (
										<span className='text-danger d-flex align-items-center' key={type}>
											{' '}
											<FontAwesomeIcon icon={['fas', 'triangle-exclamation']} fixedWidth />
											{message}
										</span>
									)
								})
							}
						/>
					</Form.Group>
				</Modal.Body>
				<LayoutModalFooter>
					<Button onClick={() => setShow(!show)} variant='secondary'>
						Cancel
					</Button>
					<Button type='submit'>Done</Button>
				</LayoutModalFooter>
			</Form>
		</Modal>
	)
}

export default ModalElementAddUser
