import React, {Dispatch, SetStateAction} from 'react'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'

// Font Awesome
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Fractal Components
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from '../Layout/Modal/LayoutModalFooter'

// React Hook Forms
import {useForm, SubmitHandler} from 'react-hook-form'
import {ErrorMessage} from '@hookform/error-message'
import useMosaicApi from 'hooks/useMosaicApi'

interface IElementConfigModal {
	show: boolean
	setShow: Dispatch<SetStateAction<boolean>>
	url: string
	app_uuid: string
}

type Inputs = {
	username: string
	password: string
	confirmPassword: string
}

const ModalElementConfig = ({show, setShow, url, app_uuid}: IElementConfigModal) => {
	const {appInstanceConfig} = useMosaicApi()

	// React Hook Forms
	const {
		register,
		handleSubmit,
		watch,
		reset,
		formState: {errors},
	} = useForm<Inputs>({
		criteriaMode: 'all',
	})

	const onSubmit: SubmitHandler<Inputs> = (data) => {
		return handleOnDoneClick()
	}

	const handleOnDoneClick = async () => {
		const response = await appInstanceConfig(watch('username'), watch('password'), app_uuid)
		console.log(response)
		if (response?.status == 201) {
			window.open(url, '_blank')
		}
		setShow(!show)
	}

	return (
		<Modal show={show} onHide={() => setShow(!show)} onExited={() => reset()} centered>
			<Form onSubmit={handleSubmit(onSubmit)}>
				<LayoutModalHeader>Create Element User</LayoutModalHeader>
				<Modal.Body className='rounded-0 rounded-bottom'>
					<Form.Group className='mb-3'>
						<Form.Label>Username</Form.Label>
						<Form.Control
							{...register('username', {
								required: 'This input is required',
							})}
							type='text'
							placeholder='username'
						/>
						<ErrorMessage
							errors={errors}
							name='username'
							render={({messages}) =>
								messages &&
								Object.entries(messages).map(([type, message]) => {
									return (
										<span className='text-danger d-flex align-items-center' key={type}>
											{' '}
											<FontAwesomeIcon icon={['fas', 'triangle-exclamation']} fixedWidth />
											{message}
										</span>
									)
								})
							}
						/>
					</Form.Group>
					<Form.Group className='mb-3'>
						<Form.Label>Password</Form.Label>
						<Form.Control
							{...register('password', {
								required: 'This input is required',
							})}
							type='password'
							placeholder='password'
						/>
						<ErrorMessage
							errors={errors}
							name='password'
							render={({messages}) =>
								messages &&
								Object.entries(messages).map(([type, message]) => {
									return (
										<span className='text-danger d-flex align-items-center' key={type}>
											{' '}
											<FontAwesomeIcon icon={['fas', 'triangle-exclamation']} fixedWidth />
											{message}
										</span>
									)
								})
							}
						/>
					</Form.Group>
					<Form.Group className='mb-3'>
						<Form.Label>Confirm Password</Form.Label>
						<Form.Control
							{...register('confirmPassword', {
								required: 'This input is required',
								validate: (value) => value === watch('password') || 'The inputs do not match',
							})}
							type='password'
							placeholder='confirm password'
						/>
						<ErrorMessage
							errors={errors}
							name='confirmPassword'
							render={({messages}) =>
								messages &&
								Object.entries(messages).map(([type, message]) => {
									return (
										<span className='text-danger d-flex align-items-center' key={type}>
											{' '}
											<FontAwesomeIcon icon={['fas', 'triangle-exclamation']} fixedWidth />
											{message}
										</span>
									)
								})
							}
						/>
					</Form.Group>
				</Modal.Body>
				<LayoutModalFooter>
					<Button onClick={() => setShow(!show)} variant='secondary'>
						Cancel
					</Button>
					<Button type='submit'>Done</Button>
				</LayoutModalFooter>
			</Form>
		</Modal>
	)
}

export default ModalElementConfig
