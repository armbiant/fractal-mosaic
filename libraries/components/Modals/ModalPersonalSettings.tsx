// React
import {Dispatch, SetStateAction, useState} from 'react'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

// Fractal Components
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'
import Member from 'data/members/Member'
import CardSm from 'components/Card/CardSm'
import TextInput from 'components/Input/TextInput'
import PictureChooser from 'components/PictureChooser/PictureChooser'

// Fractal Client Api
import useMosaicApi from 'hooks/useMosaicApi'
import useSwrAuth from 'hooks/useSwrAuth'

// NextAuth
import {useSession} from 'next-auth/react'

interface IModalPersonalSettings {
	show: boolean
	setShow: Dispatch<SetStateAction<boolean>>
}

const ModalPersonalSettings = ({show, setShow}: IModalPersonalSettings) => {
	const {uploadUserImageToDjango, updateUsersDisplayName} = useMosaicApi()

	const {data: session} = useSession()

	const {data, error} = useSwrAuth(`${process.env.NEXT_PUBLIC_MOSAIC_API}member/${session?.uuid}/`)

	const hideModal = () => {
		setInput('')
		setShow(!!!show)
		setUserImage(undefined)
	}

	// Disable save button click during fetch
	const [isFetching, setIsFetching] = useState(false)

	const onSaveClick = () => {
		setIsFetching(true)
		if (userImage) {
			uploadUserImageToDjango(userImage)
		}
		if (input) {
			updateUsersDisplayName(input, session?.uuid as string)
		}
		setIsFetching(false)

		hideModal()
	}

	// Track value of input field
	const [input, setInput] = useState('')
	const handleRangeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setInput(e.target.value)
	}

	// State of userImage inside Picture Chooser Prop
	// Need this in the parent so the save button here can call client api method with state set from child component
	const [userImage, setUserImage] = useState<File>()

	return (
		<Modal show={show} onHide={() => hideModal()} centered>
			<LayoutModalHeader>
				Setup &ensp;
				{/* eslint-disable-next-line react/no-children-prop */}
				<Member uuid={data && data.uuid} children={<CardSm />} />
			</LayoutModalHeader>
			<Modal.Body className='rounded-0'>
				<Form onSubmit={(e) => e.preventDefault()}>
					<Form.Group className='mb-3'>
						<Form.Label>Display Name</Form.Label>
						<Member uuid={data && data.uuid}>
							<TextInput type='text' value={input} onChange={(e: any) => handleRangeChange(e as any)} />
						</Member>
					</Form.Group>
				</Form>

				<Form.Label>Profile Photo</Form.Label>
				<PictureChooser memberUuid={data && data.uuid} userImage={userImage} setUserImage={setUserImage} />
			</Modal.Body>
			<LayoutModalFooter>
				<Button onClick={() => hideModal()} variant='secondary'>
					Cancel
				</Button>
				<Button disabled={isFetching && true} onClick={() => onSaveClick()} variant='primary'>
					Save
				</Button>
			</LayoutModalFooter>
		</Modal>
	)
}

export default ModalPersonalSettings
