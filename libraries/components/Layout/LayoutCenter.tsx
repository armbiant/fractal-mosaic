// Util
import {LayoutProps} from 'types'

const LayoutCenter = ({children}: LayoutProps) => {
	return <section className='d-flex flex-column flex-grow-1 h-100 overflow-auto'>{children}</section>
}

export default LayoutCenter
