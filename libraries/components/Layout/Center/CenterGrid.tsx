// Fractal
import css from './layoutCenter.module.css'

// Util
import {LayoutProps} from 'types'

const CenterGrid = ({children}: LayoutProps) => {
	return <div className={css.centerGrid}>{children}</div>
}

export default CenterGrid
