// Util
import {LayoutProps} from 'types'

const CenterGridHeader = ({children}: LayoutProps) => {
	return <header className='top-0 d-flex p-3 gap-3 align-items-center'>{children}</header>
}

export default CenterGridHeader
