// Bootstrap
import Button from 'react-bootstrap/Button'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Util
import {LayoutMobileProps} from 'types'

const SidebarMobileToggle = ({setSidebar, icn}: LayoutMobileProps) => {
	return (
		<Button className='d-flex d-sm-none px-1' variant='link' onClick={setSidebar}>
			<FontAwesomeIcon icon={icn} fixedWidth />
		</Button>
	)
}

export default SidebarMobileToggle
