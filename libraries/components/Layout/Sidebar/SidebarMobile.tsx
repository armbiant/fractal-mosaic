// Bootstrap
import Offcanvas from 'react-bootstrap/Offcanvas'

// Util
import { SidebarProps } from 'types'


const SidebarMobile = ({ children, sidebar, setSidebar }: SidebarProps) => {

	return (

		<Offcanvas show={sidebar} onHide={setSidebar} placement="end">
			<Offcanvas.Header closeButton />
			<Offcanvas.Body className="p-0">
				{children}
			</Offcanvas.Body>
		</Offcanvas>

	)
}

export default SidebarMobile