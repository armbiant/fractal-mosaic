// Util
import { LayoutProps } from 'types'


const NavFooter = ({ children }: LayoutProps) => {
	return (
		<footer className="d-flex position-sticky bottom-0 bg-secondary gap-3">
			{children}
		</footer>
	)
}

export default NavFooter