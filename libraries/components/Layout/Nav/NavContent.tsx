// Util
import { LayoutProps } from 'types'


const NavContent = ({ children }: LayoutProps) => {
	return (
		<div className="d-flex flex-column gap-3">
			{children}
		</div>
	)
}

export default NavContent