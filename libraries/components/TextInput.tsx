// Bootstrap
import FormControl from 'react-bootstrap/FormControl'
import InputGroup from 'react-bootstrap/InputGroup'

// Util
import { TextInputProps } from '../types'

const TextInput = ({ title, children, ...rest }: TextInputProps) => {
  return (
    <InputGroup>
      <FormControl type="text" placeholder={title} {...rest} />
      {children}
    </InputGroup>
  )
}

export default TextInput
