import {useEffect, useMemo, useState} from 'react'

// Bootstrap
import Card from 'react-bootstrap/Card'
import Dropdown from 'react-bootstrap/Dropdown'
import Placeholder from 'react-bootstrap/Placeholder'
import Button from 'react-bootstrap/Button'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCircle, faGear, faUpRightFromSquare, faPowerOff} from '@fortawesome/free-solid-svg-icons'

// Fractal
import css from './card.module.css'
import DropdownItm from '../Dropdowns/DropdownItm'
import ImageCircleLg from '../Picture/PictureLg'

// Utils
import handleHealthStatus from 'util/handleHealthStatus'
import {CardProps} from 'types'
import Link from 'next/link'
import ModalElementConfig from '../Modals/ModalElementConfig'
import ModalElementAddUser from '../Modals/ModalElementAddUser'
import ModalPhotoprismConfig from '../Modals/ModalPhotoprismConfig'

// Api Util
// TODO - IMPLEMENT COMPONENT TO HAVE THIS API FUNCTION PASSED IN
// import useMosaicApi from "clientApi/clientApi"

const CardXl = ({loading, ...rest}: CardProps) => {
	if (loading) {
		return <Skeleton {...rest} />
	}

	return <Default {...rest} />
}

const Default = ({
	app,
	title,
	health,
	uuid,
	img,
	icn,
	url,
	actions,
	subtitle,
	description,
	github,
	detailPage,
	target_state,
	device_name,
	config,
	appPowerSwitch,
}: CardProps) => {
	// TODO - ACCEPT INSTALL APP METHOD AS PROPS OR REUSE SOMEHOW
	// const { installApp } = useMosaicApi()

	const [triggerElementConfig, setTriggerElementConfig] = useState(false)
	const [triggerPhotoprismConfig, setTriggerPhotoprismConfig] = useState(false)

	const handleElementModalOpen = (appUuid: string) => {
		if (isAppInstanceElement() && !config) {
			setTriggerElementConfig(true)
		}
	}

	const handlePhotoprismModalOpen = (appUuid: string) => {
		if (isAppInstancePhotoprism() && !config) {
			setTriggerPhotoprismConfig(true)
		}
	}

	const isAppInstanceElement = (): boolean => {
		return app === `${process.env.NEXT_PUBLIC_ELEMENT_APP_UUID}`
	}

	const isAppInstancePhotoprism = (): boolean => {
		return app === `${process.env.NEXT_PUBLIC_PHOTOPRISM_APP_UUID}`
	}

	const formatUrl = (url: string): string => {
		if (url?.includes('http')) {
			return url
		} else {
			return `https://${url}`
		}
	}

	// Disable app power switch during request
	const [isFetching, setIsFetching] = useState(false)

	const handleAppPowerSwitchClick = async () => {
		setIsFetching(true)
		appPowerSwitch
			? await appPowerSwitch(uuid as string, target_state === 'running' ? 'stopped' : 'running')
			: undefined
		setIsFetching(false)
	}

	// Add Element specific app action to first item in actions array
	const [elementAddUserModalShow, setElementAddUserModalShow] = useState(false)

	// Memoize adding this action so a new action isn't added every render
	useMemo(() => {
		console.log('function called')
		if (isAppInstanceElement()) {
			actions?.unshift({
				func: () => setElementAddUserModalShow(!elementAddUserModalShow),
				icn: ['fas', 'user'],
				title: 'Add user',
			})
		}
	}, [actions])

	return (
		<>
			<Card key={uuid}>
				<Card.Body className={css.cardLg__grid}>
					{/* Card Status*/}
					<div className={css.cardLg__status}>
						{health && (
							<div
								className={isFetching ? css.appPowerSwitchDivDisabled : ''}
								onClick={() => handleAppPowerSwitchClick()}>
								<FontAwesomeIcon
									icon={faPowerOff}
									className={`p-2 rounded-circle ${handleHealthStatus(health)} ${css.appPowerSwitch}`}
									fixedWidth
								/>
							</div>
						)}
						{github && github}
					</div>

					{/* Card Image / Icon*/}
					<div className={css.cardLg__img__container}>
						{img && <img src={img} className={css.cardLg__img} />}
						{icn && <ImageCircleLg icn={icn} />}
					</div>

					{/* Card title*/}
					<div className={css.cardLg__title}>
						<div className='d-flex'>
							<h3 className='mb-0'>{title}</h3>
							{device_name && <span>&nbsp; - &nbsp;{device_name}</span>}
						</div>
						{/* Url that renders for element*/}
						{isAppInstanceElement() && url && (
							<a
								onClick={() => handleElementModalOpen(app as string)}
								href={isAppInstanceElement() ? (!config ? 'javascript:void(0)' : formatUrl(url)) : formatUrl(url)}
								target={isAppInstanceElement() ? (!config ? undefined : '_blank') : '_blank'}>
								<small>{formatUrl(url)}</small>
								<FontAwesomeIcon icon={faUpRightFromSquare} className='ms-1' size='sm' fixedWidth />
							</a>
						)}
						{/* Url that renders for photoprism*/}
						{isAppInstancePhotoprism() && url && (
							<a
								onClick={() => handlePhotoprismModalOpen(app as string)}
								href={isAppInstancePhotoprism() ? (!config ? 'javascript:void(0)' : formatUrl(url)) : formatUrl(url)}
								target={isAppInstancePhotoprism() ? (!config ? undefined : '_blank') : '_blank'}>
								<small>{formatUrl(url)}</small>
								<FontAwesomeIcon icon={faUpRightFromSquare} className='ms-1' size='sm' fixedWidth />
							</a>
						)}
						{/* Url that renders for all other app instances*/}
						{!isAppInstancePhotoprism() && !isAppInstanceElement() && url && (
							<a href={formatUrl(url)} target={'_blank'}>
								<small>{formatUrl(url)}</small>
								<FontAwesomeIcon icon={faUpRightFromSquare} className='ms-1' size='sm' fixedWidth />
							</a>
						)}
						{subtitle && <p className='mb-2'>{subtitle}</p>}
					</div>

					{description && <p className='text-muted'>{description}</p>}

					{detailPage && (
						<Link href={detailPage}>
							<Button className={css.cardLg__install}>See Details</Button>
						</Link>
					)}

					{/* Gear Dropdown */}
					<div className={css.cardLg__dropdown}>
						{actions && ( // display dropdown menu, only if we have an action prop.
							<Dropdown>
								<Dropdown.Toggle variant='secondary' size='sm'>
									<FontAwesomeIcon icon={faGear} size='sm' className='text-muted' fixedWidth />
								</Dropdown.Toggle>
								<Dropdown.Menu align='end'>
									{actions.map((action) => {
										return (
											<DropdownItm icn={action.icn} title={action.title} onClick={action.func} key={action.title} />
										)
									})}
								</Dropdown.Menu>
							</Dropdown>
						)}
					</div>
				</Card.Body>
			</Card>
			<ModalElementConfig
				show={triggerElementConfig}
				setShow={setTriggerElementConfig}
				url={formatUrl(url as string)}
				app_uuid={uuid as string}
			/>
			<ModalElementAddUser
				show={elementAddUserModalShow}
				setShow={setElementAddUserModalShow}
				app_uuid={uuid as string}
			/>
			<ModalPhotoprismConfig
				show={triggerPhotoprismConfig}
				setShow={setTriggerPhotoprismConfig}
				url={formatUrl(url as string)}
				app_uuid={uuid as string}
			/>
		</>
	)
}

const Skeleton = ({health, uuid, img, icn, url, actions}: CardProps) => {
	return (
		<Card key={uuid}>
			<Placeholder as='div' animation='wave'>
				<Card.Body className={css.cardLg__grid}>
					{/* Card Status*/}
					<div className={css.cardLg__status}>
						{health && (
							<div
								className='bg-white'
								style={{
									width: '1.25rem',
									height: '1.25rem',
									borderRadius: '50%',
								}}></div>
						)}
					</div>

					{/* Card Image / Icon*/}
					<div className={css.cardLg__img__container}>
						{img && <div className='bg-white' style={{width: '4rem', height: '4rem', borderRadius: '50%'}}></div>}
						{icn && <div className='bg-white' style={{width: '4rem', height: '4rem', borderRadius: '50%'}}></div>}
					</div>

					{/* Card title*/}
					<div className={css.cardLg__title}>
						<div className='bg-white rounded w-50 mb-2' style={{height: '1.25rem'}}></div>

						{url && <div className='bg-white rounded w-75' style={{height: '0.875rem'}}></div>}
					</div>

					{/* Gear Dropdown */}
					<div className={css.cardLg__dropdown}>
						{actions && ( // display dropdown menu, only if we have an action prop.
							<div className='bg-white mx-2' style={{width: '1rem', height: '1rem', borderRadius: '50%'}}></div>
						)}
					</div>
				</Card.Body>
			</Placeholder>
		</Card>
	)
}

export default CardXl
