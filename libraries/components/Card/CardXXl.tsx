import {useState} from 'react'

// Next Auth
import {signIn} from 'next-auth/react'

// Bootstrap
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Components
import PictureLg from '../Picture/PictureLg'
import GithubStars from '../GithubStars/GithubStars'

// Style
import CardCategories from './CardCategories'
import css from './card.module.css'

// Utils
import {ImgType, LoadingType, SessionType, TitleType, UrlType, UuidType} from 'types'

// Mosaic Api
import useMosaicApi from 'hooks/useMosaicApi'
import SelectDevice from '../Modals/SelectDevice'
import ModalDeviceInstall from '../Modals/ModalDeviceInstall'

interface CardXXlProps {
	isInstalled: boolean
	useSession: SessionType
	title: TitleType
	subtitle: TitleType
	description: string
	githubStars: number
	githubUrl: UrlType
	logo: ImgType
	img: ImgType
	links: {
		linkName: string
		linkUrl: UrlType
	}[]
	categories: {
		category: string
	}[]
	uuid?: UuidType
	loading?: LoadingType
}

const CardXXl = ({loading, ...rest}: CardXXlProps) => {
	if (loading) {
		return <Skeleton {...rest} />
	}
	return <Default {...rest} />
}

const Default = ({
	isInstalled,
	useSession,
	title,
	subtitle,
	description,
	githubStars,
	githubUrl,
	logo,
	img,
	links,
	categories,
	uuid,
}: CardXXlProps) => {
	const {data: session, status} = useSession()
	const {installApp} = useMosaicApi()

	const [installText, setInstallText] = useState<'Install' | 'Installed'>(isInstalled ? 'Installed' : 'Install')
	const [isAppInstalling, setIsAppInstalling] = useState(false)

	// State for select device modal
	const [showDeviceModal, setShowDeviceModal] = useState(false)

	// State for add device modal
	const [addDeviceModal, setAddDeviceModal] = useState(false)

	return (
		<>
			<Card key={uuid} body>
				<section className={css.cardXXl__grid}>
					<header className={css.cardXXl__header}>
						<PictureLg img={logo} circle={false} />
						<div className={css.cardXXl__titleText}>
							<h1 className={css.cardXXl__title}>{title}</h1>
							<h4 className={css.cardXXl__subtitle}>{subtitle}</h4>
						</div>
					</header>

					<div className={css.cardXXl__cta}>
						{status === 'authenticated' && (
							<div className={css.cardXXl__groupSelect}>
								<Button
									disabled={isInstalled || installText === 'Installed' ? true : false}
									onClick={() => setShowDeviceModal((currState) => !currState)}>
									{isAppInstalling ? 'Loading...' : installText}
								</Button>
							</div>
						)}
						{status === 'unauthenticated' && (
							<Button onClick={() => signIn('keycloak', {}, {prompt: 'login'})}>Sign In To Install</Button>
						)}
					</div>

					<div className={css.cardXXl__tagList}>
						<span>Categories:</span>
						{categories.map((category, index) => {
							return <CardCategories title={category.category} uuid={index as unknown as string} />
						})}
					</div>

					<div className={css.cardXXl__githubStars}>
						<GithubStars stars={githubStars} url={githubUrl} />
					</div>

					<figure className={css.cardXXl__hero}>
						<img src={img} />
					</figure>

					<figcaption className={css.cardXXl__description}>{description}</figcaption>

					<div className={css.cardXXl__menu}>
						<h6>Resources:</h6>
						{links.flatMap(({linkName, linkUrl}) => {
							return (
								<a href={linkUrl} className={css.cardXXl__menuItem} target='_blank' key={linkName}>
									<span>{linkName}</span>
									<FontAwesomeIcon icon={['fas', 'arrow-up-right-from-square']} size='sm' fixedWidth />
								</a>
							)
						})}
					</div>
				</section>
			</Card>

			{/* Modals */}
			<SelectDevice
				userUuid={session?.uuid}
				title={title}
				show={showDeviceModal}
				onHide={() => setShowDeviceModal((currState) => !currState)}
				appUuid={uuid as string}
				showDeviceModal={addDeviceModal}
				setShowDeviceModal={setAddDeviceModal}
			/>
			<ModalDeviceInstall show={addDeviceModal} setShow={setAddDeviceModal} />
		</>
	)
}

const Skeleton = ({...rest}: CardXXlProps) => {
	return null
}

export default CardXXl
