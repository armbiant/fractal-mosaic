import React, {useEffect, useState} from 'react'

// Bootstrap
import Button from 'react-bootstrap/Button'

import css from './card.module.css'

// Mosaic Api
import {useSession} from 'next-auth/react'
import SelectDevice from '../Modals/SelectDevice'
import ModalDeviceInstall from '../Modals/ModalDeviceInstall'
import useSwrAuth from 'hooks/useSwrAuth'
import {FRONTEND_APP_CHECK_ENDPOINT} from 'util/endpoints'

interface IAppData {
	uuid: string
	image: string
	github_url: string
	content: string
	subtitle: string
	description: string
	name: string
}

const CardFeatureApp = React.forwardRef(
	({uuid, image, github_url, content, subtitle, description, name}: IAppData, ref) => {
		// Data to check whether or not user has app installed or not.
		// Disables button if they have the app
		const {data, error} = useSwrAuth(`${FRONTEND_APP_CHECK_ENDPOINT(uuid)}`)

		const {data: session, status} = useSession()
		// State for select device modal
		const [showDeviceModal, setShowDeviceModal] = useState(false)

		// State for add device modal
		const [addDeviceModal, setAddDeviceModal] = useState(false)

		useEffect(() => {
			if (ref) {
				setTimeout(() => {
					// @ts-ignore
					ref.current.scrollIntoView({behavior: 'smooth'})
				}, 750)
			}
		}, [])
		return (
			<>
				<div
					// @ts-ignore
					ref={ref}
					className={`d-flex flex-column align-items-center justify-content-between ${css.cardFeatureApp}`}>
					<h3 className='m-0'>{name}</h3>
					<div className={css.cardLg__img__container}>
						<img src={image} alt='app icon' className={css.cardLg__img} />
					</div>
					<Button disabled={data && true} onClick={() => setShowDeviceModal(true)}>
						{data ? 'Deployed' : 'Deploy'}
					</Button>
				</div>
				{/* Modals */}
				<SelectDevice
					userUuid={session?.uuid as string}
					title={name}
					show={showDeviceModal}
					onHide={() => setShowDeviceModal((currState) => !currState)}
					appUuid={uuid as string}
					showDeviceModal={addDeviceModal}
					setShowDeviceModal={setAddDeviceModal}
				/>
				<ModalDeviceInstall show={addDeviceModal} setShow={setAddDeviceModal} />
			</>
		)
	}
)

export default CardFeatureApp
