// Bootstrap
import ListGroup from 'react-bootstrap/ListGroup'
import Dropdown from 'react-bootstrap/Dropdown'
import Placeholder from 'react-bootstrap/Placeholder'

// Icons
import {faCircle, faGear, faPlus} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Fractal Components
import css from './card.module.css'
import PictureMd from '../Picture/PictureMd'
import DropdownItm from '../Dropdowns/DropdownItm'

// Utils
import {CardProps} from 'types'
import handleHealthStatus from 'util/handleHealthStatus'

const CardMd = ({loading, ...rest}: CardProps) => {
	if (loading) {
		return <Skeleton {...rest} />
	}

	return <Default {...rest} />
}

const Default = ({title, health, uuid, img, icn, actions, addDeviceButton}: CardProps) => {
	return (
		<ListGroup.Item key={uuid} className={css.cardMd}>
			{img ? (
				<PictureMd img={img} icn={icn} health={health} />
			) : (
				<div className='d-flex align-items-center fs-5'>
					<FontAwesomeIcon
						icon={!addDeviceButton ? faCircle : faPlus}
						className={!addDeviceButton ? handleHealthStatus(health) : 'text-dark'}
						size={!addDeviceButton ? 'sm' : 'lg'}
						fixedWidth
					/>
				</div>
			)}

			<div className='me-auto'>{title}</div>

			{actions && ( // display dropdown menu, only if we have an action prop.
				<Dropdown>
					<Dropdown.Toggle variant='secondary' size='sm'>
						<FontAwesomeIcon icon={faGear} size='sm' className='text-muted' fixedWidth />
					</Dropdown.Toggle>
					<Dropdown.Menu align='end'>
						{/* map array of actions to dropdown menu. */}
						{actions.map((action) => {
							return <DropdownItm icn={action.icn} title={action.title} onClick={action.func} key={action.title} />
						})}
					</Dropdown.Menu>
				</Dropdown>
			)}
		</ListGroup.Item>
	)
}

const Skeleton = ({img, uuid, health, actions}: CardProps) => {
	return (
		<ListGroup.Item key={uuid} className={css.cardMd}>
			{img ? (
				<Placeholder as='div' animation='wave'>
					<div className='bg-white' style={{width: '2rem', height: '2rem', borderRadius: '50%'}}></div>
				</Placeholder>
			) : (
				<Placeholder as='div' animation='wave'>
					<div
						className='bg-white'
						style={{
							width: '0.875rem',
							height: '0.875rem',
							borderRadius: '50%',
						}}></div>
				</Placeholder>
			)}

			<Placeholder as='div' animation='wave' className='w-100'>
				<Placeholder className='bg-white align-self-center fs-5 rounded w-100' style={{minWidth: '8ch'}} />
			</Placeholder>

			{actions && (
				<Placeholder as='div' animation='wave'>
					<div
						className='bg-white mx-2'
						style={{
							width: '0.875rem',
							height: '0.875rem',
							borderRadius: '50%',
						}}></div>
				</Placeholder>
			)}
		</ListGroup.Item>
	)
}

export default CardMd
