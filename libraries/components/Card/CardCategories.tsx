// Bootstrap
import Card from 'react-bootstrap/Card'
import Placeholder from 'react-bootstrap/Placeholder'

// Fractal
import css from './card.module.css'

// Utils
import {CardProps} from 'types'

const CardCategories = ({loading, ...rest}: CardProps) => {
	if (loading) {
		return <Skeleton {...rest} />
	}

	return <Default {...rest} />
}

const Default = ({title, uuid}: CardProps) => {
	return (
		<Card className={css.cardSm} key={uuid}>
			<span className='text-muted'>{title}</span>
		</Card>
	)
}

const Skeleton = ({title, uuid}: CardProps) => {
	return (
		<Card className={css.cardSm} key={uuid}>
			{title && (
				<Placeholder as='div' animation='wave'>
					<div className='bg-secondary' style={{width: '1rem', height: '1rem', borderRadius: '50%'}}></div>
				</Placeholder>
			)}
		</Card>
	)
}

export default CardCategories
