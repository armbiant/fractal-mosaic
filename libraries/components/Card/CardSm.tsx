// Bootstrap
import Card from 'react-bootstrap/Card'
import Placeholder from 'react-bootstrap/Placeholder'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faCircle} from '@fortawesome/free-solid-svg-icons'

// Fractal
import css from './card.module.css'
import PictureSm from '../Picture/PictureSm'

// Utils
import {CardProps} from 'types'
import handleHealthStatus from 'util/handleHealthStatus'
import FractalAvatar from '../Picture/FractalAvatar'

const CardSm = ({loading, ...rest}: CardProps) => {
	if (loading) {
		return <Skeleton {...rest} />
	}

	return <Default {...rest} />
}

const Default = ({title, health, uuid, img, icn, noCircle}: CardProps) => {
	return (
		<Card className={css.cardSm} key={uuid}>
			{icn && <PictureSm icn={icn} />}

			{img ? <PictureSm img={img} circle={!noCircle} /> : <FractalAvatar size='18' />}

			<span className='text-muted'>{title}</span>

			{health && (
				<small>
					<FontAwesomeIcon icon={faCircle} className={handleHealthStatus(health)} size='sm' fixedWidth />
				</small>
			)}
		</Card>
	)
}

const Skeleton = ({health, uuid, img, icn}: CardProps) => {
	return (
		<Card className={css.cardSm} key={uuid}>
			{img && (
				<Placeholder as='div' animation='wave'>
					<div className='bg-secondary' style={{width: '1rem', height: '1rem', borderRadius: '50%'}}></div>
				</Placeholder>
			)}

			{icn && (
				<Placeholder as='div' animation='wave'>
					<div
						className='bg-secondary'
						style={{
							width: '0.875rem',
							height: '0.875rem',
							borderRadius: '50%',
						}}></div>
				</Placeholder>
			)}

			<Placeholder as='div' animation='wave'>
				<Placeholder className='text-secondary align-self-center fs-5 rounded w-100' style={{minWidth: '8ch'}} />
			</Placeholder>

			{health && (
				<Placeholder as='div' animation='wave'>
					<div
						className='bg-secondary'
						style={{
							width: '0.875rem',
							height: '0.875rem',
							borderRadius: '50%',
						}}></div>
				</Placeholder>
			)}
		</Card>
	)
}

export default CardSm
