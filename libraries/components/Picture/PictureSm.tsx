// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Fractal
import css from './picture.module.css'

// Util
import { PictureProps } from 'types'

const PictureSm = ({ img, icn, circle = true, ...rest }: PictureProps) => {
  return (
    <div className={`${css.picture__sm} ${circle && css.picture__circle}`}>
      {img && (
        <img
          src={img}
          className={`${css.picture__img} ${circle && css.picture__circle}`}
        />
      )}
      {icn && <FontAwesomeIcon icon={icn} size="sm" fixedWidth {...rest} />}
    </div>
  )
}

export default PictureSm
