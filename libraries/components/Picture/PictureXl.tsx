// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Fractal
import css from './picture.module.css'

// Util
import {PictureProps} from 'types'

const PictureXl = ({img, icn, circle = true, ...rest}: PictureProps) => {
	return (
		<div className={`${css.picture__xl} ${circle && css.picture__circle}`}>
			{icn && <FontAwesomeIcon icon={icn} size='lg' fixedWidth {...rest} />}
			{img && <img src={img} className={css.picture__img} />}
		</div>
	)
}

export default PictureXl
