// NextAuth
import {useSession} from 'next-auth/react'

import Avatar from 'react-avatar'

interface IAvatarProps {
	size?: string
}

const FractalAvatar = ({size}: IAvatarProps) => {
	const {data: session} = useSession()

	return <Avatar name={session?.name as string} size={size ? size : '35'} round='50px' />
}

export default FractalAvatar
