import Placeholder from 'react-bootstrap/Placeholder'
import { CenterTitleProps } from 'types'

const CenterTitle = ({ loading, ...rest }: CenterTitleProps) => {
  if (loading) return <Skeleton />

  return <Default {...rest} />
}

const Default = ({ title }: CenterTitleProps) => {
  return <h1 className="me-auto my-0">{title}</h1>
}

const Skeleton = () => {
  return (
    <Placeholder
      animation="wave"
      className="flex-grow-1 me-auto rounded"
      style={{ maxWidth: '30ch' }}
    >
      <div
        className="m-0 bg-secondary w-100"
        style={{ height: '2.375rem' }}
      ></div>
    </Placeholder>
  )
}

export default CenterTitle
