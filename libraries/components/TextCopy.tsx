import {useRef, useEffect} from 'react'

// Bootstrap
import FormControl from 'react-bootstrap/FormControl'
import InputGroup from 'react-bootstrap/InputGroup'
import Button from 'react-bootstrap/Button'
import Placeholder from 'react-bootstrap/Placeholder'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Util
import {TextInputProps} from '../types'
import useCopyToClipboard from 'hooks/useCopyToClipboard'

const TextCopy = ({loading, ...rest}: TextInputProps) => {
	if (loading) {
		return <Skeleton {...rest} />
	}

	return <Default {...rest} />
}

const Default = ({title, disabled, code, rtl, ...rest}: TextInputProps) => {
	const [isCopied, handleCopy] = useCopyToClipboard(1750)

	// If the user entered a device name, scroll to the end of the input field
	const formControlRef = useRef<HTMLInputElement>(null)
	const scrollToEnd = () => {
		const node = formControlRef.current
		if (node) {
			if (rtl) node.scrollLeft = node.scrollWidth - node.clientWidth
			else node.scrollLeft = 0
		}
	}
	useEffect(scrollToEnd, [title])

	return (
		<InputGroup>
			<FormControl
				className={code ? 'code p-0' : 'p-0'}
				type='text'
				value={title}
				aria-label={title}
				ref={formControlRef}
				readOnly
				disabled
				{...rest}
			/>
			<InputGroup.Text className='p-0'>
				<Button onClick={() => handleCopy(title)} variant='link' className='bg-transparent' disabled={disabled}>
					<FontAwesomeIcon
						icon={isCopied ? ['fas', 'check'] : ['fas', 'copy']}
						className={isCopied ? 'text-success' : 'text-muted'}
						size='sm'
						fixedWidth
					/>
				</Button>
			</InputGroup.Text>
		</InputGroup>
	)
}

const Skeleton = () => {
	return (
		<Placeholder as='div' animation='wave'>
			<div className='w-100 bg-white rounded' style={{height: '40px'}}></div>
		</Placeholder>
	)
}

export default TextCopy
