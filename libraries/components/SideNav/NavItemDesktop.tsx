import Link from 'next/link'

// Bootstrap
import Tooltip from 'react-bootstrap/Tooltip'
import NavLink from 'react-bootstrap/NavLink'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Placeholder from 'react-bootstrap/Placeholder'

// Custom Fractal Components
import PictureLg from '../Picture/PictureLg'
import css from './navlink.module.css'

// Util
import useNavLink from 'hooks/useNavLink'
import {NavItemProps} from 'types'
import FractalAvatar from '../Picture/FractalAvatar'

const NavItemDesktop = ({loading, ...rest}: NavItemProps) => {
	if (loading) {
		return <Skeleton {...rest} />
	}

	return <Default {...rest} />
}

const Default = ({title, icn, img, uuid, linkTo, isActive, action}: NavItemProps) => {
	const handleLink = useNavLink(uuid, linkTo, action)

	return (
		<Link href={handleLink} as={handleLink} passHref>
			<NavLink
				className={`${css.navLink} ${linkTo == 'logOut' && 'text-danger'} ${isActive && 'text-black'}`}
				onClick={action ? action : undefined}
				key={uuid ? uuid : undefined}>
				{/* If 'logOut' is true, add the "text-danger" class and rotate the icon 180 degrees */}
				<OverlayTrigger
					overlay={
						<Tooltip>
							<h3 className='m-0'>{title}</h3>
						</Tooltip>
					}
					placement='right'>
					{/* TODO: Pass  */}
					<div>
						{img ? (
							<PictureLg img={img} icn={icn} flip={linkTo == 'logOut' ? 'horizontal' : undefined} isActive={isActive} />
						) : (
							<FractalAvatar size='40' />
						)}
					</div>
				</OverlayTrigger>
			</NavLink>
		</Link>
	)
}

const Skeleton = ({uuid}: NavItemProps) => {
	return (
		<Placeholder as='div' animation='wave' key={uuid}>
			<div className='bg-white' style={{width: '3rem', height: '3rem', borderRadius: '50%'}}></div>
		</Placeholder>
	)
}

export default NavItemDesktop
