// Next
import Link from 'next/link'

// Bootstrap
import NavLink from 'react-bootstrap/NavLink'
import Placeholder from 'react-bootstrap/Placeholder'

//Custom Fractal Components
import PictureSm from '../Picture/PictureSm'
import css from './MonoMobileNavItem.module.css'

// Util
import useNavLink from 'hooks/useNavLink'
import { MonoNavItemProps } from 'types'

const MonoMobileNavItem = ({ loading, ...rest }: MonoNavItemProps) => {
  if (loading) {
    return <Skeleton {...rest} />
  }

  return <Default {...rest} />
}

const Default = ({
  title,
  icn,
  img,
  uuid,
  linkTo,
  isActive,
  menuPress,
  action,
}: MonoNavItemProps) => {
  const handleLink = useNavLink(uuid, linkTo, action)

  const handleOnClick = () => {
    if (action) {
      action()
    }
    menuPress
  }

  return (
    <Link href={handleLink} as={handleLink} passHref>
      <NavLink
        className={`${css.navitem} ${isActive && css.navitem__active} py-1`}
        onClick={handleOnClick}
        href={uuid ? `/group/${uuid}` : undefined}
        key={uuid ? uuid : undefined}
      >
        <PictureSm
          img={img}
          icn={icn}
          flip={linkTo == 'logOut' ? 'horizontal' : undefined}
        />
        <p className={`m-0 ${isActive && 'text-black'}`}>{title}</p>
      </NavLink>
    </Link>
  )
}

const Skeleton = ({ uuid }: MonoNavItemProps) => {
  return (
    <Placeholder
      as="div"
      animation="wave"
      className="d-flex align-items-center gap-2"
      key={uuid}
    >
      <div
        className="bg-white"
        style={{ width: '2rem', height: '2rem', borderRadius: '50%' }}
      ></div>
      <h3
        className="bg-white m-0 rounded w-50"
        style={{ height: '1.5rem' }}
      ></h3>
    </Placeholder>
  )
}

export default MonoMobileNavItem
