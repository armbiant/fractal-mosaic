// Bootstrap
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// Util
import { MonoNavProps } from 'types'

//Styles 
import css from './MonoMobileNav.module.css'

const MonoMobileNav = ({currentDomain}:MonoNavProps) => {

	return (
		<Navbar bg="white" className={css.mobilenav}>
			<Nav className={css.mobilenav__container}>
				<Nav.Link 
				className={`${css.mobilenav__link} ${currentDomain==="Home" && css.mobilenav__linkactive}`} 
				href={process.env.NEXT_PUBLIC_HOMEPAGE_BASE_URL}>
					<FontAwesomeIcon 
					icon={["fas","house"]}
					size="sm" 
					fixedWidth />
					Home
				</Nav.Link>
				<Nav.Link 
				className={`${css.mobilenav__link} ${currentDomain==="Store" && css.mobilenav__linkactive}`}
				href={process.env.NEXT_PUBLIC_STORE_BASE_URL}>
					<FontAwesomeIcon icon={["fas","store"]}
					size="sm" 
					fixedWidth/>
					Store
				</Nav.Link>
				<Nav.Link 
				className={`${css.mobilenav__link} ${currentDomain==="Docs" && css.mobilenav__linkactive}`}
				href="https://docs.fractalnetworks.co" 
				target="_blank">
					<FontAwesomeIcon 
					icon={["fas","book"]}
					size="sm" 
					fixedWidth/>
					Docs
				</Nav.Link>
				<Nav.Link  
				className={`${css.mobilenav__link} ${currentDomain==="Console" && css.mobilenav__linkactive}`}
				href={process.env.NEXT_PUBLIC_CONSOLE_BASE_URL}>
					<FontAwesomeIcon 
					icon={["fas","gauge"]}
					size="sm" 
					fixedWidth/>
					Console
				</Nav.Link>
			</Nav>
		</Navbar>
	)
}

export default MonoMobileNav