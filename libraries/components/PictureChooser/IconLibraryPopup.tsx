// React
import React from 'react'

// Bootstrap
import Form from 'react-bootstrap/Form'
import ListGroup from 'react-bootstrap/ListGroup'
import Button from 'react-bootstrap/Button'
import PopoverBody from 'react-bootstrap/PopoverBody'
import PopoverHeader from 'react-bootstrap/PopoverHeader'
import Pagination from 'react-bootstrap/Pagination'

// Font Awesome
import { fas, IconName, IconPrefix } from '@fortawesome/free-solid-svg-icons'
import { fab } from '@fortawesome/free-brands-svg-icons'

// Utils
import handleQuery from 'util/iconLibraryHelpers'
import {
  iconCategoriesList,
  iconCategoriesListType,
} from 'util/iconCategoriesList'

// Fractal Components
import css from './iconLibrary.module.css'
import PictureMd from '../Picture/PictureMd'

type FontAwesomeGraphQlData = {
  id: IconName
  label: string
  membership: {
    free: string[] | undefined[]
  }
}

type IconList = {
  prefix: IconPrefix
  iconName: IconName
}[]

// TODO: size of default list is causing slow down. Redesign to paginate
// TODO: defaultIcons and query results should be in the same format. That will reduce alot of conditionals and duplicate logic.

const IconLibraryPopup = ({
  handleSelectedIcon,
}: {
  handleSelectedIcon: (icon: [IconPrefix, IconName]) => void
}) => {
  // Font Awesome icon library
  const libraries = { ...fab, ...fas }
  // Convert to array of objects. Cut uneeded parts of object.
  const defaultIcns: IconList = Object.keys(libraries)
    .map((key) => libraries[key])
    .map(({ icon, ...rest }) => {
      return rest
    })
  console.log(defaultIcns)

  const defaultIcons = { ...fab, ...fas }
  // Search input
  const [input, setInput] = React.useState('')
  // Categories
  const [categories, setCategories] = React.useState<string[]>([])
  // Formated query string from combining input and categories selected
  const [query, setQuery] = React.useState('')
  // Icon data we receive back from font awesome api
  const [results, setResults] = React.useState<FontAwesomeGraphQlData[]>([])
  const [resultIds, setResultIds] = React.useState<string[]>()

  // Add / remove categories
  const addToCategory = (category: string) =>
    setCategories((state) => [...state, category])
  const removeFromCategory = (category: string) => {
    const filteredCategories = categories.filter((item) => {
      return item !== category
    })
    setCategories([...filteredCategories])
  }

  // Update Query 1.25s after Input changes
  React.useEffect(() => {
    let timer = setTimeout(() => {
      updateQuery()
    }, 1250)
    return () => clearTimeout(timer)
  }, [input])

  // Update Query when Categories change
  React.useEffect(() => updateQuery(), [categories])

  // Combine Input + Categories to create Query
  const updateQuery = () => setQuery(input + ' ' + categories.join(' ').trim())

  // Call API when Query changes
  React.useEffect(() => {
    handleQuery(query).then((data) => setResults(data.data.search))
  }, [query])

  // Arrays used for checking category matches
  React.useEffect(() => {
    // uses search results
    if (results.length >= 1)
      setResultIds(
        results
          .filter((icon) => icon.membership.free.length > 0)
          .map((item) => item.id)
      )
    // uses default icons
    else {
      setResultIds(Object.values(defaultIcons).map((item) => item.iconName))
    }
  }, [results])

  // format results from Font Awesome API into icon format
  function formatIconName(
    icon: FontAwesomeGraphQlData
  ): [IconPrefix, IconName] {
    if (icon.membership.free[0] === 'brands') return ['fab', icon.id]
    if (icon.membership.free[0] === 'solid') return ['fas', icon.id]
    console.error('not fab or fas', icon)
    return ['fas', 'triangle-exclamation']
  }

  // pagination
  const pageClicked = () => {
    return 3
  }

  function iconList() {
    // TODO: size of default list is causing slow down. Redesign to paginate
    if (results.length == 0 && query == ' ') {
      return Object.keys(defaultIcons).map((key) => {
        const formattedIcon: [IconPrefix, IconName] = [
          defaultIcons[key].prefix,
          defaultIcons[key].iconName,
        ]
        return (
          <Button
            onClick={() => handleSelectedIcon(formattedIcon)}
            key={key}
            variant="secondary"
            className="bg-transparent border-0 p-0 m-0 rounded-pill"
          >
            <PictureMd icn={formattedIcon} />
          </Button>
        )
      })
    }
    // Search Results
    const filteredIcons = results.filter(
      (icon) => icon.membership.free.length > 0
    )
    return filteredIcons.map((icon) => {
      return (
        <Button
          onClick={() => handleSelectedIcon(formatIconName(icon))}
          key={icon.id}
          variant="secondary"
          className={css.iconItem}
        >
          <PictureMd icn={formatIconName(icon)} />
        </Button>
      )
    })
  }

  function categoriesList() {
    // key = name of a category (e.g. accessibility)
    return Object.keys(iconCategoriesList as iconCategoriesListType).map(
      (key) => {
        // array of iconIds shared by search results and this this category
        let shared: string[] = iconCategoriesList[
          key as keyof typeof iconCategoriesList
        ].icons.filter((item: string) => resultIds?.indexOf(item) !== -1)

        // if there are no matches, do nothing
        if (shared.length < 1) {
          return null
        }

        // If there are matches, display a category for this
        return (
          <IconCategory
            key={key}
            results={shared.length}
            category={key}
            addToCategory={addToCategory}
            removeFromCategory={removeFromCategory}
          >
            {' '}
            {
              iconCategoriesList[key as keyof typeof iconCategoriesList].label
            }{' '}
          </IconCategory>
        )
      }
    )
  }

  return (
    <PopoverBody className={css.container}>
      <PopoverHeader className={css.search}>
        <Form.Control
          size="sm"
          type="text"
          name="search input"
          value={input}
          onChange={(e: any) => setInput(e.target.value)}
          placeholder="Search Icons"
          aria-label="Search Icons"
          aria-describedby="Search Icons"
        />
      </PopoverHeader>
      <div className={css.categories}>
        <h5 className={css.categoriesHeader}>Categories</h5>
        <ListGroup as="ul" variant="flush">
          {categoriesList()}
        </ListGroup>
      </div>
      <div className={css.icons}>
        <div className={css.iconContainer}>{iconList()}</div>
        {(results.length >= 20 || results.length <= 0) && (
          <IconPagination
            currentPage={1}
            totalPages={14}
            pageClicked={pageClicked}
          />
        )}
      </div>
    </PopoverBody>
  )
}

export default IconLibraryPopup

interface IconCategoryProps {
  children: any
  category: string
  results: number
  addToCategory: (category: string) => void
  removeFromCategory: (category: string) => void
}

const IconCategory = ({
  children,
  category,
  addToCategory,
  removeFromCategory,
  results = 0,
}: IconCategoryProps) => {
  const [active, setActive] = React.useState(false)
  const handleClick = () => {
    setActive((state) => !state)

    if (!active) addToCategory(category)

    if (active) removeFromCategory(category)
  }

  return (
    <ListGroup.Item
      onClick={() => handleClick()}
      active={active}
      className={css.categoryItem}
      as="li"
    >
      <div>{children}</div>
      <div className="text-muted ms-1">({results})</div>
    </ListGroup.Item>
  )
}

interface IconPaginationProps {
  totalPages: number
  currentPage: number
  pageClicked: (page: number | string) => void
}

const IconPagination = ({
  totalPages,
  currentPage,
  pageClicked,
}: IconPaginationProps) => {
  return (
    <Pagination className={css.iconPagination}>
      {currentPage > 0 && <Pagination.Prev />}
      <Pagination.Item active>{currentPage}</Pagination.Item>
      {currentPage < totalPages && <Pagination.Next />}
    </Pagination>
  )
}
