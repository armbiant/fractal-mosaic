// React
import {Dispatch, SetStateAction, useState} from 'react'

// Bootstrap
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Popover from 'react-bootstrap/Popover'
import {ButtonProps} from 'react-bootstrap'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {IconName, IconPrefix} from '@fortawesome/free-solid-svg-icons'

//Fractal
import IconLibraryPopup from './IconLibraryPopup'
import PictureXl from '../Picture/PictureXl'
import Member from 'data/members/Member'
import Group from 'data/groups/Group'

// Util
import useImageUpload from 'hooks/useImageUpload'

// NextAuth
import useSwrAuth from 'hooks/useSwrAuth'
import {useSession} from 'next-auth/react'
import FractalAvatar from '../Picture/FractalAvatar'

interface PictureChooserProps {
	memberUuid?: string
	groupUuid?: string
	userImage?: File | undefined
	setUserImage?: Dispatch<SetStateAction<File | undefined>>
}

const PictureChooser = ({memberUuid, groupUuid, userImage, setUserImage}: PictureChooserProps) => {
	const {data: session} = useSession()

	const {data, error} = useSwrAuth(`${process.env.NEXT_PUBLIC_MOSAIC_API}member/${session?.uuid}/`)

	const img: string | undefined = data && data.profile_image ? data.profile_image : undefined

	// Image Hook. "selectedImg" is the image that saves to the backend
	const {selectedImg, inputRef, showOpenFileDialog, setImgFile, resetImg} = useImageUpload()
	const handleImgBtnClick = () => {
		// If the user chooses an image, set icon to empty.
		setSelectedIcon(undefined)

		// Then, open the file dialog
		showOpenFileDialog()
	}

	// Icon Hook
	const [selectedIcon, setSelectedIcon] = useState<[IconPrefix, IconName]>()
	const handleIcnBtnClick = (icon: [IconPrefix, IconName]) => {
		// If the user chooses an icon, remove any selected images
		resetImg()

		// Then, continue
		setSelectedIcon(icon)
	}

	// ****************************
	// ****************************
	// ****************************
	// Logic and state for image upload to django component
	// ****************************
	// ****************************
	// ****************************

	// Image to be posted to backend
	// const [userImage, setUserImage] = useState<File>()
	// Base 64 image for placeholder state in frontend
	const [base64Img, setBase64Img] = useState<string>('')

	const handleImageUploadOnChange = (e: any) => {
		if (e.target.files) {
			let imgFile = e?.target?.files[0]
			const reader = new FileReader()

			reader.readAsDataURL(imgFile)

			reader.onload = () => setBase64Img(reader.result as string)
			if (setUserImage) setUserImage(e.target.files[0])
		}
	}

	return (
		<div className='d-flex align-items-center'>
			{/* Show the new icon/image, otherwise show the existing one*/}
			{userImage || img ? (
				<PictureXl icn={selectedIcon} img={base64Img ? base64Img : img} circle />
			) : (
				<FractalAvatar size='70' />
			)}
			<Form.Group className='ms-3'>
				{/* Show the icon option as long as this isn't a member page */}
				{!memberUuid && <IcnBtn handleSelectedIcon={handleIcnBtnClick} />}
				{/* <ImgBtn onClick={handleImgBtnClick} /> */}
				<Button variant='secondary' onChange={(e: any) => handleImageUploadOnChange(e)}>
					<Form.Control ref={inputRef} type='file' />
				</Button>
				{/* <Button onClick={() => handleUserImageUpload()}>Save</Button> */}
				{/* <input ref={inputRef} onChange={setImgFile} type='file' className='visually-hidden' /> */}
			</Form.Group>
		</div>
	)
}

const IcnBtn = ({handleSelectedIcon}: any) => {
	const [popoverShown, setState] = useState(false)

	const handleClick = () => {
		setState(!popoverShown)
	}

	return (
		<OverlayTrigger
			overlay={
				<Popover className='b-0' style={{maxWidth: '20rem', width: '20rem'}}>
					<IconLibraryPopup handleSelectedIcon={handleSelectedIcon} />
				</Popover>
			}
			show={popoverShown}
			placement='auto'>
			<Button variant='secondary' onClick={handleClick}>
				<FontAwesomeIcon
					icon={popoverShown ? ['fas', 'xmark'] : ['fas', 'icons']}
					size='sm'
					className='me-2'
					fixedWidth
				/>
				Choose Icon
			</Button>
		</OverlayTrigger>
	)
}

const ImgBtn = ({onClick}: ButtonProps) => {
	return (
		<Button variant='secondary' onClick={onClick}>
			<FontAwesomeIcon icon={['fas', 'file-arrow-up']} size='sm' className='me-2' fixedWidth />
			Upload Image
		</Button>
	)
}

export default PictureChooser
