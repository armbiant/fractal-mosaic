// Bootstrap
import DropdownItem from 'react-bootstrap/DropdownItem'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Util
import {DropdownItmProps} from 'types'

const DropdownItm = ({title, icn, ...rest}: DropdownItmProps) => {
	return (
		<DropdownItem {...rest}>
			{icn && <FontAwesomeIcon className='pe-1' icon={icn} fixedWidth />}
			{title}
		</DropdownItem>
	)
}

export default DropdownItm
