export const APP_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}app/`
export const APP_LIST_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}app/?installed=True`
export const APP_CHECK_ENDPOINT = (APP_UUID: string) =>
	`${process.env.HIVE_BACKEND_API_SERVICE_URL}/app/${APP_UUID}/app-name/`
export const FRONTEND_APP_CHECK_ENDPOINT = (APP_UUID: string) =>
	`${process.env.NEXT_PUBLIC_MOSAIC_API}app/${APP_UUID}/app-name/`
export const CATALOG_APP_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}app/`
export const FEATURED_APPS_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}catalog/app/?featured=True`
export const CATALOG_APP_LIST_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}app/?installed=False`
export const GENERATE_TOKEN_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}token/`
export const DEVICE_LIST_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}device/`
export const MEMBER_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}member/`
export const MEMBER_JOIN_ENDPOINT = `${process.env.HIVE_BACKEND_API_SERVICE_URL}/member/join/`
export const MEMBER_PASSPHRASE_ENDPOINT = (USER_UUID: string) =>
	`${process.env.NEXT_PUBLIC_MOSAIC_API}member/${USER_UUID}/passphrase/`
export const APP_INSTANCE_CONFIG_ENDPOINT = `${process.env.NEXT_PUBLIC_MOSAIC_API}appinstance_config/`
export const ADD_ELEMENT_USER_ENDPOINT = (appInstanceUuid: string) =>
	`${process.env.NEXT_PUBLIC_MOSAIC_API}app/${appInstanceUuid}/element/`
export const BILLING_PAYMENT_INTENT = `${process.env.NEXT_PUBLIC_MOSAIC_API}billing/payment_intent/`
