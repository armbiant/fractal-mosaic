// React
import { useEffect } from "react";

// DARK MODE

let rootVariables = [
  {
    cssVar: "--fn-white",
    cssValue: "",
  },
  {
    cssVar: "--fn-gray-100",
    cssValue: "",
  },
  {
    cssVar: "--fn-gray-200",
    cssValue: "",
  },
  {
    cssVar: "--fn-gray-300",
    cssValue: "",
  },
  {
    cssVar: "--fn-gray-400",
    cssValue: "",
  },
  {
    cssVar: "--fn-gray-600",
    cssValue: "",
  },
  {
    cssVar: "--fn-gray-700",
    cssValue: "",
  },
  {
    cssVar: "--fn-gray-800",
    cssValue: "",
  },
  {
    cssVar: "--fn-gray-900",
    cssValue: "",
  },
  {
    cssVar: "--fn-black",
    cssValue: "",
  },
];

let secondaryVariables = [
  {
    cssVar: "--fn-secondary",
    cssValue: "",
  },
  {
    cssVar: "--fn-dark",
    cssValue: "",
  },
];

let redVariables = [
  {
    cssVar: "--fn-red-light",
    cssValue: "",
  },
  {
    cssVar: "--fn-red-dark",
    cssValue: "",
  },
];

let greenVariables = [
  {
    cssVar: "--fn-green-light",
    cssValue: "",
  },
  {
    cssVar: "--fn-green-dark",
    cssValue: "",
  },
];

let blueVariables = [
  {
    cssVar: "--fn-blue-light",
    cssValue: "",
  },
  {
    cssVar: "--fn-blue-dark",
    cssValue: "",
  },
];

let brandVariables = [
  {
    cssVar: "--fn-brand-light",
    cssValue: "",
  },
  {
    cssVar: "--fn-brand-dark",
    cssValue: "",
  },
];

let githubVariables = [
  {
    cssVar: "--fn-github-light",
    cssValue: "",
  },
  {
    cssVar: "--fn-github-dark",
    cssValue: "",
  },
];

let gitlabVariables = [
  {
    cssVar: "--fn-gitlab-light",
    cssValue: "",
  },
  {
    cssVar: "--fn-gitlab-dark",
    cssValue: "",
  },
];

export const SetStyleOnLoad = (): any => {
  //Run when the page loads
  useEffect(() => {
    // If the user's browser is set to dark mode
    if (
      window.matchMedia &&
      window.matchMedia("(prefers-color-scheme: dark)").matches
    ) {
      // flip the css variable names and values. White will become #000
      darkModeToggler();
    }
  }, []);

  return null;
};

const reverseArray = (array: any[]) => {
  // populate the rootVariables array with the current values
  array.map((arrayItem) => {
    arrayItem.cssValue = getComputedStyle(document.body).getPropertyValue(
      arrayItem.cssVar
    );
  });

  // create a duplicate array, only containing the cssValue property
  let reversedValues = array.map((arrayItem) => arrayItem.cssValue);

  // reverse the array
  reversedValues.reverse();

  // create a new array using the original cssVar and the reversed cssValue
  let newArray = array;

  newArray.forEach((arrayItem, i) => (arrayItem.cssValue = reversedValues[i]));

  // map the value of the new array to the root css variables
  newArray.map((arrayItem) =>
    document.documentElement.style.setProperty(
      arrayItem.cssVar,
      arrayItem.cssValue
    )
  );
};

export const darkModeToggler = () => {
  reverseArray(rootVariables);
  reverseArray(secondaryVariables);
  reverseArray(redVariables);
  reverseArray(greenVariables);
  reverseArray(blueVariables);
  reverseArray(brandVariables);
  reverseArray(githubVariables);
  reverseArray(gitlabVariables);
};
