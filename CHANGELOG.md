# JULY 26, 2022

# Merge Request: [backend/standby](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/87)

# Author: Justin Russell

# Description: Everything in this merge request is for the Hive Backend.

# Whats New

`standby` event

- An event that is sent to all of the user's devices that are not currently running the app and are healthy. The purpose of this event is to allow all of user's devices to begin pulling in storage snapshots. Doing so allows failover to complete more quickly since the device should have the latest data upon startup.

`apps/api/CHANGELOG.md`

- Change log for changes to the Hive Backend.

`README.md`

- Adds descriptions to Hive Backend section

`handle_app_installation_state` signal now handles new AppInstances. This additional handles the new [installation method that Zander got set up](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/85).

# Bug Fixes:

Scheduler:

- Scheduler no longer schedules app if the app's `state` is `not-installed`.
  - In the event that the app gets uninstalled while starting, a stop event will now be sent.
- Scheduler sends `stop` event before sending `start` event. This still needs some further refinement as stopping may take longer than expected (device is still uploading snapshot, etc).
- Scheduler can now schedule apps that do not have a device yet. This is allows for freshly installed apps to be scheduled.

Misc:

- `instances` event now handles the case where no apps should be running on the device.
- Link Health check now reschedules to another device if failure occurs.
- Links are now created from `https://gateway.fractalnetworks.co`.
- `unhealthy` `instance-state` event now sets app's `current state` to `stopped`.
- When receiving a device event, responses are always `200 OK`. This is to account for the case where a device has acked a `stop` event for an app that was not supposed to be running on the device.
- Fixes paths for `pyenv` target in Makefile

# Validation Steps:

The easiest way to test the Hive Backend is to run the Hive Device. Follow the validation steps for the [Hive Device](https://gitlab.com/fractalnetworks/components/hive-device/-/merge_requests/2)

# JULY 26, 2022

# Merge Request: [image-upload](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/88)

# Author: Zander Ambrose

# Changes made

1. profile_image FileField added to User Model in hive_backend
2. django-cleanup added to installed apps and requirements.txt to clean up multiple images user uploads.
3. Front end implementation for getting user's profile_image from api or defaulting to hardcoded image source now. (This will become an avatar feature in a separate PR)
   - Personal settings modal now updates state with whatever file you choose as an indication to the user of what image they have selected on there file system BEFORE request is made to backend
   - Client api method is written and firing when user selects an image file and saves their new profile_image.

# Validation Steps

1. git clone [git@gitlab.com](mailto:git@gitlab.com):fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout image-upload
1. Run command `make dev`
1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://console.fractalnetworks.localhost:3002
1. Click Account Settings button in your personal space to trigger modal.
1. Choose a file to edit your profile picture
   - State should change in the modal as an indication to the user that they selected a new image, but not in the navbar or sidebar components as request hasn't been made yet.
   - Click save and all instances of your profile image should be updated immediately (This is done by calling an SWR mutation hook)
   - View the uploaded file in django backend by going to apps/api/media/<profile_images>/<email_address>/<file_name>
   - Choose a new image and validate that only one image remains in the users directory in apps/api/media/<profile_images>/<email_address>/<file_name>
1. Click on the navbar to navigate to store or landing page and the navbar should have your image.

# July 27, 2022

# Merge Request: [app-delete](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/89)

# Changes made

1. Wire up App Uninstall modal to backend
1. Wire up Device Delete modal to backend
1. Fix two minor bugs in UI
   - Edit Config and Delete Device modals got switched in terms of ordering. 1. Correct now
   - CardXXl is now dynamically fetching github stars from our backend and visual 1. representation of that is now correct.

# Validation Steps

1. git clone git@gitlab.com:fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout app-delete
1. Run command make dev

1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://console.fractalnetworks.localhost:3002

1. Sign in with credentials

   - username: testuser1
   - password: testuser1

1. Install an instance of an app or as many as you'd like from the appstore if you do not have any apps currently.
1. Click on the gear on the console UI app card and open up the Uninstall App modal. Correctly type in the name of the app (or don't and verify client side validation) and click uninstall app button. App should be removed from the UI.
1. Next Verify that the Delete Device functionality is working in the right sidebar devices section. Click the gear on the card and click Delete Device to trigger the modal, then click the remove button. Device should be removed from the UI.

# JULY 29, 2022

# Merge Request: [backend/stopping](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/90)

# Author: Justin Russell

# Description:

Hive Backend now waits for previous Device to stop before continuing with failover. It does this by adding a "stopping" target state.

# What's New

`target_state: stopping`

- New addition to allow for the scheduler to not reschedule an app while this state is active. When a stop event is triggered, the backend will now prevent scheduling for the app until a stop event has been received from the device that was previously running the app. This allows the previously running device to upload any remaining snapshots before stopping.

# August 1st, 2022

# Merge Request: [keycloak-registration](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/91)

# Author: Zander Ambrose

# Changes made

1. Users can register in our app through the built in keycloak page. This was configured in our Realm settings under the Login tab. User Registration was flipped on.
1. Display_name model field added to our custom User Model.

   - Authentication middleware class updated to extract the username from the token (the user provides a username when registering on keycloak page) and add it to the User model if creating a new user in the display_name model field.

1. Front end Personal Settings modal is able to PATCH the display name property from the front end if user wants to change this.

   - Client api method is written for the PATCH request to UserViewSet and implemented in the modal it needs to be called in.
   - Member component extracts correct api response data to use to render the display_name django is sending us.

# Validation Steps

1. git clone git@gitlab.com:fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout keycloak-registration
1. Run command make dev

1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://console.fractalnetworks.localhost:3002

1. Use the REGISTER button to create a registration flow now that we have this option.
1. Once logged in, you can click the Account Settings button and update your display name. Click save and django will have the new data as your display_name field. Click the Account Settings modal again and notice the updated display name.

# AUGUST 2, 2022

# Merge Request: [backend/endpoint/reschedule](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/92)

# Description:

New endpoint for manually rescheduling an app! Storage private key generation now generates two keys.

# What's New

`api/v1/app/<APP_INSTANCE_UUID>/reschedule/`

- Reschedules the provided `APP_INSTANCE_UUID` to another device if available. In the case that there isn't another device available, then the endpoint returns an `HTTP 400 Bad Request` and doesn't attempt to reschedule.

# What's Updated

`generate_storage_keys`

- Has been moved to from `hive_backend/tasks/requests.py` to `hive_backend/api/utils.py`.

- No longer an async task seeing as it doesn't make any blocking HTTP requests.

- Generates storage keys for: `app` and `link`.

- Now returns a dictionary of storage keys that is structured like so:

  ```python
  api_keys = {
   "app": "ASKDJBjkf0bkjk12348...=",
   "link": "aslkdfjskdkBf0o123...="
  }
  ```

`hive_backend/api/signals.py/handle_app_installation_state` signal

- Updated to reflect changes to `generate_storage_keys`.

`hive_backend/tasks/scheduler.py`

- `app_scheduler.py` no longer calls `save_related` when there is no device attached to an app.

`hive_backend/tasks/scheduler.py/reschedule_on_start_timeout`

- Now excludes apps that have a `target_state` of `stopping`. This fixes a bug where the scheduler doesn't wait for the original device to stop the app before rescheduling.

# August 3rd, 2022

# Merge Request: [narrow-scope](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/93)

# Author: Zander Ambrose

# Changes made

1. Groups front end components are commented out
1. Custom domain support modals and components are commented out
1. Update Device Name client api method written and implemented in modal sending PATCH requests to Django so User can customize the device names.
1. Client api method written and implemented to return Members health status. (This is determined by checking the health status for all devices that user owns. If they are all healthy then the UI shows a green status icon, if they are all not healthy, UI shows red status icon, and if some are healthy and some are not healthy, the UI shows a yellow status icon.
1. Utility custom hook created to return users uuid from accessToken when the client needs that for requests

# Validation Steps

1. git clone git@gitlab.com:fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout narrow-scope
1. Run command make dev

1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://console.fractalnetworks.localhost:3002

1. Log in with your credentials or testuser1, testuser1 (username, password).
1. Try updating your device name in the right sidebar and it will update in our DB and in the UI.
1. Verify there is no information about Groups in the UI.
1. App Cards do not have any custom domain functionality and you can only uninstall the app.

# August 3rd, 2022

# Merge Request: [multi-user-signin](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/94)

# Author: Zander Ambrose

# Changes made

1. User can sign in with multiple accounts
   - Front end signs out with NextAuth and then uses window.location.href to handle signing out with keycloak. This is the same way we handled this in the Fractal Link UI

# Validation Steps

1. git clone [git@gitlab.com](mailto:git@gitlab.com):fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout multi-user-signin
1. Run command `make dev`
1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://console.fractalnetworks.localhost:3002
1. Log in with your credentials or testuser1, testuser1 (username, password).
1. Log out and sign in or register with a new account.

# AUGUST 5, 2022

# Merge Request: [adddevice](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/95)

# Whats Changed

1. Authentication middleware is refactored.
   - The front end calls `api/v1/member/join/` on the signIn callback in the Next backend. This endpoint is an action on the UserViewSet. The authentication middleware checks to see if the request is being made to this path and will attempt to get the user by UUID or create a user if they aren't in the DB.
   - For every other request to our backend, the auth middleware will attempt to get the user in the DB or raise an `AuthenticationFailed` error. This allows us to only create users from the `api/v1/member/join/` endpoint.
   - System scoped JWTs now do not create Users in the database. Instead these tokens use a `FractalTokenUser` which is basically an Anonymous User. Doing so allows System scope tokens to hit any endpoint without needing a User to exist in the database.
1. UserViewset is refactored
   - Create method is not allowed anymore directly to our member endpoint. This is handled only in our join endpoint.
   - Our join endpoint returns 201 if user is created, otherwise it will return a 200. This allows us to monitor user creation more easily.
1. Add Device Page is implemented. Users will be directed here if they log into the console_ui without a device.
1. Auth-handler configuration object for Next Auth calls our join endpoint on the signIn callback.
1. Appstore_ui See Details button has been refactored to use next Link component rather than just an `a` tag with an href.

# Validation Steps

1. git clone [git@gitlab.com](mailto:git@gitlab.com):fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout adddevice
1. Run command `make dev`
1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://console.fractalnetworks.localhost:3002
1. Log in as a user with no device or register as a new user.
1. You should be taken to our add a device page at `/adddevice`.
   - Here we should have your user information as well as a generated device token.

# August 5th, 2022

# Merge Request: [adddevice](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/95)

# Author: Zander Ambrose

# Changes made

1. Authentication middleware is refactored.

   - The front end calls api/v1/member/join/ on the signIn callback in the Next backend. This endpoint is an action on the UserViewSet. The authentication middleware checks to see if the request is being made to this path and will attempt to get the user by UUID or create a user if they aren't in the DB.
   - For every other request to our backend, the auth middleware will attempt to get the user in the DB or raise an AuthenticationFailed error. This allows us to only create users from the api/v1/member/join/ endpoint.

1. UserViewset is refactored

   - Create method is not allowed anymore directly to our member endpoint. This is handled only in our join endpoint.
   - Our join endpoint returns 201 if user is created, otherwise it will return a 200. This allows us to monitor user creation more easily.

1. Add Device Page is implemented. Users will be directed here if they log into the console_ui without a device.
1. Auth-handler configuration object for Next Auth calls our join endpoint on the signIn callback.
1. Appstore_ui See Details button has been refactored to use next Link component rather than just an a tag with an href.

# Validation Steps

1. git clone git@gitlab.com:fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout adddevice
1. Run command make dev

1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://console.fractalnetworks.localhost:3002

1. Log in as a user with no device or register as a new user.
1. You should be taken to our add a device page at /adddevice.
   - Here we should have your user information as well as a generated device token.

# AUGUST 8, 2022

# Merge Request: [backend/deployment-fixes](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/96)

# Whats Changed

`/api/v1/app/<APP_INSTANCE_UUID>/reschedule/`

- Fixes bug where the app wasn't being rescheduled to another device.

- Now returns a 400 if the app is not currently installed or should not be running.

`*.fractalnetworks.co` is now a CSRF Trusted Origin. This is required to use the admin interface when deployed at a URL that isn't `localhost`.

# AUGUST 11, 2022

# Merge Request: [setup/logging](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/98)

# Description: Sets up Logging for Django & Celery. Also makes Device creation a Get or Create

# What's New

Logging

- Sets up logging for both Django as well as the celery worker.

- Celery worker now logs to two different files:

  - `celery.log`: Where all of Celery's default logging goes (Logs related to Celery Beat, etc)

  - `scheduler_log_file.log`: Where all of the scheduler related logs go.

# What's Changed

`/api/v1/device/` now does a get or create based off of the device's name & owner's uuid. This fixes the case where Devices with the same name were being added multiple times.

# August 11th, 2022

# Merge Request: [global-uuid](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/97)

# Author: Zander Ambrose

# Changes made

1. We have appended the users UUID from the JWT sub onto the Session object for ease of use in the front end. This is done on the Next Auth session callback.

   - We have also added this uuid type on the Session object using Module Augmentation for a better typescript DX
   - Refactor code fetching users detail information from django with uuid from session object rather than decoding the JWT.

1. Create Fractal Avatar component that can accept a size prop in order to use in different areas.

   - Implement this Fractal Avatar if we do not have a profile picture from hive-backend in all necessary places.

# Validation Steps

1. git clone git@gitlab.com:fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout global-uuid
1. Run command make dev

1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://console.fractalnetworks.localhost:3002

1. Log in or register as a new user.

   - If you are a new user, add a device in the django admin to bypass ADD DEVICE page.

1. If you have not uploaded a profile picture, then your profile picture should be a circle avatar with the initials of the name you have chosen.
1. Click the Personal Settings modal and you should be able to upload a profile picture and the display in the UI will be updated to your newly choosen picture.

# August 12th, 2022

# Merge Request: [element](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/99)

# Author: Zander Ambrose

# Changes made

1. Element app added to hive-backend App fixtures
2. Front end Element detail page is created.
   - Image assets are uploaded and implemented
3. App UUID's are abstracted into ENV variables and added to docker compose environment variables to be consumed in front end.

# Validation Steps

1. git clone [git@gitlab.com](mailto:git@gitlab.com):fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout element
1. Run command `make dev`
1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://store.fractalnetworks.localhost:3001
1. Element will be a new app in our appstore landing page.
1. Click on See Details to view the element detail view page.
1. View the element specific content.

# August 12th, 2022

# Merge Request: [add-device-flow](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/100)

# Author: Zander Ambrose

# Changes made

1. User has the ability to click done to continue UX flow after running our docker command in their terminal on Add A Device page. (This is the page we point users to if they log into the console_ui and do not have a device.)
   - Clicking the Done button starts a polling process where we fetch Device detail data from the hive-backend. If the hive-backend returns a device associated with that user, we route the user to the landing page of the console_ui. If we are returned no device data for the user, we refetch after a timeout of 2.5 seconds. We have a total of 5 fetches until the polling will finish and we will throw an error indicating something went wrong and they will need to try again.
   - Two modals are implemented on this page.
     - 1.) Loading Modal: rendered while we fetch data
     - 2.) Error Modal: if we are returned no device data for the user or if the fetch to the hive-backend throws an error.
2. App Cards in console_ui have an href link to their fractal link domain once the app is installed.
   - App.tsx component destructures link domain from data fetched from hive-backend and passes the url prop down to the AppCard.

# Validation Steps

1. git clone [git@gitlab.com](mailto:git@gitlab.com):fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout add-device-flow
1. Run command `make dev`
1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://console.fractalnetworks.localhost:3002
1. Register as a new user, or log in as a user with no device
1. Click the Done button on the Add A Device page, and a modal should appear in the UI with the text "Please wait while we get your device running" and a loading spinner in the body of the modal.
1. After the timeout, a separate modal should appear with the text "Error creating device. Please try again."
1. Once we have the ability to run the docker command from the terminal and get it updating in the hive-backend, we can try this same process to validate that clicking Done will route the user home before the timeout throws the error.

# AUGUST 15, 2022

# Merge Request: [backend/postgres](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/101)

# Description: Switches Django Database to Postgres

# What's New

Django database switched to `postgresql`.

# What's Changed

`apps/api/requirements.txt`

- Adds `psycopg2` module for postgres support.

`Makefile`

- `api-reset` - updated to remove postgres data.

# Validation Steps

Since originally the database in `apps/api/data/` was sqlite3, you'll need to remove the database file (`db.sqlite3`) before running the stack.

1. `make api-reset` - Removes everything in data directory. This command also tries to restart the api container. If the container isn't running, the target will return a non-zero exit status when it tries to restart. This is okay, since the commands that needed to run happen before the restart.
2. `make dev` - This will install the new dependency added to the `hive-backend`.
3. `make api-logs` - Make sure that you see that the API is running and that the fixtures were loaded successfully:

```text
fractal-mosaic-ui-hive-backend-1  | Installed 2 object(s) from 1 fixture(s)
fractal-mosaic-ui-hive-backend-1  | Installed 3 object(s) from 1 fixture(s)
fractal-mosaic-ui-hive-backend-1  | Installed 7 object(s) from 1 fixture(s)
fractal-mosaic-ui-hive-backend-1  | Installed 1 object(s) from 1 fixture(s)
```

# August 15th, 2022

# Merge Request: [link-url](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/102)

# Author: Zander Ambrose

# What's Changed

1. Appstore_ui directs user to console_ui after successfully installing an application
2. Console_ui handles adding the https protocol to the fractal link href that is rendered and clickable to the user.

# Validation Steps

1. git clone [git@gitlab.com](mailto:git@gitlab.com):fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout link-url
1. Run command `make dev`
1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://store.fractalnetworks.localhost:3001
1. Log in with a user who has a device
1. Install an application in the app store
   - You should be directed to the console_ui
1. The console_ui should contain an href link that will append the https protocol if the link returned from the hive-backend didn't have one.

# August 16th, 2022

# Merge Request: [responsive-style](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/103)

# Author: Zander Ambrose

# What's Changed

1. Media query written in card.module.css. CardXXl components' content now avoids bleeding out of the grid context into padding.

# Validation Steps

1. git clone [git@gitlab.com](mailto:git@gitlab.com):fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout responsive-style
1. Run command `make dev`
1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://store.fractalnetworks.localhost:3001
1. Click an app detail view page.
1. Right click and select inspect to open dev tools
1. Choose mobile view and verify the content is not bleeding out of the grid context like before.
   - Once the viewport hits a width of 400 px, the CTA button (i.e sign in to install or install) will wrap the whitespace which keeps that element contained inside the grid content and not bleeding out of the content into the padding.

# AUGUST 17, 2022

# Merge Request: [scheduler/stuck-in-error](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/104)

# Description: Fixes a few scheduler behaviors for single devices

# What's Changed

**Scheduler no longer sends a stop event when the App Instance is in an error state (no healthy devices to failover to) and the Device that is currently attached to the App has come back online.**

- This handles a case where a stop event would be sent, followed by the start event. Now only the start event is sent.

`run_app_in_standby` celery task interval changed from `30` seconds to `60` seconds.

`scheduler` celery task no longer excludes the current device when rescheduling an app that was in an `error` state.

- This fixes the case where the currently attached device is the only device that is currently healthy but the scheduler doesn't failover due to exclusion.

**Switches print statements to use logging.**

# August 25th, 2022

# Merge Request: [passphrase](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/107)

# Author: Zander Ambrose

# What's Changed

### Backend:

1. User model updated to contain "salt" and "salted_passphrase" model fields.
2. def handle_passphrase function created as an @action for UserViewSet.
   - This url is at `/api/v1/member/<user_uuid>/passphrase/`.
   - GET request returns user instance data
   - POST request partially updates the user models "salt" and "salted_passphrase" fields.
3. Device pre_delete signal is created in order to handle dissociating any app instance running on that device. (We need to give the user an option here to reschedule with other devices before user deletes the device.)

### Frontend:

1. Device Install modal now has a passphrase gaurd that requires creating or validating your System Passphrase in order to add a device. This passphrase is then added to out docker run command as an env variable (i.e --env DEVICE_SECRET=<fractal_passphrase>).
   - Salt and salted_passphrase generated on client and POST'ed to hive-backend.
   - Client side validation for input field requirements and validation of system passphrase.
1. Get and Set passphrase client api methods are implemented in useMosaicApi client library.
1. saltPassphrase and verifyPassphrase utility methods are created and abstracted into util/passphrase module that lives in our FractalLibrary.

# Validation Steps

1. git clone [git@gitlab.com](mailto:git@gitlab.com):fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout passphrase
1. Run command `make dev`
1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://console.fractalnetworks.localhost:3002
1. Attempt to add a device
1. You should be required to create your system passphrase and then confirm the passphrase.
1. You are then taken to the add a device modal which should have the env variable DEVCE_SECRET which is what you just entered.
1. Now try and add another device and you should be prompted with a single input that validates your previously submitted passphrase.
1. Meanwhile all input fields should have client side validation with relevant error messages.
1. Make sure this same UX flow is working in the appstore_ui when user clicks on Install button on any app detail page.
1. We should also be able to delete a device we want without the DELETE request erroring out. Pre_delete device signal.

# August 26th, 2022

# Merge Request: [remove-device-check](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/108)

# Author: Zander Ambrose

# What's changed

1. With Auth protected route higher order component doesn't send the user to our adddevice page if they do not have a device.

# Validation

1. Remove all devices and load the console_ui. You shouldn't be routed to adddevice page.

# AUGUST 26, 2022

# Merge Request: [scheduler/matriarch](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/109)

# A New and Simpler Scheduler: Matriarch

Introducing the Matriarch scheduler. Matriarch is a much more simplified scheduler that ensures that the currently assigned device either starts or stops an app. This redesign removes many of the asynchronous scheduled tasks that were running.

The scheduler no longer is responsible for also sending device requests. Instead, the scheduler simply takes in the `current_state` of an application (determined and managed by the device), `desired_state` (the state the user has chosen -- app is started / stopped), and optionally `context` (Fractal Link / Storage key information, etc). The scheduler then determines what commands should be run on the device and serializes them into a list of JSON messages that can be sent to the device via `command_executor`.

The `command_executor` simply takes a list of JSON serialized device commands, and sends these messages to the device. The `command_executor` has been designed in a way that allows for commands to have dependencies. One example of this use case is for rescheduling an application to another device. In order to reschedule, first a stop event must be sent to the device that was originally running it. Once the stop event has been fulfilled by the device, only then will a start message be sent to the newly chosen device.

Eventually, users will be able to choose a scheduler that they would prefer. Maybe a user only wants to run an app on a subset of their devices, or maybe they only want to run their app on a specific device. This new architecture makes it easier to control how their apps are run.

# What's Changed

`hive_backend/api/utils.py`

- Fractal Links are now generated synchronously. Before Fractal Link generation was in an async task that was awaited. _This isn't a permanent change but temporary since the device will soon generate their Fractal Link (and Storage Keys) and share it (encrypted) to the backend._

`hive_backend/api/views.py`

- `AppInstanceViewSet.create` now uses the App name as the default name if a name isn't given in the request.

- Instances event refactored work with **Matriarch**.

`apps/api/fixtures/test_data.json`

- Removed test device objects.

`apps/api/hive_backend/api/signals.py`

- `handle_app_state_change` is a new `AppInstance` `pre_save` signal that handles sending start / stop events immediately on state change. _NOTE: Right now any time save is called, this code executes which means that messages will be sent out based on the logic set up._

`.env`

- Refreshed `SYSTEM_JWT`

#### Known Issues

App Instance health may vary and be slow to update. Current an app's health is updated to `"green"` whenever the app is correctly reported in the `instances` event. Basically, if the app is in the instances array and `target_state` is running, then the app is considered healthy.

If the Device **does not** ack a message immediately, the `send_device_request` will fail after a 60 second timeout. Doing so blocks the worker thread for that amount of time.

#### Hive Device Changes

`apps/hive-device/src/tasks.py`

- Commands have been updated to look like what the **Matriarch** scheduler sends.
- The `payload` key in the received messaage is now sent to the start & stop tasks. Payload contains the same information as previously expected.

## 2022-08-26

#### Merge Request: [featured_apps](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/111)

#### Author: Zander Ambrose

#### What's Changed

#### Backend:

1. AppInstanceViewSet updated to use get method in order to not break installApp requests from front end.
2. AppCatalogViewSet returns all featured services that you currently do no have installed if we fetch the data with the featured=True query param.

#### Frontend:

1. FeaturedApps component is created and implemented in the console_ui.
2. CardFeatureApp component is created and is rendered in the FeaturedApps component.
3. `FEATURED_APPS_ENDPOINT` is added to frontend endpoints file.

#### Validation Steps

1. git clone [git@gitlab.com](mailto:git@gitlab.com):fractalnetworks/fractal-mosaic-ui.git
1. cd fractal-mosaic-ui
1. git checkout featured_apps
1. Run command `make dev`
1. Wait for npm i inside the appstore_ui container to finish running in order for all ui containers to successfully run.
1. In your browser open up http://console.fractalnetworks.localhost:3002
1. You should see a featured services component.
1. Clicking deploy on any of the app cards should initiate the installation flow.
1. Once successfully installed, the UI should update state to reflect you newly installed app, as well as removing that app from the featured services component.

## 2022-08-26

#### Merge Request: [redis/caching](https://gitlab.com/fractalnetworks/fractal-mosaic-ui/-/merge_requests/112)

#### What's New

Adds Redis caching support.

##### What's Changed

`requirements.txt`
`redis==4.3.4` - Redis dependency for Django
`django-redis==5.2.0` - This library has more commands available that Redis supports. This is specifically used in order to use wildcards when querying Redis (example: `cache.keys('seen-*')`)

`apps/api/hive_backend/api/models.py`

- `Device.handle_ping_event` now stores itself in Redis like so: `seen-<UUID>, <UUID>`. The key is stored for 60 seconds, and each time a ping event is received, the key is updated.

`apps/api/hive_backend/tasks/executor.py`

- `mark_device_unhealthy` now queries Redis for all devices that are currently in Redis like so:

  ```python
  keys = cache.keys('seen-*') # gets all keys that start with "seen-"
  healthy_devices = cache.get_many(keys)
  ```

  The task then queries all devices excluding those that are returned from the Redis query above, and marks them all as unhealthy.

You may need to rebuild your `hive-backend` docker image (`make dev`) in order to install the latest requirements.

<hr>

## 2022-08-26

<hr>

### [Merge Request 113](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/113)

(Bugfix / Test / Enhancement)

#### What's Fixed

- IsOwnerFilterBackend no longer conditionally filters objects by owner, admin does not get to see objects that do not belong to them
  - If admin needs special access we should add dedicated / audited endpoint for that specific use case
- Added test for `IsOwnerFilterBackend` bugfix
- Added `make apitest` target for running backend tests to root `Makefile`

#### What's Better

- Install docker and docker-compose in device container from official docker image
- Add CHANGELOG.md template

## 2022-08-29

<hr>

### [Merge Request 114](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/114)

(Bugfix)

#### What's Better

- `api/v1/device/event/` will no longer return a `500 Interal Server Error` when given a Device or Account that doesn't exist. Instead, if either Device or Account does not exist, a `200 OK` is returned.

<hr>

### [Merge Request #115](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/115)

(Feature)

#### What's New

- Reschedule app action modal is created on the app instance card.
- A user can manually choose to reschedule each app with any device they have in their device list.
- Reschedule app client api method is implemented in useMosaicApi custom hook.
- AppInstanceViewSet Partial_Update method updated to handle Fractal Cloud Device uuid get or create.
<hr>

### 08/31/2022

<hr>

### [Merge Request #116](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/116)

(Feature / Enahncement)

#### What's New

- AppInstanceViewSet `app_name_check`@action returns only state='installed' applications.
- AppCatalogViewSet get_queryset method doesn't filter apps based on "featured" query param. This prevents UI from removing apps the user has installed in our featured services component.
- Device_name is added to the app instance cards.
- On successful appReschedule request, mutation is called for app instance card to reflect updated device_name.
- Featured Services app cards fetch whether or not user has this app instance installed and reflects the state in the UI. If installed text is deployed and disabled, otherwise the button is enabled and the text is deploy.
- System passphrase dialog is adjusted according to Jira ticket.

#### What's Better

- App Instance card power switch has styling to indicate it is clickable. This button includes hover styling as well as click styling.

<hr>

## 2022-08-31

<hr>

### [Merge Request #117](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/117)

(Enhancement / Debt / Test)

#### What's Better

- `apps/api/hive_backend/api/signals.py`:

  - `handle_device_cascade` `pre_delete` signal: Unsets all AppInstance's device property where the device is the one being deleted

- `apps/api/hive_backend/api/serializers.py`
  - `AppInstanceSerializer` excludes `storage_apikeys` so that private keys are never shared with the UI or anywhere this serializer is used.

#### What's Debt

- `/api/v1/member`: Eventually it would be nice to have a test that ensures that new users can only be created at the `/api/v1/member/join` endpoint.

#### What's Tested

- API Tests for our main endpoints:

  - `apps/api/tests/hive_backend/api/test_app_endpoint.py`

    - Tests for `/api/v1/app`:
      - List / detail views + all actions available.

  - `apps/api/tests/hive_backend/api/test_device_endpoint.py`
    - Tests for `/api/v1/device`:
      - List / detail views + all actions available.
      - Verifying the pre_delete signal for Device is called.
  - `apps/api/tests/hive_backend/api/test_member_endpoint.py`
    - Tests for `/api/v1/member`:
      - List / detail views. See **What's Debt** section.

<hr>

### [Merge Request #118](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#118)

( Enhancement / Bugfix )

#### What's Better

- Device waits to ack the stop message until the app that was requested to stop has successfully been stopped.

#### What's Tested

- Manually tested the stop message is not acked by device until stop has completed.

<hr>

## 2022-09-01

<hr>

### [Merge Request #119](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#119)

( Enhancement )

#### What's Better

**Hive Device:**

- Simplified logic for ACKing stop commands.

- Device now sends `instances` message to the backend upon successful start. Doing so allows the Hive Backend to update the **AppInstance's** health faster.

### [Merge Request #120](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/120)

(Enhancement / Bugfix / Debt)

#### What's Better

- Moved device event handling of `instances` event into its own file.
- Scheduler can now successfully execute synchronous migration between devices.
  - Introduced `rescheduled_to_device` property on `AppInstance` model to fix the race condition described below.

### What's Fixed

- Fixed race condition that caused a synchronous reschedule between devices to be interrupted by the handler of `instances` event emitting a `SchedulerAction.start`
  - We now use the `reschedule_to_device` which references a pending reschedule action of an `AppInstance` to a new `Device`
  - The device `partial_update` endpoint takes a PATCH request to set a new device and instead sets that device as the `reschedule_to_device` property of AppInstance
  - Once we have confirmed receipt of the `SchedulerAction.stop` event from the previous device we update `AppInstance.device` to point to the device referenced by `reschedule_to_device`, we then set `reschedule_to_device` to None
  - This change also allows us to move scheduler initiation to a `post_save` signal, this is still #TODO

#### What's Debt

- Need tests, scared to change anything now that it's working :(

<hr>

## 2022-09-02

<hr>

### [Merge Request #122](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/122)

(Enhancement/Bugfix)

#### What's Better

- Buttons triggering http requests to hive-backend are disabled until request returns a response to avoid multiple fetches:
  - App Reschedule
  - Upload profile image
  - Update user display name
  - App Power Switch
  - Creating Passphrase
  - Deploying app instance

#### What's Fixed

- Form submission on enter click refreshing the page due to default submit handler:
  - Personal Settings Modal
  - Entering passphrase modal
  - Creating passphrase modal


### [Merge Request #123](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#123)

( Enhancement / Debt / Bugfix )

#### What's Better

- Hive Device no longer blocks when handling received messages from the websocket. Instead, the `handle_message` function is fired off as an async task.

- Hive Device now caches if a task is running for an app instance. This means that the device now ignores received events for an app that is already being started / stopped. As soon as the task the start / stop has successfully returned, the app instance's key is cleared.

#### What's Fixed

- Hive Device no longer is blocked by apps that take a long time to start. The Device can now start / stop multiple apps simultaneously.

#### What's Debt

- Need to figure out the best way to test caching / running commands / etc.


### [Merge Request #124](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#124)

( Feature / Bugfix )

#### What's New

- `Flower` (`docker-compose.flower.yml`)
  - Adds **Celery Flower** as a new optional container to run along side the stack. Flower allows for a GUI for monitoring Celery Tasks.
  - `docker-compose.flower.yml` has been added to the default containers that are run. Simply remove this file from the `COMPOSE_FILES` variable in the `Makefile` in order to omit this from the stack.

#### What's Fixed

`/apps/api/hive_backend/api/device_event_handlers/instances_event.py`

- Hive Backend will no longer attempt to send `stop` requests to a device that is not online (not in the device alive cache). Instead, if the device is not in the cache, `handle_instance_event` returns `False`.

#### What's Debt

- Need tests to ensure that the Hive Backend never tries to send messages to devices that are offline.
 <hr>

### 2022-09-09

<hr>

### [Merge Request #125](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/125)
(Enhancement / Bugfix)


#### What's Better
- Instead of bulk updating devices when marking them unhealthy we call save on each device so we can (in the future) fire websocket events that will update UI state in the browser.

#### What's Fixed
- Use `.exclude(health="red")` to fetch unhealthy devices in async task that marks devices unhealthy instead of `.filter(health="green")` which would have missed devices with a health state of `yellow` that had gone offline.

### [Merge Request 127](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/127)

(Feature)

#### What's New
FrontEnd:
- Front end will intercept link url click and initiate a username and password POST request to hive-backend to generate element server configuration.
- CardXl checks if app instance contains config data to determine if modal should be rendered.
- Modal only opens if the app instance has the app uuid of Element.
- Element Config modal built with client side validation and POST request on submit
- client api method written to talk with hive-backend
- `api/v1/appinstance_config` endpoint created in endpoints file.
Backend:
- AppInstanceConfig and AppInstance modeling updates
- AppInstanceConfigSerializer created
- AppInstanceConfig viewset created and business logic implemented to update app_instance config data as well as send device request
<hr>

### 2022-09-09

<hr>

### [Merge Request #126](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/126)
( Enhancement / Bugfix / Debt )

#### What's Better

- Removed a lot of unneccessary log messages. It should be easier to follow apps starting / stopping.

- New functions in `src/utils.py`:

  - `_read_file(file, return_lines=False) -> list|str`
    - Async function that reads a given file and returns the content either as a stream of characters, or a list of lines.
  - `get_app_path_from_env(env_file) -> str`
    - Reads given env_file and returns an app path for the given env_file or `False` if environment file doesn't exist.
  - `determine_app_running(client, app_uuid) -> bool`
    - Looks up the app's docker-compose file using information from the app's environment file. Then parses said docker-compose to compare if all containers in the compose file are currently running. Returning `True` if all the containers are running, `False` otherwise.

  All of the described functions above help `get_instances` correctly report an app as running.

#### What's Fixed

- The Instances Event
  - Previously, the `instances` message that was sent into the websocket didn't check if all containers for an app were running. This means that there were cases where the device reports an app instance as running (it's in the instances array), but in actuality it wasn't. This is no longer the case.

  - Instead, `get_instances` now determines if an app is running by parsing the app's docker-compose file for all services that should be running. Then compares currently running containers for an app. If they differ or any of the app's container health checks are failing, then the instance is not reported. This allows for the `instances` message to be more accurate to whether the app is running or not.

- `src/compose.py/up`
  - Now runs `docker-compose ... up --wait` which waits for all of the containers in the compose file to be both running and their health checks to report "healthy".

#### What's Tested

- Manually tested that the new instances event is reporting correctly by:
  - Stopping any of the containers of an app and correctly seeing the start event for that app.
  - Removing the environment file for an app triggers another start. This ensures that the new instances implementation does not crash when the environment file is deleted. When the environment file is deleted, the app instance is not reported in the instances array, which triggers a start message from the Backend. This start message regenerates the app's environment file.
  - Rescheduled Element to another device successfully multiple times.


<hr>

### 2022-09-13

<hr>

### [Merge Request #129](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/129)
(Feature)

#### What's New
- Ability to create Element admin user from console UI.


### [Merge Request #130](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/130)
(Feature)

#### What's New

- Ability to run Django in production mode based on `DJANGO_ENV` environment variable.
  - Admin password has been updated for **both production and test**! Ask Justin for it!

#### What's Debt

- When deploying to production, need a place to serve static files.

<hr>

### 2022-09-16

<hr>

### [Merge Request #131](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/131)
(Feature / Enhancement)

#### What's New
- Ghost detail page is added.
- Ghost app is added to app_data and test_data fixtures
- Ghost image assets are in
- Ghost app uuid is added to dev env file and docker compose file for each UI container.

#### What's Better
- Element app detail page has better styling for icon images.
- CardCategories is created for category cards rather than using CardSm for every different use case.
- PictureAppDetailIcon is created and styled for app detail images with dynamic size for any image using this picture component.

<hr>

### 2022-09-15

<hr>

### [Merge Request #132](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/132)
(Feature / Enhancement)

#### What's New
- Element app instance card adds the add user action functionality so we can POST to hive-backend to initiate add user flow.
    - ModalElementAddUser is added in this component
- ModalElementAddUser is created with client side validation and calls clientApi method on submit
- Client Api method addElementUser is mocked out in useMosaicApi custom hook
- Custom endpoints `ADD_ELEMENT_USER_ENDPOINT` is created in endpoints.ts

#### What's Better
- Personal Settings modal is refactored out of console_ui app and into the library to be called in the MonoNav library component.
- Personal Settings button is changed to include the text Contact Fractal Support
- Modal Personal Settings is removed from console_ui index page
- Profile dropdown adds Use Custom Gateway and Account Settings items if we are viewing the MonoNav from the console_ui.

### 2022-09-14

<hr>

### [Merge Request #133](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/133)
(Feature / Enhancement)

#### What's Debt

- Tests missing for the `send_device_request` function.
- Tests missing for the `instances` event.
  - While the scheduler itself has some tests written for it, there should be tests that verify that an instance message is handled correctly.
- Need to mock Celery `apply_async` so that tests dont actually send tasks to the Celery worker (`hive_backend_worker`).

#### What's Tested

- Tests written for `matriarch` scheduler.
  - Happy paths have been tested. Some edge cases are still to be written.
- Tests written specifically for the `create_element_admin` function.
- Tests written for the `command_executor` task.
- Test written for creating an app config.

<hr>

### 2022-09-19

<hr>


### [Merge Request #134](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/134)
### [Merge Request #135](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/135)
(Bugfix / Test)

#### What's Fixed

- `/apps/api/hive_backend/tasks/executory.py/mark_devices_unhealthy`
  - Correctly updating all App Instance's health to `red` when the instance's device is marked unhealthy.

#### What's Tested

- Test written for `mark_devices_unhealthy`.
  - Tests that `mark_devices_unhealthy` updates `Device` and related `AppInstances` health to `red` when the device does not exist in the cache.
  - Tests that `mark_devices_unhealthy` does not update `Device` health if the device is in the cache.

<hr>

### [Merge Request 136](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/136)
Feature

#### What's New
- Added chat based support integration powered by Matrix Chatterbot!


<hr>


### 2022-09-20

<hr>

### [Merge Request #137](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/137)
( Test )

#### What's Tested

More tests written for Hive Device. Tests have been broken up into two files:

- `test_tasks.py`
  - `start_app` tested for both success & app not found.
  - `stop_app` tested for both success & app not found.
  - `run_app_instance_command` tested that `spawn_background_thread` is called twice.

- `test_utils.py`
  - `regsiter_with_backend` tested to ensure a Device Token is returned.
  - `get_device_name` tested to see if name is returned based on `DEVICE_NAME` environment variable.
  - `ensure_storaged_plugin_ready` tested to verify that the correct storage plugin is installed based on the architecture of the device.
  - `generate_env` tested to ensure that write is called. Needs another test to ensure data that is written pertains to an app.
  - `get_app_path_from_env` tested to ensure that an app path is returned. Need to also test the case where the app isn't found.

<hr>

### 2022-09-19

<hr>

### [Merge Request #138](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/138)
(Feature / Enhancement / Debt)

#### What's New

- Device now can run Ghost!

#### What's Better

- Device's link container has been moved into it's own file: `apps/link.yaml`. This change allows for easier changes to the link container as a whole across all apps. The only configuration for a link that's applciation specific is the cert volume mount + `EXPOSE` environment variable.
  - All `docker-compose` shell commands now include `-f /apps/link.yaml`.

#### What's Debt

- Need to figure out how to get a working healthcheck for Ghost. A temporary healthcheck was added in the meantime.
- Database password for Ghost is hardcoded.

<hr>
- Currently the Fernet key that's generated with the `DEVICE_SECRET` environment variable is not cached. This means that extra CPU cycles will be used anytime the device encrypts / decrypts anything with the passphrase.
- No tests written to ensure that the Device generates storage keys and sends them back in an `instance-state` message.

<hr>

### 2022-09-27

<hr>

### [Merge Request #141](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/141)
(Feature / Enhancement / Test)

<hr>

#### What's New

- Device now generates storage private keys and encrypt them with the passphrase of the device. The Device supports keys that were previously generated (unencrypted) so hopefully this means that this change should be backwards compatible!

- `hive-device/src/crypto.py`:

  - `CryptoBaseClass`: A base class that exposes all of the methods that should be subclassed.
  - `PassphraseEncryptionXSalsa20`: A subclass of `CryptoBaseClass` that provides implementations for encrypting / decrypting data with a passphrase.

- Hive Backend no longer generates private keys! The backend simply stores the passphrase-encrypted storage keys that device provides on successful start.

#### What's Better

Makefile:

- `make device-reset` now resets both devices.

Hive Backend:

- When an App Instance is uninstalled, the Hive Backend clears the app's `storage_apikeys`.
- `AppInstance.encrypted_storage_apikeys` stores device encrypted storage keys.
- Storage keys are no longer generated on installation.
- `instance-state` events are now recognized. This event is currently being used to handle updating an **App Instance's** `storage_apikeys`.

### What's Tested

Hive Device:

  `hive-device/tests/test_crypto.py`:
    - Storage key encryption test to ensure that keys are encrypted.
    - Storage key decryption test to ensure that keys that are encrypted by `PassphraseEncryptionXSalsa20` can be decrypted by the `DEVICE_SECRET` passphrase.

  `hive-device/tests/test_utils.py`:
    - Storage key generation test to ensure that Ed25519 Private Keys are correctly generated.

### 2022-09-29

<hr>

### [Merge Request #142](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/142)
(Feature, Bugfix)

<hr>

#### What's New

- Device now supports running NextCloud. Currently using the builtin sqlite3 database.
  - NOTE: Failover seems to take a while due to size of snapshots?

#### What's Fixed

- Device no longer crashes when stopping an app if `encrypted_storage_apikeys` is not passed.
- Hive Backend doesn't clear storage keys on successful start / stop. It only updates `encrypted_storage_apikeys`. This change allows older versions of the device to continue running / stopping apps that were installed previously. NOTE: Once this version of the Hive Backend has been deployed, storage keys for newly installed apps will **NOT** generate `storage_apikeys`.

<hr>

### 10-3-2022

<hr>

### [Merge Request #143](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/143)
(Feature)

#### What's New
- Billing API added as an app to hive-backend
    - Hive-backend Dockerfile pip installs the billing api and is added to installed apps list for hive-backend project.
- All Billing API env variables are added to hive-backend service
- Frontend adds Tab called "Support Us" to initiate donation payment intent.  Frontend POST's to billing api (`api/v1/billing/payment_intent/`) which talks to stripe to handle payment.
- Frontend handles client side validation for donation as well as success/failure messaging during payment flow.

<hr>

### 2022-10-05

<hr>

### [Merge Request #144](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/144)
(Enhancement)

This merge request prepares the repo for open source!
#### What's Better

Environment file removed.

Hive Backend:

- Cleaned up a lot of unused functions and files.
- API Tokens in the admin panel now also show the User they are associated with.
- Registered the **billing app** with Django admin.

Hive Device:

- Cleaned up a lot of unused files.

<hr>

### [Merge Request #145](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/145)
(Feature)

#### What's New
- Donation model created in billing app with owner (ForeignKey to hive-backend.api.User model) and amount fields.
- PaymentIntent APIView sets a key value pair in the redis cache with payment_intent_id, user's uuid (sent from the client in POST request body `uuid: session?.uuid`) and amount.
- Webhook APIView endpoint created to listen for Stripe webhooks.
    - If we get a webhook event with type payment_intent.succeeded, we call helper function `handle_payment_success` and pass in the payment_intent_id.
    - This helper function gets the cache value and creates a new Donation object with the users uuid and amount and then deletes the cache key.  Cache keys will be deleted by default after 1 hour.
- Stripe webhook secret added to .env file and docker compose dev env variables.
- Two Make targets added to verify correct tables and rows are being inserted, updated, or deleted.
    - Make db-shell: exec into hive-db to view "billing_donation" table.
    - Make cache-shell: exec into redis cache to verify donation key is being stored or deleted depending on webhook response.

<hr>

### 2022-10-05

<hr>

### [Merge Request #146](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/146)
(Enhancement)

#### What's Better
- Auto Updates checkbox is implemented and updates state in docker command as well as UI.
    - Gives UX more transparency into software updating.

### 2022-10-07

<hr>

### [Merge Request #147](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/147)
(Feature / Bugfix / Test)

#### What's New

Hive Device now supports automatic upgrades! The Device now sends its current version to the Hive Backend on startup and once every hour. The Hive Backend now receives the version and stores the Device's reported version on the Device model. If the version reported by the Device doesn't match the `LatestDeviceVersion` on the backend (set up manually for now), then an upgrade event is sent to the Device. On reception of the upgrade message, the Device will launch the Watchtower container (named the `Hive-Device-Updater`), which then pulls the latest Hive Device image (`fractalnetworks/hive-device:alpha`), stops the currently running device, then restarts it with the same configuration that was used when the device was started.

Auto Updates are set as an environment variable on the Device. If `AUTO_UPDATES` is set to False, then the Device will not update when receiving an update message. `UPDATE_INTERVAL` (defaults to `3600` seconds -- 1 hour) is a configurable interval at which the update message is sent to the **Hive Backend**.

Hive Device

- `Dockerfile`
  - Labels:
    - `co.fractalnetworks.mosaic=device`: Allows for Watchtower to easily filter currently running devices on the Docker host.
    - `com.centurylinklabs.watchtower.stop-signal="SIGKILL"`: Configures Watchtower to use `SIGKILL` when stopping the device.
- `src/__init__.py`
  - `__version__` value added (defaults to `1.0.0`)
- `src/tasks.py`
  - `upgrade_device`: Gets all currently running containers with the `co.fractalnetworks.mosaic=device` tag, then launches the **Hive Device Updater**.
  - `send_version` async task: Sends a websocket message every `UPDATE_INTERVAL` seconds (default: once an hour)with the device's current version (pulled from the Device's `__init__.py`)
  - `handle_message`: Now supports `UpgradeDevice` message sent from the **Hive Backend**. Will update if `AUTO_UPDATES` environment variable set to `True`.
- `src/utils.py`
  - `_run_command` now decodes `stderr` and `stdout` from bytes to `UTF-8` string.
- `src/compose.py`
  - `run_updater`: Runs a Watchtower container (**named Hive Device Updater**) that handles updating the currently running Device.

Hive Backend

- Fixtures have been updated to add a default `LatestDeviceVersion` of `1.0.0`.
- `hive_backend/api.models.py`
  - `LatestDeviceVersion`
    - `version`: Latest device version. _NOTE: Eventually version will be pulled from Docker._
  - `Device.version`: Version reported by the Device.
- `hive_backend/api/device_event_handlers/version_event`: Handles version events from the Device. Saves the current version reported by the Device and will send an update message if the Device has `auto_updates` enabled and its version **is not equal** to the `LatestDeviceVersion`.
- `hive_backend/api/management`:
  - Overrides default `manage.py loaddata` behavior to skip `LatestDeviceVersion` if it already exists.
- Fixed authentication and permissions on views.
- Adds `/api/v1/device/version/` endpoint to update device versions. **Requires `system` scope JWT.**

#### What's Fixed

Hive Backend

- Tests are fixed. Errors were occuring due to `fixtures/intial_data.yaml` containing `contenttypes` and `auth.Permission`. These are autocreated when Django creates / syncs the test database.
- Deleting a Device which has apps attached to it no longer breaks the `handle_app_state_change` signal:
  - `handle_app_state_change`:
    - Logic updated to handle when stopping an app whose Device has been deleted.
  - `handle_device_cascade`:
    - Instead of calling update (which does not trigger the pre_save signal), the signal now iterates through each of the Device's apps and sets each app's `target_state` to `stopped` and `device` to `None`. Doing so now allows the `handle_app_state_change` signal to stop all apps whose Device has been deleted.

#### What's Tested

- Tests that ensure update messages are sent to the Device correctly when required.
- Manual testing to ensure devices are properly updating.

<hr>

### 2022-10-21


### [Merge Request #148](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/148)
(Feature / Enhancement)

#### What's New
- docker-compose.observability file created with monitoring stack and added to Makefile and compose.sh helper.
- Django prometheus app added to django.  Middleware and urls configured in django.  Add django prometheus to requirements.txt.
- Observability README.md added to info on monitoring stack.
- Prometheus scraping targets configured
- Grafana dashboards and data sources provisioned
- Alertmanager wired up with alerting rules from prometheus
- Matrix bot receiving alerts from alert manager and sending to Element server channel 

#### What's Better
- CI rules added to only run if files changed in directories that matter to the images being built.
    - CI jobs added for observability services (Prometheus, Grafana, Matrix-Bot, and Alertmanager).
- RabbitMQ image changed to include management plugin

<hr>

### 2022-11-30

<hr>

### [Merge Request #153](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/153)
( Enhancement / Bugfix )

#### What's Better
- Add a user modal for element service in console UI has a checkbox to indicate whether or not you are registering an admin user.
- The admin boolean is POST'd to the backend
- Backend handles parsing the admin bool from body and triggers the create_admin_user or create_user command accordingly.

#### What's Fixed
- If a user attempts to create an admin user for element immediately after a device is added, we have a race condition whether or not the command will actually trigger the backend to configure this user.  The UI then does not give the user the ability to register another user.  Stop gap solution for now is to add the checkbox in the add user modal so there is another way for us to trigger the user creation.


### [Merge Request #154](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/154)
(Enhancement)

#### What's Better

Hive Device will now launch an `alpine:latest` container that ensures `/var/lib/fractal` is created on the Docker Host.

```python
client.containers.run(
    image="alpine:latest",
    remove=True,
    volumes=["/var/lib:/var/lib"], # ensure /var/lib mounted from host
    entrypoint="mkdir -p /var/lib/fractal") # create /var/lib/fractal then exit
```

As a result of this change, Devices using Docker in Docker no longer have to have an anonymous volume mount for `/var/lib/fractal`.

<hr>

### [Merge Request #152](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/152)
( Feature )

#### What's New

Email sending for the **Hive API** has been setup. Currently the only emails that are sent are when the user's `Device` goes online / offline. However, the structure has been setup in a way that allows for easily adding more notifications. There is a base notifier (called `BaseNotifier`) that implements the main logic for each of the possible notifications (currently only device health changes). Each notifier is intended to subclass `BaseNotifier` to implement the `_notify` method that handles firing the notification.

One notifier that has been implemented so far (in its most basic form) is the `EmailNotifier`. The `EmailNotifier` simply takes the provided message contents, and sends an email based on what's provided. The email sending itself is an async celery task (called `send_email`) that's defined in `apps/api/hive_backend/tasks/exectuor`. This task simply wraps the Django `send_mail` function.

> NOTE: Email sending is based on environment variables that are captured in `apps/api/hive_backend/settings.py`. Make sure you have gotten the latest environment file (`Hive API Env`) from our secret store. *Emails are **ONLY** sent when `DJANGO_ENV` is set to either `PROD` or `TEST`.*

New Module: `apps/api/hive_backend/api/notifiers`

- Contains code related to sending notifications to users.

- `apps/api/hive_backend/api/notifiers/__init__.py`:
  - `class BaseNotifier()`
    - Base class that exposes the various notifications that can be sent with the Hive Backend. This class is intended to be subclassed to add the necessary delivery method by implementing the `_notify` method.
    - `device_health_change`
      - Constructs the notification, then notifies a user when their device's health has went offline / online.
- `EmailNotifier` (`apps/api/hive_backend/api/notifiers/email.py`):
  - Notifier that enables sending emails to a user based on the email configuration provided in `apps/api/hive_backend/settings.py`. Uses the `send_email` task to send an email asynchronously.
  - **NOTE: Will only send an email if `DJANGO_ENV` environment variable set to `TEST` or `PROD`. If in `DEV`, the email is printed to the logs.**
  - Uses the builtin email sending provided by Django which is configured using `EMAIL_` variables provided in `apps/api/hive_backend/settings.py`. These credentials are provided via environment variables. These values are stored in our secret store.
    - `EMAIL_HOST`: SMTP server hostname to use.
    - `EMAIL_HOST_USER`: Username to use when authenticating with the SMTP server.
    - `EMAIL_HOST_PASSWORD`: Password to use when authenticating with the SMTP server.

Adds Templates for Emails (`apps/api/templates/email_templates`):

- `app`
  - Contains templates for App Instance related messages. Currently there are templates for:
    - App Instance installation state changes
    - App Instance target_state changes (user started/stopped app)

- `device`
  - Contains templates for Device related messages. Currently there are templates for:
    - Device health changes
    - New Device additions

- `user`
  - Contains templates for User related messages. Currently there is a template for:
    - New User welcome message.
    - Launch device reminder.

- `base.html`
  - Contains the base formatting and styling for all templates. Each of the above mentioned templates inherits from this in order to maintain styling.

New Signals in `apps/api/hive_backend/api/notifiers/signals.py`:

- `notify_device_health_changes`:
  - New `pre_save` signal for `Device` that checks if a Device's health has changed from `red` to `green` or vice versa. Calls `get_notifier`, which right now only returns an instance of an `EmailNotifier` configured for the device's user. If the device's health has changed based on the criteria mentioned above, then `device_health_change` method on the notifier is called.

- `send_welcome_message`:
  - New `post_save` signal on `User` that checks if a new User has been created. If so, a welcome message is sent to the email associated with the `User`.

Added New Task File `apps/api/hive_backend/tasks/notifications.py`:

- `send_email`:
  - Sends an email with the provided `recipient`, `sender`, `subject`, `email_body`. Uses all email variables configured from `apps/api/hive_backend/settings.py`

- `send_launch_device_reminder`:
  - Periodic task that sends a notification to all Users that currently do not have a Device attached to their account, reminding them to launch a Device.
    - **NOTE: Task is set to run once a day.**

New Utility in `apps/api/hive_backend/api/utils.py`:

- `get_notifier(user)`
  - Intended to get the notifier for the provided `User`. This currently is hardcoded to always return an instance of an `EmailNotifier` for the provided user.

#### How to Test

1. Copy the latest environment file from our secret store (`Hive API Env`) and paste into `.env`.
1. Run `make up`
1. Login with an account that has a valid email address. I simply used the same account that is used in the production environment.
1. Go to the UI and click add device. Copy the API_TOKEN variable value and replace the API_TOKEN value in `apps/hive-device/docker-compose.device.yml` with the copied token.
1. Run `make up` again.
1. Check the email that the account is tied to. You should see two emails come in (the device will initially be `red`, then once started will change to `green`.).

#### What's Better

- Consolidated the Hive Device Service & Websocket into the main docker-compose stack.
- Adds volume mount at `/var/lib/fractal` for Docker in Docker containers in Device compose files in order to fix `No Such File or Directory` error.

### [Merge Request #155](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/155)
( Feature )

#### What's New

Adds hardware notifications support to the Hive Backend. The Hive Backend can now ask the Device to check if its disk space is running low. If so, the Device will send an event to the backend. The backend can then use that event to notify the User through email).

**Hive Backend**

New Scheduled Task:

- `disk_space_check` - Task that runs every Monday that sends a `disk_space_check` request to all healthy Devices.

New Email Templates:

- `low_disk_space` - Low disk space template to be used when emailing the user.

New Event Type:

- `DeviceCommand` - Message that is sent to the Device with the following structure:

```python
{
  "account": "a8c3dbac-fc4e-4976-884e-7dfad5835fcf",
  "device": "a8c3dbac-fc4e-4976-884e-7dfad5835fcf",
  "command": "DeviceCommand",
  "payload": {
      "action": "check_disk_space" # Some action the device should take.
  }
}
```

**Device**

Command Module (`apps/hive-device/src/commands`):

- Contains `DeviceCommands` that are supported by the device.
  - `check_disk_space` - Command that gets disk usage percentage. If this percentage is over 90%, then a `low_disk_space` message is sent to the Hive Backend.

Device now accepts `DeviceCommand` messages from the Hive Backend. Upon receiving a `DeviceCommand` message, the Device will attempt to run the related command. We **never** run code arbitrarily on a Device. The backend will send a command type in its payload (e.g. `"check_disk_space"`) and if this command is in the `DeviceCommands` enum, then it will attempt to run the respective enum type's hardcoded command. Otherwise the Device does nothing with the request.

#### What's Changed

`apps/api/hive_backend/tasks/executor.py/send_device_request`

- Will not send a device request to the device if the Device or User has been deleted.

### [Merge Request #156](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/156)
( Feature )

#### What's New

**Device**

New Periodic Task:

- `publish_volume_statuses` - Task that runs every 30 seconds that sends volume name and its latest snapshot time into the websocket. The structure looks as follows:

```json
{
  "type": "volume-info",
  "volumes": [
    {
      "name": "675f5400-bc34-41a6-bf3e-82df467e7699_APP",
      "last_updated": "2022-12-14 20:42:17"
    },
    ...
  ]
}
```

Fractal CLI has been added into the Hive Device. Device now logs into the CLI using the passed in API Token on startup.

#### What's Better

CI file fixed to only push latest Device Docker image to Dockerhub when the branch is main. We were accidentally pushing images up anytime the Device had changed (oops :-)).

### [Merge Request #157](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/157)
( Feature )

#### What's New

**Device**

Device now sends app volume public keys when starting / stopping apps. This addition is backwards compatible with all volumes. When decrypting / generating the private key blob from the backend, the pubkey for the app volume is returned as well. This pubkey is then simply added to the `instance-state` message that is sent after successfully starting / stopping an app:

```python
{
  "instance": app_instance_uuid, # uuid of app
  "state": "running" | "stopped", # current state of application
  "type": "instance-state",
  "storage_apikeys": {}, # legacy storage keys
  "encrypted_storage_apikeys": encrypted_keys, # encrypted blob of volume private keys
  "storage_pubkey": public_key, # public key for the app volume
}
```

**Hive Backend**

Hive Backend now stores the public key of the app (mentioned above) on the `App Instance` model:

```python
class AppInstance(OwnedBaseModel):
  ...
  storage_pubkey = models.CharField(max_length=255, blank=True, null=True)
  ...
```

The Backend saves the pubkey whenever the device sends an `instance-state` event and a pubkey was not already stored on the `AppInstance`.

The purpose of storing this `public key` is so that the **Hive Backend** can now query the **Storage API** for information on the App Instance's volume. With this information the **Hive Backend** can now provide volume replication statuses for apps. This gives the user more insight into the health of their application, and opens us up to being able to troubleshoot problems with apps.

Anytime a request is made to a detail view for an `AppInstance`, the **Hive Backend** synchronously makes a request to the **Storage API** to get the volume info for the `AppInstance.storage_pubkey`. The `updated` (last replicated snapshot timestamp) key is extracted then compared with the current time. There are three possible states the Backend can indicate:

- `replicated` - The volume has had a snapshot pushed to it **within the last 5 minutes**. This state is considered healthy (`"green"`)
- `recent-unreplicated` - The volume has had a snapshot pushed **after 5 minutes, but within the last hour**. This state is considered relatively healthy (`"yellow"`)
- `old-unreplicated` - The volume has not had a snapshot pushed **within the last hour**. This state is considered unhealthy (`"red"`)

This volume information is then added onto the serialized `AppInstance` data:

**App Instance Detail JSON when Storage API Request Succeeds**

```python
{
  "volume_info": {
    "state":"replicated" | "recent-unreplciated" | "old-unreplicated", # replication state mentioned above
    "message":"2022-12-16 16:52:26", # snapshot replication timestamp
    "health":"green" | "yellow" | "red" # health mentioned above
  },
  "target_state": "running",
  ...
}
```

If the **Hive Backend's** request to the **Storage API** encounters some sort of error, then the volume info key will display an error state instead:

**App Instance Detail JSON when Storage API Request Error occurs**

```python
{
  "volume_info": {
    "state": "error", # error state
    "message": "Error contacting https://storage.fractalnetworks.co", # error message
    "health": "red" # considered unhealthy
  },
  "target_state": "running",
  ...
}
```

There is also the case where a pubkey doesn't exist on an App Instance yet (the app was just installed). In this case, a request to the **Storage API** will **not** be made, instead when requesting an app's detail page the `volume_info` field be set to a default state:

**App Instance Detail JSON when pubkey has not been added yet**

```python
{
  "volume_info": {
    "state": null, # initial state
    "message": null, # nothing to report yet
    "health": "red" # default health state
  },
  "target_state": "running",
  ...
}
```

Finally, if an `AppInstance` has been stopped (`target_state` is not `"running"`), then the health of the volume is assumed to be healthy.

#### What's Better

`AppInstance` in Django Admin will now attempt to display the link domain instead of a UUID for easier browsing.

**Example**

```text
BitWarden - daybed-oink.fractalnetworks.co (AppInstance) - justin@fractalnetworks.co - 8813b625-e219-4390-9a7b-fe0e22c9e50b (User)
```

### 2023-01-03

<hr>

### [Merge Request #163](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#163)

(Enhancement / Bugfix)

#### What's Better

Consolidated the device version fixture data into a single file for easier updating. Hive Device version has been updated to `1.0.3` in order to remove the `tail -f /dev/null` container that was being created due to a logic error in a previous version of the `fractl` CLI.

#### What's Fixed

- Switched all async tasks from using `.delay` to `.apply_async` in order to set the `ignore_result` argument. Otherwise, overtime Celery task results are being stored in RabbitMQ which can ultimately lead to a out of memory error in RabbitMQ.
- Configured Celery to set a default expiration time of 90 seconds for messages in the result backend. This should help prevent messages from being continuously added into the unnamed Celery queues.
### 2023-01-05

<hr>

### [Merge Request #165](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#165)
(Enhancement)

#### What's Better

**Observability**

- Adds more alerting for RabbitMQ:

  - Too many queue messages ready in RabbitMQ. We have previously noticed that we've a build up of these messages which ultimately crashes RabbitMQ, crippling our stack.
  - High memory usage watermark alarm. This will notify us whenever this alarm is raised.

- Storage API
  - Adds a simple alert to inform us of when the Storage API is down.

- Connectivity API
  - Adds a simple alert to inform us of when the Connectivity API is down.

Dashboards

- New Connectivity Dashboard

  - Displays simple metrics:
    - **Number of Links**
    - **Connectivity Link Create Calls**
    - **Connectivity Link Delete Calls**

  - Refinement
    - Names for most dashboards are more readable. Adds more tags.

Connectivity API metrics are now gathered into Prometheus.

### [Merge Request #166](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#166)
(Feature)

#### What's New

Hive Backend will now send emails when exceptions occur when `DJANGO_ENV=PROD`. These emails are sent inside a Celery async task to avoid blocking the main thread.

New Logging Handler:

`apps/api/hive_backend/logging.py/AsyncAdminEmailHandler`

- This handler is basically the [Django AdminEmailHandler](https://docs.djangoproject.com/en/4.1/topics/logging/#adminemailhandler), but instead of sending emails synchronously, are sent inside a Celery async task. This Celery task is described below.

New Logging Filter:

`apps/api/hive_backend/logging.py/RequireProductionTrue`

- Simple filter that checks if the `DJANGO_ENV` setting configured in the `settings.py` is set to `PROD`. This filter allows us to only send emails in production.

New Celery Task:

`apps/api/hive_backend/tasks/notifications.py/send_admin_email`

- Wrapper around Django's `mail_admins`.

### [Merge Request #167](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#167)
(Feature)

#### What's New

Users now have the option to unsubscribe from notifcations sent by the Hive Backend.

New addition to `User` model:

- `notifications_muted`: Boolean field that keeps track of if the user has muted notifcations or not. Defaults to `False`.

Adds migration for the above mentioned User model addition.

Changes to `apps/api/hive_backend/api/utils.py/get_notifier`:

- Get Notifier will now check if the provided `User` has notifications muted or not.

Changes to all notifications:

- If the value of `get_notifier` is `False`, then the notification will not be sent.

New Action on `UserViewSet`

- `unsubscribe_from_notifcations`

  - This action expects `encoded_user_info` passed after `unsubscribe`. This encoded user info is the user's email and uuid concatenated into a comma separated base64 encoded string. Upon receiving a GET request to this endpoint, the `encoded_user_info` is decoded, and the email and uuid is extracted. The User is then fetched from the database, and their `notifications_muted` field is set to `True`. This mutes all notifications for the user. If at any point an error occurs, either while decoding or fetching the user, a 404 page is displayed.

Email Notifier Additions:

`_build_unique_unsubscribe_url`

- This method simply fetches the current user to get their `uuid` and `email`. It then concatenates them into a comma separated string, which is then urlsafe base64 encoded and formatted with the current server's `SITE_URL`. Here's an example:

```python
to_encode = "hello@there.com,6fceeb94-3ab7-497b-8269-3f75b34f059a"
final_url_after_encoding = "https://hive-api.fractalnetworks.co/api/v1/member/unsubscribe/ZWxwdGZvQGdtYWlsLmNvbSw4ODEzYjYyNS1lMjE5LTQzOTAtOWE3Yi1mZTBlMjJjOWU1MGasdfI=/
```

This URL is then added to the context for the template that's being rendered.

Changes to Base Template `base.html`

- `unsubscribe_url` is retrieved from context. This url is displayed at the bottom of the rendered template.

New Template `apps/api/templates/email_templates/unsubscribed.html`

- Displays confirmation that the User has been successfully unsubscribed from notifications.

Emails sent by the Hive Backend now have an unsubscribe link at the bottom of every notification email. This is done by adding the unsubscribe link into `base.html`.

### 2023-01-13

### [Merge Request #170](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#170)
(Enhancement)

Removes the observability stack from the repo in preparation for open source!

- Removes the builds for observability stack from `.gitlab-ci.yml`
- Removes `apps/observability`

### 2023-01-20

### [Merge Request #174](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#174)
(Enhancement, Bugfix)

#### What's Better

Hive Backend will now use `SYSTEM_JWT` when making a link create request to
the Connectivity API.

New Admin Endpoint

- `/api/v1/check-token/`: Accepts a token and if the token is either a
Fractal API Token or Device Token, then the associated token's user uuid is
returned.

Device will now use its `DEVICE_TOKEN` as the authentication token for the link.

**NOTE: This is a potentially breaking change if device is not updated before
starting or stopping an app. This is because of authentication being enabled
on Connectivity.**

#### What's Fixed

- If a link domain is not returned from Connectivity during a link create call,
Hive will set the App Instance's link property into an empty object like so:

```json
{
  "links": {
    "default": {
      "domain": null,
      "token": null
    }
  }
}
```

Increments Device Version to `1.0.5`

### 2023-01-XX


### [Merge Request #180](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#180)
(Bugfix)

#### What's Fixed
- Fixed the issue with expired JWT tokens not being refreshed by next-auth by lowering next-auth session maxAge to 24 hours so that JWT token will be refreshed and updated in the redis cache

<!--
## Changelog Template (Leave at bottom)

### 2023-01-XX

### [Merge Request #NUMBER](https://gitlab.com/fractalnetworks/fractal-mosaic/-/merge_requests/#NUMBER)
(Feature / Enhancement / Debt / Bugfix / Test)

#### What's New
- Describe any new features or functionality.

#### What's Better
- Describe any enhancements that make the product, code, or infrastructure more robust.

#### What's Fixed
- Describe any bugfixes included in the change.

#### What's Debt
- No new technical debt.

#### What's Tested
- Write what is tested and how it is tested. If you aren't testing it add a note about missing tests to the Debt section.
-->
