import os
import logging
import uuid

import requests

from hive_backend.api.models import AppInstance, User
from hive_backend.api.notifiers.email import EmailNotifier

scheduler_logger = logging.getLogger("scheduler")

def generate_link_domain(user_uuid: uuid.UUID, **kwargs):
    '''
    Sends request to Connectivity API to generate a Fractal Link
    '''
    auth_token = os.environ.get("SYSTEM_JWT")

    connectivity_url = os.environ.get("CONNECTIVITY_URL")

    request_headers = {
        "Authorization": f"Bearer {auth_token}",
        "Idempotency-Token": str(uuid.uuid4()),
        "Override-Account-UUID": str(user_uuid) if not os.environ.get("SELF_HOSTED") else '00000000-0000-0000-0000-000000000000'
    }

    body = {
        "region": None,
    }

    link_create_response = requests.post(
        f'{connectivity_url}/api/v1/link/create',
        json=body,
        headers=request_headers
    )

    if not link_create_response.ok:
        scheduler_logger.error("Got HTTP Status (%s) when generating link",
                               link_create_response.status_code)
        return False

    try:
        link_domain = link_create_response.json().get("domain")
    except requests.exceptions.JSONDecodeError:
        return False

    return link_domain

def new_app_installation(instance: AppInstance) -> tuple:
    '''
    Generates new Fractal Link domain and Storage private keys

    NOTE: This is only temporary until the device generates both the
          Fractal Link & Storage Private keys.
    '''
    link_domain = generate_link_domain(instance.owner.uuid)

    # attempt to capture job's returned data
    # FIXME: if generate_link_domain returned False, set the app to error
    if not link_domain:
        return
    # got a link domain back, save it to instance's links
    else:
        LINK_API_TOKEN = os.environ.get("LINK_TOKEN")
        link_api_key = LINK_API_TOKEN

    links = {
        "default": {
            "domain": link_domain,
            "token": link_api_key
        }
    }

    return links

def send_device_request(device, command, payload):
    '''
    Sends commands to a device
    '''
    jwt = os.environ.get("SYSTEM_JWT")
    event_service_url = os.environ.get("EVENT_SERVICE_URL")

    request_headers = {
        "Authorization": f"Bearer {jwt}"
    }

    req_body = {
        "service": "hive-backend",
        "type": "device-request",
        "command": command,
        "account": str(device.owner.uuid),
        "device": str(device.uuid),
        "payload": payload
    }

    response = requests.post(
        f'{event_service_url}/api/v1/request/',
        json=req_body,
        headers=request_headers
    )
    if not response.ok:
        scheduler_logger.error("HTTP Status (%d) sending event to Device. Event: %s",
            response.status_code,
            req_body)
        return False

    scheduler_logger.info("Device ACK: Request for App Instance completed by Device: %s",
        str(req_body))

    return True

def get_notifier(user: User):
    '''
    FIXME: Handle getting default notifier for given user. For now,
    just returning EmailNotifier.
    '''
    # dont return a notifier if the user has muted their notifications
    if user.notifications_muted:
        return False
    else:
        return EmailNotifier(user.email)
