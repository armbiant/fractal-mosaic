import logging
from hive_backend.api.models import AppInstance
from hive_backend.api.serializers import AppInstanceContextSerializer
from hive_backend.scheduler import AppInstanceCurrentState, AppInstanceDesiredState, AppInstanceState
from hive_backend.scheduler.matriarch import schedule
from hive_backend.tasks.executor import command_executor
from django.core.cache import caches


logger = logging.getLogger(__name__)
cache = caches['redis']

def handle_instance_event(owner, device, event):
    logger.debug('handle_instance_event')
    logger.info(event)
    instances_list = event.get('instances', '')

    # get all of user's apps that are attached to the device instance
    device_apps = AppInstance.objects.filter(owner=owner, device=device, reschedule_to_device__isnull=True)
    # get apps that are pending a reschedule to this device
    apps_to_reschedule = AppInstance.objects.filter(owner=owner, reschedule_to_device=device)
    apps_on_other_device = AppInstance.objects.filter(owner=owner, uuid__in=instances_list).exclude(device=device)

    # apps that must be sequentially rescheduled to another device
    for app in apps_to_reschedule:
        # race condition where device was deleted but we are still processing its instances event
        if not app.device:
            continue
        context = AppInstanceContextSerializer(app).data

        # dont try to send stop to device that's offline
        device_in_cache = cache.get(f'seen-{app.device.uuid}', False)
        if not device_in_cache:
            logger.info("Device (%s) is offline. Not sending stop", app.device.uuid)
            continue

        logger.info(
            'Rescheduling app instance (%s) to device (%s)',
            app.uuid,
            device.uuid)

        current_state = AppInstanceCurrentState(
            AppInstanceState.running,
            app.uuid,
            app.device.uuid)
        desired_state = AppInstanceDesiredState(
            AppInstanceState[app.target_state],
            app.uuid,
            device.uuid)

        device_commands = schedule(current_state, desired_state, context)
        logger.debug("Device commands: %s", device_commands)
        command_executor.apply_async(args=(device_commands,), ignore_result=True)

    # apps that should not be running on device
    # should be able to remove this once device stops app on connectivity loss
    for app in apps_on_other_device:
        context = AppInstanceContextSerializer(app).data

        logger.info(
            'Device (%s) shouldnt be running App Instance (%s)',
            device.uuid,
            app.uuid)

        current_state = AppInstanceCurrentState(
            AppInstanceState.running,
            app.uuid,
            device.uuid)
        desired_state = AppInstanceDesiredState(
            AppInstanceState.stopped,
            app.uuid,
            device.uuid)

        device_commands = schedule(current_state, desired_state, context)
        logger.debug("Device commands: %s", device_commands)
        command_executor.apply_async(args=(device_commands,), ignore_result=True)

    # app is associated with device. Determine whether app should
    # should be started / stopped / marked healthy
    for app in device_apps:
        # generate app context
        context = AppInstanceContextSerializer(app).data

        # request stop event if the device is running an app that it shouldn't
        # TODO handle pausing so we don't delete volumes
        if str(app.uuid) in instances_list and app.target_state != 'running':
            current_state = AppInstanceCurrentState(
                AppInstanceState.running,
                app.uuid,
                device.uuid)
            desired_state = AppInstanceDesiredState(
                AppInstanceState.stopped, # TODO. use app.target_state here. Device currently doesn't support "paused"
                app.uuid,
                device.uuid)
            device_commands = schedule(current_state, desired_state, context)
            logger.debug("Device commands: %s", device_commands)
            command_executor.apply_async(args=(device_commands,), ignore_result=True)

        # request start event if the device is not running an app that it should
        elif str(app.uuid) not in instances_list and app.target_state == 'running':
            current_state = AppInstanceCurrentState(
                AppInstanceState.stopped,
                app.uuid,
                device.uuid)
            desired_state = AppInstanceDesiredState(
                AppInstanceState.running,
                app.uuid,
                device.uuid)
            device_commands = schedule(current_state, desired_state, context)
            logger.debug("Device commands: %s", device_commands)
            command_executor.apply_async(args=(device_commands,), ignore_result=True)

        # update app health to green since device is running the app like expected
        elif str(app.uuid) in instances_list and app.target_state == "running":
            if app.health != "green":
                app.health = "green"
                app.save()

        # update app health to red since device is not running the app like expected
        elif str(app.uuid) not in instances_list and app.target_state == "stopped":
            if app.health != "red":
                app.health = "red"
                app.save()
    return True