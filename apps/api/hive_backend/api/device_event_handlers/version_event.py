from hive_backend.api.models import LatestDeviceVersion, Device
from hive_backend.tasks.executor import command_executor

def handle_version_event(device: Device, event: dict) -> bool:
    '''
    Handles version events from the Device. Updates the device's current
    version to the version reported by the device.

    TODO: Compare the device's version to the latest device version. Send
    update message to the device if the device's version doesn't match.

    Params:
    - device(Device): Device object that is reporting its version.
    - event(dict): The raw event sent by the device.

    Returns:
    - True if an update message was sent.
    - False if an update message was not sent.
    '''
    print(f'handle_version_event called')

    version = event.get("version")
    auto_updates = event.get('auto_updates', True)

    # update stored device version if it's different
    if version != device.version:
        device.version = version
        device.save()

    # compare the device's current version to the latest version
    # TODO: Handle major / minor / patch version differences
    latest = LatestDeviceVersion.objects.get()

    # device version is expected to be in sync.
    # if version is higher or lower, then an upgrade message will be sent.
    if auto_updates and latest.version != version:
        # send upgrade message to the device
        device_command = [{
            "account": device.owner.uuid,
            "device": device.uuid,
            "command": "UpgradeDevice",
            "version": latest.version
        }]
        command_executor.apply_async(args=(device_command,), ignore_result=True)
        return True
    return False
