import logging

import hive_backend.api.utils as utils
from hive_backend.api.models import User, Device

logger = logging.getLogger(__name__)

def handle_device_state_event(owner: User, device: Device, event: dict):
    '''
    Manages device state changes.
    '''
    logger.info(f"Got device ({device}) state message: {event}")

    payload = event.get("payload")
    state_event = payload.get("event")

    # determine what state has changed
    if state_event == "low_disk_space":
        notifier = utils.get_notifier(owner)
        notifier.low_disk_space_warning(device.name)
