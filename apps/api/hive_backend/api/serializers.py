from rest_framework import serializers
from django.conf import settings
from api_token.serializers import DeviceTokenSerializer
from hive_backend.api.models import AppInstance, Device, User, AppInstanceConfig


class AppInstanceSerializer(serializers.ModelSerializer):
    # Add device name to app instance to avoid a separate fetch to device endpoint with primary key
    device_name = serializers.CharField(source='device.name', read_only=True)

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        # send url to image instead of sending raw image
        if not representation.get('image'):
            representation['image'] = f'{settings.SITE_URL}{instance.app.image.url}'
        else:
            representation['image'] = f'{settings.SITE_URL}{instance.image.url}'

        return representation

    def create(self, validated_data):
        owner = validated_data["owner"].uuid
        instance, _ = AppInstance.objects.update_or_create(
            owner=owner,
            app=validated_data["app"],
            defaults=validated_data)
        return instance

    class Meta:
        model = AppInstance
        exclude = ['storage_apikeys', 'encrypted_storage_apikeys']
        read_only_fields = ['owner']


class AppInstanceUuidSerializer(serializers.ModelSerializer):


    class Meta:
        model = AppInstance
        fields = ('uuid',)


class AppInstanceContextSerializer(serializers.ModelSerializer):
    application = serializers.JSONField(required=False)
    instance = serializers.UUIDField(source='uuid')
    account = serializers.UUIDField(source='owner.uuid')


    class Meta:
        model = AppInstance
        fields = ['links', 'storage_apikeys', 'encrypted_storage_apikeys', 'instance', 'application', 'account']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['application'] = {
            "appstore": instance.app.catalog.name,
            "name": instance.name.lower(),
            "version": "latest" # FIXME: version hardcoded to latest for now
        }
        return representation


class AppInstanceConfigSerializer(serializers.ModelSerializer):


    class Meta: 
        model = AppInstanceConfig
        fields = ['appinstance', 'data', 'uuid']


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required= False)
    app_catalogs = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    password = serializers.CharField(required=False, write_only=True)


    class Meta:
        model = User
        fields = '__all__'


class DeviceSerializer(serializers.ModelSerializer):
    name = serializers.CharField(required=True)
    token = DeviceTokenSerializer(required=False)


    class Meta:
        model = Device
        fields = '__all__'
        read_only_fields = ['owner']

    def create(self, validated_data):
        '''
        If the Device name already exists, return that instance
        instead of creating a new one. This allows us to only ever
        create one device per given name.
        '''
        instance, _ = Device.objects.get_or_create(**validated_data)
        return instance
