from django.conf.urls import include
from django.urls import re_path
from rest_framework import routers
from hive_backend.api.views import AppInstanceViewSet, UserViewSet, DeviceViewSet, AppInstanceConfigViewSet
from api_token.views import CheckFractalAPITokenView, FractalAPITokenViewSet, CheckDeviceTokenView, CheckTokenView

router = routers.DefaultRouter()

router.register(r'app', AppInstanceViewSet, basename='app')
router.register(r'member', UserViewSet, basename='member')
router.register(r'device', DeviceViewSet, basename='device')
router.register(r'token', FractalAPITokenViewSet, basename='token')
router.register(r'appinstance_config', AppInstanceConfigViewSet, basename='appconfig')

urlpatterns = [
    re_path(r'', include(router.urls)),
    re_path(r'^check-api-token', CheckFractalAPITokenView.as_view(), name='check-api-token'),
    re_path(r'^check-device-token', CheckDeviceTokenView.as_view(), name='check-device-token'),
    re_path(r'^check-token', CheckTokenView.as_view(), name='check-token'),
]
