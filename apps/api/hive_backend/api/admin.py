from django.contrib import admin
from hive_backend.api.models import AppInstance, User, AppInstanceConfig, DNSConfig, AppInstanceVolume, Device, LatestDeviceVersion

# Register your models here.
admin.site.register(AppInstance)
admin.site.register(User)
admin.site.register(AppInstanceConfig)
admin.site.register(DNSConfig)
admin.site.register(AppInstanceVolume)
admin.site.register(Device)
admin.site.register(LatestDeviceVersion)
