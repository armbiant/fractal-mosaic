from rest_framework import serializers


class AppInstanceDesiredStateSerializer(serializers.Serializer):
    state = serializers.CharField()
    app_instance_id = serializers.UUIDField()
    device_id = serializers.UUIDField()
    force = serializers.BooleanField()


class DeviceCommandSerializer(serializers.Serializer):
    command = serializers.CharField()
    state = AppInstanceDesiredStateSerializer()
    payload = serializers.JSONField()
    device = serializers.UUIDField()
    account = serializers.UUIDField()
