from enum import Enum
from typing import NamedTuple
from uuid import UUID


class AppInstanceState(Enum):
    running = "running"
    stopped = "stopped"
    installed = "installed"
    uninstalled = "uninstalled"


class AppInstanceCurrentState(NamedTuple):
    state: AppInstanceState
    app_instance_id: UUID
    device_id: UUID


class AppInstanceDesiredState(NamedTuple):
    state: AppInstanceState
    app_instance_id: UUID
    device_id: UUID
    force: bool = False # If True, backend can make request to storage backend to change writer


class SchedulerAction(Enum):
    create_volume = "create_volume"
    remove_volume = "remove_volume"
    lock = "lock"
    unlock = "unlock"
    start = "start"
    stop = "stop"
    standby = "standby" # Create all volumes for App Instance
    take_snapshot = "take_snapshot"


class DeviceCommand(NamedTuple):
    command: SchedulerAction
    state: AppInstanceDesiredState
    payload: dict
    device: UUID
    account: UUID
