from django.apps import AppConfig


class AppCatalogConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hive_backend.app_catalog'
    verbose_name = "App Catalog"