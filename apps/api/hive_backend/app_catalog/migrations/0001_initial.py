# Generated by Django 4.0.4 on 2022-08-19 21:58

from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='App',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
                ('image', models.FileField(null=True, upload_to='./app_catalog')),
                ('subtitle', models.CharField(blank=True, max_length=255)),
                ('description', models.TextField(blank=True)),
                ('hero_image', models.FileField(blank=True, null=True, upload_to='./app_catalog_hero_img')),
                ('content', models.TextField(blank=True)),
                ('github_url', models.URLField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AppCatalog',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
                ('repo_url', models.URLField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AppCategory',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AppResource',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=255)),
                ('url', models.URLField()),
                ('app', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_catalog.app')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='AppGalleryImage',
            fields=[
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('deleted', models.BooleanField(default=False)),
                ('url', models.FileField(upload_to='./app_catalog_gallery')),
                ('caption', models.CharField(max_length=100)),
                ('app', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_catalog.app')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='app',
            name='catalog',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='app_catalog.appcatalog'),
        ),
        migrations.AddField(
            model_name='app',
            name='categories',
            field=models.ManyToManyField(blank=True, to='app_catalog.appcategory'),
        ),
    ]
