from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from .models import App
from .serializers import AppSerializer
from rest_framework import permissions
from django.db.models import Q
from operator import or_
from functools import reduce
from hive_backend.api.models import AppInstance

# Create your views here.

class AppCatalogViewSet(ModelViewSet):
    queryset = App.objects.all().prefetch_related('appresource_set', 'appgalleryimage_set')
    serializer_class = AppSerializer

    # def get_queryset(self):
    #     featured = self.request.query_params.get('featured', None)
    #     if featured is not None:
    #         installed_app_list = AppInstance.objects.filter(state='installed').values_list('app__uuid', flat=True)
    #         return super().get_queryset().exclude(uuid__in=installed_app_list)

    #     return super().get_queryset()

    def get_permissions(self):

        if self.action == "list" or self.action == 'retrieve':
            return [permissions.AllowAny()]

        return [permissions.IsAuthenticated()]

class AppCatalogSearchViewSet(ModelViewSet):
    serializer_class = AppSerializer

    def get_queryset(self):
        categories = self.request.GET.getlist('categories')
        search = self.request.query_params.get('search', None)
        sort = self.request.query_params.get('sort', 'asc')

        if len(categories) > 0 and search is not None:
            return App.objects.filter(reduce(or_, (Q(categories__name__icontains=category) for category in categories))| Q(name__icontains=search)).order_by(f'{"name" if sort == "asc" else "-name"}')
        elif search is not None:
            return App.objects.filter(name__icontains=search).order_by(f'{"name" if sort == "asc" else "-name"}')
        elif len(categories) > 0:
            return App.objects.filter(reduce(or_, (Q(categories__name__icontains=category) for category in categories))).order_by(f'{"name" if sort == "asc" else "-name"}')
        else:
            return App.objects.all().prefetch_related('appresource_set', 'appgalleryimage_set').order_by(f'{"name" if sort == "asc" else "-name"}')

    def get_permissions(self):

        if self.action == "list" or self.action == 'retrieve':
            return [permissions.AllowAny()]

        return [permissions.IsAuthenticated()]


