from django.conf.urls import include
from django.urls import re_path
from rest_framework import routers
from .views import AppCatalogViewSet, AppCatalogSearchViewSet

router = routers.DefaultRouter()

router.register(r'app', AppCatalogViewSet, basename='app')
router.register(r'search', AppCatalogSearchViewSet, basename='search')

urlpatterns = [
    re_path(r'', include(router.urls)),
]