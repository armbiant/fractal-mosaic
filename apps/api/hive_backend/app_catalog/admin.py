from django.contrib import admin
from hive_backend.app_catalog.models import AppCatalog, App, AppCategory, AppGalleryImage, AppResource

# Register your models here.
admin.site.register(AppCatalog)
admin.site.register(App)
admin.site.register(AppGalleryImage)
admin.site.register(AppResource)
admin.site.register(AppCategory)
