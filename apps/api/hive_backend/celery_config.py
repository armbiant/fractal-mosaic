import os
from datetime import timedelta

from celery import Celery
from celery.schedules import crontab
from celery.signals import setup_logging

# interval (in seconds) for scheduled tasks to run
SCHEDULED_TASK_INTERVAL = 10
# maximum duration (in seconds) before something is considered expired
MAX_PING_DURATION = 60

# Celery configuration
# Set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'hive_backend.settings')
app = Celery('hive_backend')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.conf.timezone = 'UTC'

# only hold messages in the result backend for 90 seconds.
# this gurantees results are never backing up in the Celery queues because
# are not consuming them.
app.conf.result_expires = 90

app.conf.beat_schedule = {
    # marks devices that have not pinged us in SCHEDULER_TASK_INTERVAL seconds as unhealthy
    'mark_devices_unhealthy': {
        'task': 'hive_backend.tasks.executor.mark_devices_unhealthy',
        'schedule': timedelta(seconds=SCHEDULED_TASK_INTERVAL),
        'args': ()
    },
    # sends a weekly message to all users that haven't launched a device yet.
    'launch_device_reminder': {
        'task': 'hive_backend.tasks.notifications.send_launch_device_reminder',
        'schedule': crontab(
            minute='0',
            hour='9', # 9 AM
            day_of_week='6', # Saturday
        ),
        'args': ()
    },
    # weekly task that sends a check_disk_space command to all healthy devices.
    'disk_space_check': {
        'task': 'hive_backend.tasks.hardware_checks.disk_space_check',
        'schedule': crontab(
            minute='0',
            hour='0',
            day_of_week='1',
        ),
        'args': ()
    },
}

@setup_logging.connect
def config_loggers(*args, **kwags):
    from logging.config import dictConfig
    from django.conf import settings
    dictConfig(settings.LOGGING)