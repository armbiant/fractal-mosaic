"""hive_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import re_path, path
from django.conf.urls import include

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    re_path(r"^api/v1/catalog/", include("hive_backend.app_catalog.urls")), 
    re_path(r"^api/v1/billing/", include("billing.urls")), 
    re_path(r"^api/v1/", include("hive_backend.api.urls")), 
    re_path(r"^admin/", admin.site.urls),
    re_path(r"^health/", include("health_check.urls")),
    path("", include("django_prometheus.urls")),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
