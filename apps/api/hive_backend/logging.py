import logging

from django.conf import settings
from django.utils.log import AdminEmailHandler


class RequireProductionTrue(logging.Filter):
    def filter(self, record):
        if settings.DJANGO_ENV == 'PROD':
            return True
        return False


class AsyncAdminEmailHandler(AdminEmailHandler):
    '''
    Subclasses the default Django AdminEmailHandler to enable sending an email
    asynchronously
    '''
    def send_mail(self, subject, message, *args, **kwargs):
        '''
        Send email using a Celery async task.
        '''
        from hive_backend.tasks.notifications import send_admin_email

        send_admin_email.apply_async(
            args=(subject, message, *args),
            kwargs={
                'connection': None,
                **kwargs
            },
            ignore_result=True)
