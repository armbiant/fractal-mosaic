from rest_framework.test import APIClient
from django.core.cache import caches
from django.core.management import call_command
from django.conf import settings
from hive_backend.api.models import User, AppInstance, Device
from hive_backend.api.serializers import AppInstanceContextSerializer
from hive_backend.app_catalog.models import App
from hive_backend.scheduler import AppInstanceState
from hive_backend.scheduler.matriarch import AppInstanceDesiredState, AppInstanceCurrentState, schedule
from uuid import uuid4
import pytest
import os

@pytest.fixture(scope='session')
def django_db_setup(django_db_setup, django_db_blocker):
    with django_db_blocker.unblock():
        call_command('loaddata', 'fixtures/initial_data.yaml')

@pytest.fixture(scope='session')
def authenticated_client():
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION='Token ' + 'cb904e30dd7997a92321c8fdda27b0a2b2992f8d')
    return client

@pytest.fixture(scope='session')
def admin_authenticated_client():
    client = APIClient()
    client.credentials(HTTP_AUTHORIZATION='Bearer ' + settings.SYSTEM_JWT)
    return client

@pytest.fixture(scope='session')
def test_user_uuid():
    return '78877f78-f15d-46af-be55-0e8f8b0196b0'

@pytest.fixture(scope='session')
def test_device_uuid():
    return '5ae97ce7-388d-4faf-8d5e-8ce3a7d001f9'

@pytest.fixture(scope='session')
def test_device_token():
    return '6cad02c15f5b7222c5e2ec5642c955c68f7abbb0'

@pytest.fixture(scope='session')
def test_app_uuid():
    return '29b46c46-6b9e-4ddf-b09d-39007e6b6ce1'

@pytest.fixture(scope='session')
def test_app_instance_uuid():
    return '5d13b011-d5c4-40d5-b79d-bed2d6a677df'

@pytest.fixture(scope='session')
def fractal_cloud_device_uuid():
    return '41e35656-349f-4d69-9e5d-6bbc735cf85e'

@pytest.fixture(scope='session')
def element_app_uuid():
    return 'a0abcd96-7772-40c8-953e-de4cba0d3a1b'

@pytest.fixture
def test_cache():
    '''
    Yields cache then clears it on test completion
    TODO: mock redis cache
    '''
    cache = caches['redis']
    yield cache
    cache.clear()

@pytest.fixture
def test_device(test_device_uuid):
    return Device.objects.get(uuid=test_device_uuid)

@pytest.fixture
def mock_link_generation(requests_mock):
    '''
    Mocks the link create HTTP request.
    '''
    CONNECTIVITY_URL = os.environ["CONNECTIVITY_URL"]
    requests_mock.post(f'{CONNECTIVITY_URL}/api/v1/link/create', json={"domain": "test-link.fractal.pub"}, status_code=201)

@pytest.fixture
def mock_device_request_success(requests_mock):
    '''
    Mocks the device request HTTP request.
    '''
    DEVICE_SERVICE_URL = os.environ.get("EVENT_SERVICE_URL")
    requests_mock.post(f'{DEVICE_SERVICE_URL}/api/v1/request/', json={}, status_code=201)

@pytest.fixture
def mock_device_request_fail(requests_mock):
    '''
    Mocks the device request HTTP request.
    '''
    DEVICE_SERVICE_URL = os.environ.get("EVENT_SERVICE_URL")
    requests_mock.post(f'{DEVICE_SERVICE_URL}/api/v1/request/', json={}, status_code=400)

@pytest.fixture
def create_app_instance(test_user_uuid, test_device_uuid, mock_link_generation, mock_device_request_success):

    def create_instance(app_uuid, state, target_state):
        '''
        Creates and returns an App Instance when given:
        - app_uuid
        - state
        - target_state
        '''
        owner = User.objects.get(uuid=test_user_uuid)
        device = Device.objects.get(uuid=test_device_uuid)
        app = App.objects.get(uuid=app_uuid)
        instance = AppInstance.objects.create(
            owner=owner,
            device=device,
            app=app,
            name=app.name,
            state=state,
            target_state=target_state,
            health="green")
        return instance

    return create_instance

@pytest.fixture
def test_app_instance(create_app_instance, test_app_uuid):
    '''
    Returns an AppInstance that belongs to test_user_uuid.
    '''
    instance = create_app_instance(test_app_uuid, "installed", "running")
    return instance

@pytest.fixture
def test_app_instance_context(test_app_instance):
    '''
    Generates a serialized context for an app instance.
    '''
    context = AppInstanceContextSerializer(test_app_instance).data
    return context

@pytest.fixture
def generate_device_commands(test_app_instance_context, test_app_instance_uuid, test_device_uuid):

    def generate_commands(sequence: bool):
        '''
        Generates a list of device commands
        '''
        if sequence:
            device2 = str(uuid4())
            current_state = AppInstanceCurrentState(
                AppInstanceState.running,
                test_app_instance_uuid,
                test_device_uuid)
            desired_state = AppInstanceDesiredState(
                AppInstanceState.running,
                test_app_instance_uuid,
                device2)
            commands = schedule(current_state, desired_state, test_app_instance_context)
        else:
            current_state = AppInstanceCurrentState(
                AppInstanceState.stopped,
                test_app_instance_uuid,
                test_device_uuid)
            desired_state = AppInstanceDesiredState(
                AppInstanceState.running,
                test_app_instance_uuid,
                test_device_uuid)
            commands = schedule(current_state, desired_state, test_app_instance_context)
        return commands

    return generate_commands
