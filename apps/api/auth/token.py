from rest_framework_simplejwt.tokens import AccessToken


class KeycloakToken(AccessToken):
    '''Overrides the access token type'''
    token_type = 'Bearer' 