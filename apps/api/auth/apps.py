from tabnanny import verbose
from django.apps import AppConfig


class AuthConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'auth'
    verbose_name = 'Fractal JWT Authentication'
    label = 'authconfig'
