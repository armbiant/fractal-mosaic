from django.utils.translation import gettext_lazy as _
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework_simplejwt.exceptions import AuthenticationFailed
from rest_framework_simplejwt.settings import api_settings
from django.core.exceptions import ObjectDoesNotExist
from auth.models import FractalTokenUser


class FractalJWTAuthentication(JWTAuthentication):
    """
    An authentication plugin that authenticates requests through a JSON web
    token provided in a request header.
    """
    def authenticate(self, request):
        request_path = request.path
        request.user_was_created = False

        # get token from Authentication header
        header = self.get_header(request)
        if header is None:
            return None

        raw_token = self.get_raw_token(header)
        if raw_token is None:
            return None

        validated_token = self.get_validated_token(raw_token)

        # check if the validated_token has the system scope.
        # If so, request is authenticated. Create a FractalTokenUser for the request.
        if "system" in validated_token['scope']:
            user = FractalTokenUser(validated_token)
            return user, validated_token

        # check if this is a request to the join endpoint.
        # This endpoint is the only endpoint that is allowed to get or create
        if request_path == "/api/v1/member/join/":
            try:
                user = self.user_model.objects.get(uuid=validated_token[api_settings.USER_ID_CLAIM])
            except ObjectDoesNotExist:
                user = self.user_model.objects.create(
                    uuid = validated_token[api_settings.USER_ID_CLAIM],
                    email = validated_token.get('email', ''),
                    display_name = validated_token.get('username', ''),
                    is_admin = True if 'system' in validated_token['scope'] else False)
                request.user_was_created = True
            return user, validated_token

        else:
            return self.get_user(validated_token), validated_token


    def get_user(self, validated_token):
        """
        Finds or creates a User based off of the validated token's uuid
        """

        try:
            user = self.user_model.objects.get(uuid=validated_token[api_settings.USER_ID_CLAIM])
        except ObjectDoesNotExist:
            raise AuthenticationFailed("User does not exist.")

        if not user.is_active:
            raise AuthenticationFailed(_('User is inactive'), code='user_inactive')

        return user
