from rest_framework import serializers
from api_token.models import DeviceToken, FractalAPIToken


class FractalAPITokenSerializer(serializers.ModelSerializer):


    class Meta: 
        model = FractalAPIToken
        fields = '__all__'
        read_only_fields = ('created_date', 'uuid', 'owner', 'key')


class DeviceTokenSerializer(serializers.ModelSerializer):


    class Meta: 
        model = DeviceToken
        fields = '__all__'
        read_only_fields = ('created_date', 'uuid')
    
    # def create(self, validated_data):
    #     return super().create(validated_data)