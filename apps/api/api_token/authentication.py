from django.utils.translation import gettext_lazy as _
from rest_framework.authentication import TokenAuthentication

from rest_framework import exceptions
from api_token.models import FractalAPIToken

class FractalTokenAuthentication(TokenAuthentication):
    model = FractalAPIToken

    def authenticate_credentials(self, key):
        model = self.get_model()

        try:
            token = model.objects.select_related('owner').get(key=key)
        except model.DoesNotExist:
            raise exceptions.AuthenticationFailed(_('Invalid token.'))

        return (token.owner, token)
