from hive_backend.api.views import create_element_user
import pytest

pytestmark = pytest.mark.django_db

'''
TODO: Figure out a way to consolidate these two tests into one.
      To do so, need to figure out how to combine the two
      mock_device_request fixtures.
'''
def test_element_create_admin_user_success(element_app_uuid, create_app_instance, mock_device_request_success):
    '''
    Ensure if request is made to the device to create an admin user in Element succeeds,
    that create_element_user returns True.
    '''
    element_instance = create_app_instance(element_app_uuid, "installed", "running")
    device_request_success = create_element_user("test_username", "test_password", element_instance.device, element_instance)
    assert device_request_success == True

def test_element_create_admin_user_fail(element_app_uuid, create_app_instance, mock_device_request_fail):
    '''
    Ensure if request is made to the device to create an admin user in Element fails,
    that create_element_user returns False.
    '''
    element_instance = create_app_instance(element_app_uuid, "installed", "running")
    device_request_success = create_element_user("test_username", "test_password", element_instance.device, element_instance)
    assert device_request_success == False
