from django.http import QueryDict
from unittest.mock import patch
from hive_backend.api.device_event_handlers.version_event import handle_version_event
import pytest

DEVICE_EVENT_ENDPOINT = '/api/v1/device/event/'

pytestmark = pytest.mark.django_db

def test_update_not_required_returns_false(test_device, admin_authenticated_client):
    device_event = {
        "service": "hive-device",
        "type": "device-version",
        "account": "78877f78-f15d-46af-be55-0e8f8b0196b0",
        "device": str(test_device.uuid),
        "address": "172.18.0.16:44316",
        "auto_updates": True,
        "version": "1.0.0"
    }
    updated = handle_version_event(test_device, device_event)
    assert updated == False


def test_auto_updates_disabled_returns_false(test_device, mock_device_request_success):
    '''
    Ensure that when auto updates are disabled, then an update message is not sent
    '''
    device_event = {
        "service": "hive-device",
        "type": "device-version",
        "account": "78877f78-f15d-46af-be55-0e8f8b0196b0",
        "device": str(test_device.uuid),
        "address": "172.18.0.16:44316",
        "auto_updates": False,
        "version": "1.0.2"
    }
    updated = handle_version_event(test_device, device_event)
    assert updated == False

def test_update_required_returns_true(test_device, mock_device_request_success):
    '''
    Ensure that when device required update, an update message is sent
    '''
    device_event = {
        "service": "hive-device",
        "type": "device-version",
        "account": "78877f78-f15d-46af-be55-0e8f8b0196b0",
        "device": str(test_device.uuid),
        "address": "172.18.0.16:44316",
        "auto_updates": True,
        "version": "0.0.1"
    }
    updated = handle_version_event(test_device, device_event)
    assert updated == True

def test_version_event_from_endpoint(test_device, admin_authenticated_client, mock_device_request_success):
    client = admin_authenticated_client

    device_event = {
        "service": "hive-device",
        "type": "device-version",
        "account": "78877f78-f15d-46af-be55-0e8f8b0196b0",
        "device": str(test_device.uuid),
        "address": "172.18.0.16:44316",
        "auto_updates": 'True',
        "version": "1.0.0"
    }

    with patch('hive_backend.api.views.handle_version_event', return_value=None) as patched:
        resp = client.post(DEVICE_EVENT_ENDPOINT, data=device_event)
        assert resp.status_code == 200

        # convert device_event dictionary into QueryDict since rest_framework APIClient data is a QueryDict
        message = QueryDict('', mutable=True)
        message.update(device_event)

        patched.assert_called_once_with(test_device, message)
