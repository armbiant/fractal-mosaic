function drawLines() {
	// var canvas = document.getElementById('stepsCanvas')
	// var canvas2 = document.getElementById('dataCanvas')
	var canvas3 = document.getElementById('internetGateway')

	// set the canvas dimensions
	function setDimensions(el) {
		el.width = el.getBoundingClientRect().width
		el.height = el.getBoundingClientRect().height
	}

	// set the width and height properties of the canvases manually
	// setDimensions(canvas)
	// setDimensions(canvas2)
	setDimensions(canvas3)

	// get the position of the items we want to draw lines between
	// var dotOne = document.getElementById('dotOne')
	// var dotTwo = document.getElementById('dotTwo')
	// var dotThree = document.getElementById('dotThree')

	// var bigDot = document.getElementById('bigDot')
	// var smallDot = document.getElementById('smallDot')

	var internet = document.getElementById('internet')
	var gateway = document.getElementById('gateway')

	// determines the x/y position of canvas from the document
	var canvasDimensions = function (param, element) {
		var position = element.getBoundingClientRect()

		var topXdoc = position.x + -window.scrollX
		var topYdoc = position.y + -window.scrollY

		if (param == 'y' || param == 'Y') {
			pos = topYdoc
		}
		if (param == 'x' || param == 'X') {
			pos = topXdoc
		}

		return pos
	}

	// determine the center of an object in relation to the canvas
	var findCenter = function (canvas, element) {
		var position = element.getBoundingClientRect()

		var centerXviewport = position.width / 2 + position.x
		var centerXdoc = centerXviewport + -window.scrollX
		var centerXcanvas = centerXdoc - canvasDimensions('x', canvas)

		var centerYviewport = position.height / 2 + position.y
		var centerYdoc = centerYviewport + -window.scrollY
		var centerYcanvas = centerYdoc - canvasDimensions('y', canvas)

		var pos = {x: centerXcanvas, y: centerYcanvas}

		return pos
	}

	// draws the lines
	// var dotOnePos = findCenter(canvas, dotOne)
	// var dotTwoPos = findCenter(canvas, dotTwo)
	// var dotThreePos = findCenter(canvas, dotThree)

	// var dotSmallPos = findCenter(canvas2, smallDot)
	// var dotBigPos = findCenter(canvas2, bigDot)

	var internetPos = findCenter(canvas3, internet)
	var gatewayPos = findCenter(canvas3, gateway)

	// var ctx = canvas.getContext('2d')
	// var ctx2 = canvas2.getContext('2d')
	var ctx3 = canvas3.getContext('2d')

	// ctx.beginPath()
	// ctx2.beginPath()
	ctx3.beginPath()

	// ctx.lineWidth = 2
	// ctx2.lineWidth = 2
	ctx3.lineWidth = 2

	// ctx.moveTo(dotOnePos.x, dotOnePos.y)
	// ctx.lineTo(dotTwoPos.x, dotTwoPos.y)
	// ctx.lineTo(dotThreePos.x, dotThreePos.y)

	// ctx2.moveTo(dotSmallPos.x, dotSmallPos.y)
	// ctx2.lineTo(dotBigPos.x, dotBigPos.y)

	ctx3.moveTo(internetPos.x, internetPos.y)
	ctx3.lineTo(gatewayPos.x, gatewayPos.y)

	// ctx.strokeStyle = '#606d6d77'
	// ctx2.strokeStyle = '#606d6d77'
	ctx3.strokeStyle = '#606d6d77'

	// ctx.stroke()
	// ctx2.stroke()
	ctx3.stroke()

	// ctx.beginPath()
	// ctx2.beginPath()
	ctx3.beginPath()
}

//reset height
var resetHeight = function (target) {
	// var canvas = document.getElementById('stepsCanvas')
	// var canvas2 = document.getElementById('dataCanvas')
	var canvas3 = document.getElementById('internetGateway')

	// canvas.height = 0
	// canvas2.height = 0
	canvas3.height = 0
}

// wait until the document loads to draw the lines
window.onload = function () {
	drawLines()
}

// if the window is resized, go a head and redraw the lines
var timeoutFlag

function resized() {
	drawLines()
}

window.onresize = function () {
	// resetHeight('stepsCanvas')
	// resetHeight('dataCanvas')
	resetHeight('internetCanvas')

	clearTimeout(timeoutFlag)

	timeoutFlag = setTimeout(resized, 100)
}
