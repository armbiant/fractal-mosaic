// Multiple Exports Exmaple
// https://stackoverflow.com/a/65612556

// Path
const path = require('path')

// Next-Transpile-Modules
const withTM = require('next-transpile-modules')(['components', 'data', 'hooks', 'types', `util`, 'auth'])

// Plausible
const {withPlausibleProxy} = require('next-plausible')

// Base Config
/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	sassOptions: {
		includePaths: [path.join(__dirname, 'styles')],
	},
}

module.exports = withTM(withPlausibleProxy()({nextConfig}))
