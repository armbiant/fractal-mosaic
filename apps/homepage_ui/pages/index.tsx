// Next JS
import type {NextPage} from 'next'
import Script from 'next/script'

// Next Auth
import {useSession} from 'next-auth/react'

import MonoNav from 'components/MonoNav/MonoNav'
import MonoMobileNav from 'components/MonoNav/MonoMobileNav'

const Home: NextPage = () => {
	return (
		<>
			<Script src='/script.js' strategy='afterInteractive' />
			<svg className='icon-sprite-sheet' focusable='false'>
				<symbol id='long-arrow-alt-right' viewBox='0 0 448 512'>
					<path d='M313.941 216H12c-6.627 0-12 5.373-12 12v56c0 6.627 5.373 12 12 12h301.941v46.059c0 21.382 25.851 32.09 40.971 16.971l86.059-86.059c9.373-9.373 9.373-24.569 0-33.941l-86.059-86.059c-15.119-15.119-40.971-4.411-40.971 16.971V216z'></path>
				</symbol>
				<symbol id='question-circle' viewBox='0 0 512 512'>
					<path d='M256 8C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm0 448c-110.532 0-200-89.431-200-200 0-110.495 89.472-200 200-200 110.491 0 200 89.471 200 200 0 110.53-89.431 200-200 200zm107.244-255.2c0 67.052-72.421 68.084-72.421 92.863V300c0 6.627-5.373 12-12 12h-45.647c-6.627 0-12-5.373-12-12v-8.659c0-35.745 27.1-50.034 47.579-61.516 17.561-9.845 28.324-16.541 28.324-29.579 0-17.246-21.999-28.693-39.784-28.693-23.189 0-33.894 10.977-48.942 29.969-4.057 5.12-11.46 6.071-16.666 2.124l-27.824-21.098c-5.107-3.872-6.251-11.066-2.644-16.363C184.846 131.491 214.94 112 261.794 112c49.071 0 101.45 38.304 101.45 88.8zM298 368c0 23.159-18.841 42-42 42s-42-18.841-42-42 18.841-42 42-42 42 18.841 42 42z'></path>
				</symbol>
				<symbol id='times' viewBox='0 0 352 512'>
					<path d='M242.72 256l100.07-100.07c12.28-12.28 12.28-32.19 0-44.48l-22.24-22.24c-12.28-12.28-32.19-12.28-44.48 0L176 189.28 75.93 89.21c-12.28-12.28-32.19-12.28-44.48 0L9.21 111.45c-12.28 12.28-12.28 32.19 0 44.48L109.28 256 9.21 356.07c-12.28 12.28-12.28 32.19 0 44.48l22.24 22.24c12.28 12.28 32.2 12.28 44.48 0L176 322.72l100.07 100.07c12.28 12.28 32.2 12.28 44.48 0l22.24-22.24c12.28-12.28 12.28-32.19 0-44.48L242.72 256z'></path>
				</symbol>
			</svg>
			<div style={{display: 'flex', flexDirection: 'column', width: '100%', height: '100%'}}>
				<MonoNav currentDomain='Home' useSession={useSession} />
				<div>
					{/* <nav className="nav-wrapper">
						<ul className="grid">
							<li className="nav-home">
								<a tabIndex={0} href="#freedom" className="nav-link">Fractal Networks / Mosaic</a>
							</li>
							<li className="nav-demo">
								<a href="#" className="nav-link">Demo</a>
							</li>
							<li className="nav-install">
								<a href="#alphaCode" className="nav-link">Install Now</a>
							</li>
						</ul>
					</nav> */}
					<main>
						{/* <!--Hero--> */}
						<section className='hero-wrapper bg-dark' id='freedom'>
							<div className='grid'>
								<header className='hero-title'>
									<h1 className='light'>Freedom from the corporate cloud.</h1>
									<p className='light'>
										Mosaic combines your personal computers to create a personal cloud. Replace paid cloud services with
										free and open-source apps under your control.
									</p>
								</header>
								<div className='hero-cta'>
									<a className='btn btn-light btn-lg' href='#alphaCode'>
										<span className='btn-txt'>Get Started</span>
									</a>
									{/* <!-- <a href="#alpha-code-modal" className="btn btn-dark btn-secondary btn-lg" type="button">
										<span className="btn-txt">Request Alpha Code<svg className="svg-icon">
											<use href="#long-arrow-alt-right" />
										</svg></span>
									</a> --> */}
								</div>
								{/* <!-- Request Alpha Code Modal --> */}
								<div id='alpha-code-modal' className='overlay light'>
									<a tabIndex={-1} className='cancel' href='#'></a>
									<div className='popup bg-dark'>
										<h3 className='light'>Get Your Alpha Code</h3>
										<a className='modal-close' href='#'>
											<svg className='svg-icon tooltip-icon'>
												<use href='#times' />
											</svg>
										</a>
										<p className='light'>Thank you for your interest in Mosaic.</p>
										<div className='modal-content'>
											<form action='https://fractalnetworks.co/api/mailing_list/subscribe/' method='post'>
												<div className='modal-grid'>
													<div className='flex-column'>
														<label htmlFor='name' className='modal-label'>
															<h5 className='light'>Name</h5>
														</label>
														<input
															required
															type='text'
															name='name'
															id='name'
															placeholder='Enter Your Name'
															className='alpha-code-input modal-input'
														/>
													</div>
													<div className='flex-column'>
														<label htmlFor='alphaEmail' className='modal-label'>
															<h5 className='light'>Email Address</h5>
														</label>
														<input
															required
															type='email'
															name='email'
															id='alphaEmail'
															placeholder='Email Address'
															className='alpha-code-input modal-input'
														/>
													</div>
													<div className='flex-column text-area'>
														<label htmlFor='alphaMessage' className='modal-label'>
															<h5 className='light'>Message</h5>
														</label>
														<textarea
															name='message'
															id='alphaMessage'
															placeholder='Message(optional)'
															cols={30}
															rows={10}
															className='alpha-code-input'></textarea>
													</div>
												</div>
												<input type='hidden' name='alpha_code' value='true' />
												<button type='submit' className='btn btn-dark'>
													<span className='btn-txt'>Get Your Code</span>
												</button>
											</form>
										</div>
									</div>
								</div>
							</div>
							<div className='hero-background'>
								<div className='hero-img-wrapper'>
									<picture className='hero-img'>
										<source
											type='image/avif'
											media='(max-width:576px)'
											srcSet='/img/Hero-vertical-2048.avif 2074w,
												/img/Hero-vertical-1024.avif 1036w,
												/img/Hero-vertical-512.avif 576w'
										/>
										<source
											type='image/webp'
											media='(max-width:576px)'
											srcSet='/img/Hero-vertical-2048.webp 2074w,
												/img/Hero-vertical-1024.webp 1036w,
												/img/Hero-vertical-512.webp 576w'
										/>
										<source
											media='(max-width:576px)'
											srcSet='/img/Hero-vertical-2048.jpg 2074w,
												/img/Hero-vertical-1024.jpg 1036w,
												/img/Hero-vertical-512.jpg 576w'
										/>
										<source
											type='image/avif'
											srcSet='/img/Hero-4096.avif 3800w,
												/img/Hero-2048.avif 1843w,								
												/img/Hero-1024.avif 922w,
												/img/Hero-512.avif 512w'
										/>
										<source
											type='image/webp'
											srcSet='/img/Hero-4096.webp 3800w,
												/img/Hero-2048.webp 1843w,
												/img/Hero-1024.webp 922w,
												/img/Hero-512.webp 512w'
										/>
										<img
											src='/img/Hero-1024.jpg'
											alt='red points floating together'
											srcSet='/img/Hero-4096.jpg 3800w,
												/img/Hero-2048.jpg 1843w,
												/img/Hero-1024.jpg 922w,
												/img/Hero-512.jpg 512w'
										/>
									</picture>
								</div>
							</div>
						</section>
						{/* <!-- Steps --> */}
						{/* <section className='steps-section bg-dark'> */}
						{/* <div className='bg-dark nav-bg'></div> */}
						{/* <div className='grid'> */}
						{/* <header className='steps-header'>
									<h1 className='light'>Stop renting. Become your own landlord.</h1>
								</header>
								<div className='steps-subhead'>
									<h3 className='light'>Own your server without the overhead.</h3>
									<p className='light'>
										Mosaic&apos;s decentralized and distributed network combines your devices creating a server solution
										that rivals the cloud.
									</p>
								</div>
								<h3 className='light red steps-zero'>It&apos;s Easy</h3>
								<div className='steps-one steps-flex'>
									<div className='steps-number-flex'>
										<h2 className='light red'>1</h2>
										<picture className='img xsm contain'>
											<source
												type='image/webp'
												srcSet='/img/sphere-red-16.webp 16w,
													/img/sphere-red-32.webp 32w,
													/img/sphere-red-64.webp 64w,
													/img/sphere-red-128.webp 128w,
													/img/sphere-red-256.webp 256w'
												sizes='calc(1.6vw + 1.6vh)'
											/>
											<img
												id='dotOne'
												style={{aspectRatio: '1/1'}}
												alt='a small red dot'
												src='/img/sphere-red-32.png'
												srcSet='/img/sphere-red-16.png 16w,
													/img/sphere-red-32.png 32w,
													/img/sphere-red-64.png 64w,
													/img/sphere-red-128.png 128w,
													/img/sphere-red-256.png 256w'
												sizes='calc(1.6vw + 1.6vh)'
											/>
										</picture>
									</div>
									<div className='sidecar'>
										<h4 className='light'>Choose a Domain</h4>
										<p className='light sm'>or bring your own.</p>
									</div>
								</div> */}
						<div className='steps-two steps-flex'>
							{/* <div className='steps-number-flex'>
										<h2 className='light grey'>2</h2>
										<picture className='img sm contain'>
											<source
												type='image/webp'
												srcSet='/img/sphere-black-16.webp 16w,
													/img/sphere-black-32.webp 32w,
													/img/sphere-black-64.webp 64w,
													/img/sphere-black-128.webp 128w,
													/img/sphere-black-256.webp 256w'
												sizes='calc(2.6vw + 2.6vh)'
											/>
											<img
												id='dotTwo'
												style={{aspectRatio: '1/1'}}
												alt='a medium size grey dot'
												src='/img/sphere-black-32.png'
												srcSet='/img/sphere-black-16.png 16w,
													/img/sphere-black-32.png 32w,
													/img/sphere-black-64.png 64w,
													/img/sphere-black-128.png 128w,
													/img/sphere-black-256.png 256w'
												sizes='calc(2.6vw + 2.6vh)'
											/>
										</picture>
									</div> */}
							<div className='sidecar see-all-apps-relative'>
								{/* <h4 className='light'>Pick Your Apps</h4>
										<p className='light sm'>or deploy your own.</p>
										<a className='btn btn-dark btn-secondary' href='#seeAllApps'>
											<span className='btn-txt'>
												See All Apps
												<svg className='svg-icon'>
													<use href='#long-arrow-alt-right' />
												</svg>
											</span>
										</a> */}
								{/* <!-- SEE ALL APPS MODAL  --> */}
								<div className='appsOverlay bg-light' id='seeAllApps'>
									<a className='modal-close' href='#'>
										<svg className='svg-icon tooltip-icon'>
											<use href='#times' />
										</svg>
									</a>
									<div className='sidebar-column-flex'>
										<h4 className='appsPopop-sticky'>Free App Store</h4>
										<div className='appsPopup '>
											<div className='app-column'>
												<div className='app-card'>
													<picture className='img sm contain'>
														<img style={{aspectRatio: '1/1'}} src='/img/logo-BitWarden.svg' alt='the BitWarden logo' />
													</picture>
													<div className='app-title'>
														<h5>BitWarden</h5>
														<p className='sm'>manage passwords</p>
													</div>
												</div>
												<div className='app-card'>
													<picture className='img sm contain'>
														<img style={{aspectRatio: '1/1'}} src='/img/logo-Elements.svg' alt='the Elements logo' />
													</picture>
													<div className='app-title'>
														<h5>Elements</h5>
														<p className='sm'>replace Slack</p>
													</div>
												</div>
												<div className='app-card'>
													<picture className='img sm contain'>
														<img style={{aspectRatio: '1/1'}} src='/img/logo-Wordpress.svg' alt='the Wordpress logo' />
													</picture>
													<div className='app-title'>
														<h5>Wordpress</h5>
														<p className='sm'>build + manage a website</p>
													</div>
												</div>
												<div className='app-card'>
													<picture className='img sm contain'>
														<img
															className='one-one'
															style={{aspectRatio: '1/1'}}
															alt='the Ghost logo'
															src='/img/logo-ghost-32.png'
															srcSet='/img/logo-ghost-16.png 16w,
																		/img/logo-ghost-32.png 32w,
																		/img/logo-ghost-64.png 64w,
																		/img/logo-ghost-128.png 128w,
																		/img/logo-ghost-256.png 256w'
															sizes='calc(2.6vw + 2.6vh)'
														/>
													</picture>
													<div className='app-title'>
														<h5>Ghost</h5>
														<p className='sm'>start a blog</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						{/* <div className='steps-three steps-flex'>
									<div className='steps-number-flex'>
										<h2 className='light grey'>3</h2>
										<picture className='img md contain'>
											<source
												type='image/webp'
												srcSet='/img/sphere-black-16.webp 16w,
													/img/sphere-black-32.webp 32w,
													/img/sphere-black-64.webp 64w,
													/img/sphere-black-128.webp 128w,
													/img/sphere-black-256.webp 256w'
												sizes='calc(4vw + 4vh)'
											/>
											<img
												id='dotThree'
												style={{aspectRatio: '1/1'}}
												alt='a medium size grey dot'
												src='/img/sphere-black-32.png'
												srcSet='/img/sphere-black-16.png 16w,
													/img/sphere-black-32.png 32w,
													/img/sphere-black-64.png 64w,
													/img/sphere-black-128.png 128w,
													/img/sphere-black-256.png 256w'
												sizes='calc(4vw + 4vh)'
											/>
										</picture>
									</div>
									<div className='sidecar launch-server'>
										<h4 className='light'>Launch Your Server</h4>
										<p className='light sm'>at home, or with us.</p>
									</div>
								</div> */}
						{/* </div> */}
						{/* <canvas id='stepsCanvas'></canvas> */}
						{/* </section> */}
						{/* <!--Friends--> */}
						<section className='friends-section bg-dark'>
							<div className='bg-dark nav-bg'></div>
							<div className='grid'>
								<header className='friends-header'>
									<h2 className='light'>Invite friends. Grow stronger together.</h2>
									<p className='light'>
										Improve <u>availability</u> and <u>fault tolerance</u> of your personal cloud by inviting friends.
									</p>
								</header>
								{/* <!-- GRID FOR HOW STRONG LAYOUT HERE --> */}
								<h3 className='light friends-subhead red'>How strong?</h3>
								{/* <!-- ONE PERSON DIV --> */}
								<div className='friends-one-header'>
									<h4 className='light'>One Person</h4>
									{/* <p className='light sm'>One gateway</p> */}
									<p className='light sm'>One device</p>
									<p className='light sm'>&nbsp;</p>
								</div>
								<div className='friends-one-detail'>
									<picture className='img xsm contain'>
										<source
											type='image/webp'
											srcSet='/img/sphere-white-16.webp 16w,
												/img/sphere-white-32.webp 32w,
												/img/sphere-white-64.webp 64w,
												/img/sphere-white-128.webp 128w'
											sizes='calc(1.6vw + 1.6vh)'
										/>
										<img
											className='one-one'
											style={{aspectRatio: '1/1'}}
											alt='a single point'
											src='/img/sphere-white-32.png'
											srcSet='/img/sphere-white-16.png 16w,
												/img/sphere-white-32.png 32w,
												/img/sphere-white-64.png 64w,
												/img/sphere-white-128.png 128w'
											sizes='calc(1.6vw + 1.6vh)'
										/>
									</picture>
									<div>
										<h5 className='light'>
											99.9<sup>%</sup>
										</h5>
										<p className='light sm'>
											availability&nbsp;
											<button className='tooltip' aria-label='more info'>
												<div className='bg-light'>
													<h6>&quot;3 Nines&quot; Availability:</h6>
													<p>Your server will experience roughly 8 hours of downtime per year.</p>
												</div>
												<svg className='svg-icon tooltip-icon'>
													<use href='#question-circle' />
												</svg>
											</button>
										</p>
									</div>
								</div>
								{/* <!-- TWO PEOPLE DIV --> */}
								<div className='friends-two-header'>
									<h4 className='light'>Two People</h4>
									{/* <p className='light sm'>Two gateways</p> */}
									<p className='light sm'>Three devices</p>
									<p className='light sm'>Two locations</p>
								</div>
								<div className='friends-two-detail'>
									<picture className='img sm contain'>
										<source
											type='image/webp'
											srcSet='/img/2-points-16.webp 16w,
												/img/2-points-32.webp 32w,
												/img/2-points-64.webp 64w,
												/img/2-points-128.webp 128w'
											sizes='calc(4vw + 4vh)'
										/>
										<img
											style={{aspectRatio: '2/1'}}
											alt='two points linked by a line'
											src='/img/2-points-32.png'
											srcSet='/img/2-points-16.png 16w,
												/img/2-points-32.png 32w,
												/img/2-points-64.png 64w,
												/img/2-points-128.png 128w'
											sizes='calc(4vw + 4vh)'
										/>
									</picture>
									<div>
										<h5 className='light'>
											33<sup>%</sup>
										</h5>
										<p className='light sm'>
											fault tolerance&nbsp;
											<button className='tooltip' aria-label='more info'>
												<div className='bg-light'>
													<h6>33% Fault Tolerance:</h6>
													<p>Your server will continue working, even if a third of devices are disconnected.</p>
												</div>
												<svg className='svg-icon tooltip-icon'>
													<use href='#question-circle' />
												</svg>
											</button>
										</p>
									</div>
									<div>
										<h5 className='light'>
											99.99<sup>%</sup>
										</h5>
										<p className='light sm'>
											availability&nbsp;
											<button className='tooltip' aria-label='more info'>
												<div className='bg-light'>
													<h6>&quot;4 Nines&quot; Availability:</h6>
													<p>Your server will experience roughly 50 minutes of downtime per year.</p>
												</div>
												<svg className='svg-icon tooltip-icon'>
													<use href='#question-circle' />
												</svg>
											</button>
										</p>
									</div>
								</div>
								{/* <!-- THREE PEOPLE DIV --> */}
								<div className='friends-three-header'>
									<h4 className='light'>Three People</h4>
									{/* <p className='light sm'>Two gateways</p> */}
									<p className='light sm'>Five devices</p>
									<p className='light sm'>Three locations</p>
								</div>
								<div className='friends-three-detail'>
									<picture className='img md contain'>
										<source
											type='image/webp'
											srcSet='/img/3-points-256.webp 256w,
												/img/3-points-128.webp 128w,
												/img/3-points-64.webp 64w,
												/img/3-points-32.webp 32w,
												/img/3-points-16.webp 16w'
											sizes='calc(4vw + 4vh)'
										/>
										<img
											style={{aspectRatio: '16/10'}}
											alt='three points linked by lines'
											src='/img/3-points-32.png'
											srcSet='/img/3-points-256.png 256w,
												/img/3-points-128.png 128w,
												/img/3-points-64.png 64w,
												/img/3-points-32.png 32w,
												/img/3-points-16.png 16w'
											sizes='calc(4vw + 4vh)'
										/>
									</picture>
									<div>
										<h5 className='light'>
											100<sup>%</sup>
										</h5>
										<p className='light sm'>cloud parity</p>
									</div>
									<div>
										<h5 className='light'>
											50<sup>%</sup>
										</h5>
										<p className='light sm'>
											fault tolerance&nbsp;
											<button className='tooltip' aria-label='more info'>
												<div className='bg-light tool-tip-right'>
													<h6>50% Fault Tolerance:</h6>
													<p>Your server will continue working, even if half of devices are disconnected.</p>
												</div>
												<svg className='svg-icon tooltip-icon'>
													<use href='#question-circle' />
												</svg>
											</button>
										</p>
									</div>
									<div>
										<h5 className='light'>
											99.999<sup>%</sup>
										</h5>
										<p className='light sm'>
											availability&nbsp;
											<button className='tooltip' aria-label='more info'>
												<div className='bg-light tool-tip-right'>
													<h6>&quot;5 Nines&quot; Availability:</h6>
													<p>Your server will experience roughly 5 minutes of downtime per year.</p>
												</div>
												<svg className='svg-icon tooltip-icon'>
													<use href='#question-circle' />
												</svg>
											</button>
										</p>
									</div>
								</div>
							</div>
						</section>
						{/* <!--Simple--> */}
						<section className='simple-section bg-dark'>
							<div className='bg-dark nav-bg'></div>
							<div className='grid'>
								<header className='simple-header'>
									<h2 className='light'>Servers as simple as smartphones.</h2>
									<p className='light'>Install cloud services to your personal cloud with a single click.</p>
								</header>
								{/* <div className='simple-subheader'>
									<h3 className='light red'>
										Server apps <s>store</s>.
									</h3>
								</div> */}
								<h4 className='light top-apps'>Top Apps</h4>
								<div className='app-column'>
									<div className='app-card'>
										<picture className='img sm contain'>
											<img style={{aspectRatio: '1/1'}} src='/img/logo-BitWarden.svg' alt='the BitWarden logo' />
										</picture>
										<div className='app-title'>
											<h5 className='light'>BitWarden</h5>
											<p className='light sm'>Password Manager</p>
										</div>
									</div>
									<div className='app-card'>
										<picture className='img sm contain'>
											<img style={{aspectRatio: '1/1'}} src='/img/logo-Elements.svg' alt='the NextCloud logo' />
										</picture>
										<div className='app-title'>
											<h5 className='light'>NextCloud</h5>
											<p className='light sm'>Filesharing Drive</p>
										</div>
									</div>
								</div>
								<div className='app-column'>
									<div className='app-card'>
										<picture className='img sm contain'>
											<img style={{aspectRatio: '1/1'}} src='/img/logo-Wordpress.svg' alt='the Wiki Js logo' />
										</picture>
										<div className='app-title'>
											<h5 className='light'>WikiJs</h5>
											<p className='light sm'>Powerful Wiki</p>
										</div>
									</div>
									{/* <div className='app-card'>
										<picture className='img sm contain'>
											<img
												className='one-one'
												style={{aspectRatio: '1/1'}}
												alt='the Ghost logo'
												src='/img/logo-ghost-32.png'
												srcSet='/img/logo-ghost-16.png 16w,
													/img/logo-ghost-32.png 32w,
													/img/logo-ghost-64.png 64w,
													/img/logo-ghost-128.png 128w,
													/img/logo-ghost-256.png 256w'
												sizes='calc(2.6vw + 2.6vh)'
											/>
										</picture>
										<div className='app-title'>
											<h5 className='light'>Ghost</h5>
											<p className='light sm'>start a blog</p>
										</div>
									</div> */}
								</div>
								<div className='button-column'>
									<a className='btn btn-dark btn-secondary' href='#seeAllApps'>
										<span className='btn-txt'>
											See All Apps
											<svg className='svg-icon'>
												<use href='#long-arrow-alt-right' />
											</svg>
										</span>
									</a>
									<a href='#add-own-app-modal' className='btn btn-dark btn-secondary'>
										<span className='btn-txt'>
											Add Your Own App
											<svg className='svg-icon'>
												<use href='#long-arrow-alt-right' />
											</svg>
										</span>
									</a>
								</div>
								{/* <div className='auto-updates'>
									<h4 className='light'>Auto Updates</h4>
									<p className='light'>Zero Config. No Maintenance. No DevOps.</p>
								</div> */}
								{/* <div className='custom-apps'>
									<h4 className='light'>Custom Apps</h4>
									<div className='app-card custom-apps-example'>
										<picture className='img md contain'>
											<source
												type='image/webp'
												srcSet='/img/torus-red-256.webp 256w,
													/img/torus-red-128.webp 128w,
													/img/torus-red-64.webp 64w,
													/img/torus-red-32.webp 32w,
													/img/torus-red-16.webp 16w'
												sizes='calc(4vw + 4vh)'
											/>
											<img
												className='one-one'
												style={{aspectRatio: '1/1'}}
												alt='a complex torus geometry'
												src='/img/torus-red-32.png'
												srcSet='/img/torus-red-256.png 256w,
													/img/torus-red-128.png 128w,
													/img/torus-red-64.png 64w,
													/img/torus-red-32.png 32w,
													/img/torus-red-16.png 16w'
												sizes='calc(4vw + 4vh)'
											/>
										</picture>
										<div className='app-title'>
											<h5 className='light'>Custom Apps</h5>
											<p className='light sm'>bring a Docker compose</p>
										</div>
									</div>
									<p className='light'>Deploy any app to your personal network with Docker compose.</p>
								</div> */}
								{/* <div className='ci'>
									<h4 className='light'>Push to Deploy</h4>
								</div>
								<figure className='auto-update-img'>
									<figcaption className='light'>1.Link To Github</figcaption>
									<picture className='img lg contain'>
										<source
											type='image/webp'
											srcSet='/img/figure-link-256.webp 256w,
												/img/figure-link-128.webp 128w,
												/img/figure-link-64.webp 64w,
												/img/figure-link-32.webp 32w,
												/img/figure-link-16.webp 16w'
											sizes='calc(6.4vw + 6.4vh)'
										/>
										<img
											style={{aspectRatio: '16/10'}}
											alt='three connected points, one point is the Github logo'
											src='/img/figure-link-64.png'
											srcSet='/img/figure-link-256.png 256w,
												/img/figure-link-128.png 128w,
												/img/figure-link-64.png 64w,
												/img/figure-link-32.png 32w,
												/img/figure-link-16.png 16w'
											sizes='calc(6.4vw + 6.4vh)'
										/>
									</picture>
								</figure>
								<figure className='auto-update-img'>
									<figcaption className='light'>2.Choose Settings</figcaption>
									<picture className='img lg contain'>
										<source
											type='image/webp'
											srcSet='/img/figure-option-256.webp 256w,
											/img/figure-option-128.webp 128w,
											/img/figure-option-64.webp 64w,
											/img/figure-option-32.webp 32w,
											/img/figure-option-16.webp 16w'
											sizes='calc(6.4vw + 6.4vh)'
										/>
										<img
											style={{aspectRatio: '16/10'}}
											alt='abstract dots representing buttons, one dot is red indicating it has been selected'
											src='/img/figure-option-64.png'
											srcSet='/img/figure-option-256.png 256w,
											/img/figure-option-128.png 128w,
											/img/figure-option-64.png 64w,
											/img/figure-option-32.png 32w,
											/img/figure-option-16.png 16w'
											sizes='calc(6.4vw + 6.4vh)'
										/>
									</picture>
								</figure>
								<figure className='auto-update-img'>
									<figcaption className='light'>3.Push To Deploy</figcaption>
									<picture className='img lg contain'>
										<source
											type='image/webp'
											srcSet='/img/figure-push-256.webp 256w,
												/img/figure-push-128.webp 128w,
												/img/figure-push-64.webp 64w,
												/img/figure-push-32.webp 32w,
												/img/figure-push-16.webp 16w'
											sizes='calc(6.4vw + 6.4vh)'
										/>
										<img
											style={{aspectRatio: '16/10'}}
											alt='transluscent dots become progressively more opaque indicating movement from left to right'
											src='/img/figure-push-64.png'
											srcSet='/img/figure-push-256.png 256w,
												/img/figure-push-128.png 128w,
												/img/figure-push-64.png 64w,
												/img/figure-push-32.png 32w,
												/img/figure-push-16.png 16w'
											sizes='calc(6.4vw + 6.4vh)'
										/>
									</picture>
								</figure> */}
							</div>
							{/* <!-- Add Your Own App Modal --> */}
							<div id='add-own-app-modal' className='overlay light'>
								<a tabIndex={-1} className='cancel' href='#'></a>
								<div className='popup bg-dark'>
									<h3 className='light'>Add Your App</h3>
									<a className='modal-close' href='#'>
										<svg className='svg-icon tooltip-icon'>
											<use href='#times' />
										</svg>
									</a>
									<p className='light'>Submit your app to the appstore.</p>
									<div className='modal-content'>
										<form action='https://fractalnetworks.co/api/app/submit/' method='post'>
											<div className='modal-grid'>
												<div className='flex-column'>
													<label htmlFor='contact_email' className='modal-label'>
														<h5 className='light'>Contact Email</h5>
													</label>
													<input
														required
														type='text'
														name='contact_email'
														id='contact_email'
														placeholder='Contact Email'
														className='alpha-code-input modal-input'
													/>
												</div>
												<div className='flex-column'>
													<label htmlFor='github' className='modal-label'>
														<h5 className='light'>Github Url</h5>
													</label>
													<input
														required
														type='text'
														name='github'
														id='github'
														placeholder='Github url'
														className='alpha-code-input modal-input'
													/>
												</div>
											</div>
											<button type='submit' className='btn btn-dark'>
												<span className='btn-txt'>Submit App</span>
											</button>
										</form>
									</div>
								</div>
							</div>
						</section>
						{/* <!--Data--> */}
						<section className='data-wrapper bg-light'>
							<div className='bg-light nav-bg'></div>
							<div className='grid'>
								<header className='data-header'>
									<h1 className='red'>Take your data back.</h1>
								</header>
								<div className='data-subheader'>
									<h3>Revolutionary hosting from homes and offices.</h3>
									<p>Mosaic&apos;s network architecture makes it safe to host websites from outside a datacenter.</p>
									<a
										className='btn btn-light btn-secondary'
										href='https://docs.fractalnetworks.co/'
										target='_blank'
										rel='noreferrer'>
										<span className='btn-txt'>
											Learn How it Works
											<svg className='svg-icon'>
												<use href='#long-arrow-alt-right' />
											</svg>
										</span>
									</a>
								</div>
								<div className='display-image'>
									<div className='display-image-wrapper'>
										<picture className='img contain'>
											<source
												type='image/webp'
												media='(max-width:576px)'
												srcSet='/img/internet-mobile-2048.webp 2074w,
													/img/internet-mobile-1024.webp 1036w,
													/img/internet-mobile-512.webp 576w'
											/>
											<source
												media='(max-width:576px)'
												srcSet='/img/internet-mobile-2048.jpg 2074w,
													/img/internet-mobile-1024.jpg 1036w,
													/img/internet-mobile-512.jpg 576w'
											/>
											<source
												type='image/webp'
												srcSet='/img/Internet.webp 1024w,
													/img/Internet-512.webp 512w,
													/img/Internet-256.webp 256w'
												sizes='calc(25vw + 25vh)'
											/>
											<img
												id='internet'
												style={{aspectRatio: '1/1'}}
												src='/img/Internet-512.jpg'
												alt='many spheres depicted in a circular frame'
												srcSet='/img/Internet.jpg 1024w,
													/img/Internet-512.jpg 512w,
													/img/Internet-256.jpg 256w'
												sizes='calc(25vw + 25vh)'
											/>
										</picture>
									</div>
								</div>
								<div className='noteworthy-image'>
									<div className='noteworthy-image-wrapper'>
										<picture className='img contain'>
											<source
												type='image/webp'
												srcSet='/img/connection-2048.webp 2048w,
													/img/connection-1024.webp 1024w,
													/img/connection-512.webp 512w,
													/img/connection-256.webp 256w'
												sizes='calc(15vw + 15vh)'
											/>
											<img
												id='gateway'
												style={{aspectRatio: '1/1'}}
												alt='a donut shaped hub connected to points by encrypted connection'
												src='/img/connection-256.png'
												srcSet='/img/connection-2048.png 2048w,
													/img/connection-1024.png 1024w,
													/img/connection-512.png 512w,
													/img/connection-256.png 256w'
												sizes='calc(15vw + 15vh)'
											/>
										</picture>
									</div>
								</div>

								<div className='data-container encrypted-storage'>
									<picture className='img xlg contain'>
										<source
											type='image/webp'
											srcSet='/img/figure-security-256.webp 256w,
												/img/figure-security-128.webp 128w,
												/img/figure-security-64.webp 64w,
												/img/figure-security-32.webp 32w,
												/img/figure-security-16.webp 16w'
											sizes='calc(10.8vw + 10.8vh)'
										/>
										<img
											className='one-one'
											style={{aspectRatio: '1/1'}}
											alt='a padlock'
											src='/img/figure-security-64.png'
											srcSet='/img/figure-security-256.png 256w,
												/img/figure-security-128.png 128w,
												/img/figure-security-64.png 64w,
												/img/figure-security-32.png 32w,
												/img/figure-security-16.png 16w'
											sizes='calc(10.8vw + 10.8vh)'
										/>
									</picture>
									<h4>Encrypted Storage</h4>
									<p>
										Data is encrypted while on your organization&apos;s machine, keeping it safe from attacks or
										snooping.
									</p>
								</div>
								<div className='data-container encrypted-communication'>
									<picture className='img xlg contain'>
										<source
											type='image/webp'
											srcSet='/img/figure-transfer-256.webp 256w,
												/img/figure-transfer-128.webp 128w,
												/img/figure-transfer-64.webp 64w,
												/img/figure-transfer-32.webp 32w,
												/img/figure-transfer-16.webp 16w'
											sizes='calc(10.8vw + 10.8vh)'
										/>
										<img
											className='one-one'
											style={{aspectRatio: '1/1'}}
											alt='an encrypted line connecting two points'
											src='/img/figure-transfer-64.png'
											srcSet='/img/figure-transfer-256.png 256w,
												/img/figure-transfer-128.png 128w,
												/img/figure-transfer-64.png 64w,
												/img/figure-transfer-32.png 32w,
												/img/figure-transfer-16.png 16w'
											sizes='calc(10.8vw + 10.8vh)'
										/>
									</picture>
									<h4>Encrypted End-to-End Communication</h4>
									<p>Packets are encrypted in transit, so no one can see what you&apos;re sending.</p>
								</div>
								{/* <div className='data-container transparent'>
									<picture className='img xlg contain'>
										<source
											type='image/webp'
											srcSet='/img/figure-transparent-256.webp 256w,
												/img/figure-transparent-128.webp 128w,
												/img/figure-transparent-64.webp 64w,
												/img/figure-transparent-32.webp 32w,
												/img/figure-transparent-16.webp 16w'
											sizes='calc(10.8vw + 10.8vh)'
										/>
										<img
											className='one-one'
											style={{aspectRatio: '1/1'}}
											alt='nine overlapping circles, each partly transparent'
											src='/img/figure-transparent-64.png'
											srcSet='/img/figure-transparent-256.png 256w,
												/img/figure-transparent-128.png 128w,
												/img/figure-transparent-64.png 64w,
												/img/figure-transparent-32.png 32w,
												/img/figure-transparent-16.png 16w'
											sizes='calc(10.8vw + 10.8vh)'
										/>
									</picture>
									<h4>Anti-Censorship, Pro-Transparency.</h4>
									<p>
										Fractal Networks is committed to transparency. Mosaic&apos;s source code is <span>open-source</span>{' '}
										and available on github.
									</p>

									<a
										className='btn btn-light btn-secondary'
										href='https://github.com/OpenENS'
										target='_blank'
										rel='noreferrer'>
										<span className='btn-txt'>
											View on Github
											<svg className='svg-icon'>
												<use href='#long-arrow-alt-right' />
											</svg>
										</span>
									</a>
								</div> */}
								{/* <div className='control-your'>
									<picture className='img md contain'>
										<source
											type='image/webp'
											srcSet='/img/3-Dots-256.webp 256w,
												/img/3-Dots-128.webp 128w,
												/img/3-Dots-64.webp 64w,
												/img/3-Dots-32.webp 32w,
												/img/3-Dots-16.webp 16w'
											sizes='calc(6.4vw + 6.4vh)'
										/>
										<img
											alt='three points, one is red'
											style={{aspectRatio: '128/56'}}
											src='/img/3-Dots-32.png'
											srcSet='/img/3-Dots-256.png 256w,
												/img/3-Dots-128.png 128w,
												/img/3-Dots-64.png 64w,
												/img/3-Dots-32.png 32w,
												/img/3-Dots-16.png 16w'
											sizes='calc(6.4vw + 6.4vh)'
										/>
									</picture>
									<h3>Control what you store.</h3>
									<p>You decide what data your machine holds.</p>
								</div>
								<div className='decide-who'>
									<picture className='img xlg contain'>
										<source
											type='image/webp'
											srcSet='/img/sphere-red-256.webp 256w,
												/img/sphere-red-128.webp 128w,
												/img/sphere-red-64.webp 64w,
												/img/sphere-red-32.webp 32w,
												/img/sphere-red-16.webp 16w'
											sizes='calc(10.8vw + 10.8vh)'
										/>
										<img
											id='smallDot'
											style={{aspectRatio: '1/1'}}
											alt='a red dot'
											src='/img/sphere-red-32.png'
											srcSet='/img/sphere-red-256.png 256w,
												/img/sphere-red-128.png 128w,
												/img/sphere-red-64.png 64w,
												/img/sphere-red-32.png 32w,
												/img/sphere-red-16.png 16w'
											sizes='calc(10.8vw + 10.8vh)'
										/>
									</picture>
									<h3>Decide who you connect to.</h3>
									<p>Mosaic gives you fine control over who, in your network, can store your data.</p>
								</div>
								<div className='control-image'>
									<div className='control-image-wrapper'>
										<picture className='img contain control'>
											<source
												type='image/webp'
												srcSet='/img/sphere-white-1024.webp 1024w,
													/img/sphere-white-512.webp 512w,
													/img/sphere-white-256.webp 256w'
												sizes='(max-width: 576px) calc(50vw + 50vh), calc(17vw + 17vh)'
											/>
											<img
												id='bigDot'
												style={{aspectRatio: '1/1'}}
												alt='large white sphere'
												src='/img/sphere-white-256.png'
												srcSet='/img/sphere-white-1024.png 1024w,
													/img/sphere-white-512.png 512w,
													/img/sphere-white-256.png 256w'
												sizes='(max-width: 576px) calc(50vw + 50vh), calc(17vw + 17vh)'
											/>
										</picture>
									</div>
								</div> */}
								{/* <canvas id='dataCanvas'></canvas> */}
								<canvas id='internetGateway'></canvas>
							</div>
						</section>
						{/* <!--Free--> */}
						<section className='free-section bg-light'>
							{/* <div className='free-bg-circle bg-transparent' id='alphaCode'>
								<div className='bg-dark nav-bg'></div>
							</div> */}
							<div className='free-bg-circle bg-dark'></div>
							{/* <!-- <div className="free-bg-container"></div> --> */}
							<div className='grid'>
								<header className='free-header'>
									<h1 className='light'>It&apos;s always free for non-commercial use.</h1>
								</header>
								{/* <!-- <div className="free-main">
									<h3 className="light">Join the closed alpha.</h3>
									<p className="light">
										Fractal Networks will host the secure gateway for testing
										purposes.
									</p>
									<form className="input-button-group" action="https://fractalnetworks.co/api/start/">
										<input required className="alpha-code-input" type="text" placeholder="Enter Alpha Code" />
										<button className="btn btn-dark" type="submit">
											<span className="btn-txt">Get Started</span>
										</button>
									</form>
									<div className="alpha-details">
										<a href="#alpha-code-modal" className="btn btn-dark btn-secondary">
											<span className="btn-txt">Request an alpha code<svg className="svg-icon">
												<use href="#long-arrow-alt-right" />
											</svg>
											</span>
										</a>
										<ul className="alpha-details-list sm light">
											<li className="light sm">Limited Transfer: 1 GB / month</li>
											<li className="light sm">Analytics: automatically enabled</li>
											<li className="light sm">Supported OS: Linux only</li>
										</ul>
									</div>
								</div> --> */}
								<div className='free-sign-up'>
									<h4 className='light'>Sign Up for the Open-Beta</h4>
									<p className='light'>Get notified when the unrestricted open-beta becomes available.</p>
									<form
										className='input-button-group'
										method='post'
										action='https://fractalnetworks.co/api/mailing_list/subscribe/'>
										<input
											required
											className='alpha-code-input'
											type='email'
											name='email'
											placeholder='Your Email'
											aria-label="Recipient's email"
										/>
										<button type='submit' className='btn btn-dark'>
											<span className='btn-txt'>Get Notified</span>
										</button>
									</form>
								</div>
								<div className='free-enterprise'>
									<h4 className='light'>Need Enterprise Pricing?</h4>
									<p className='light'>
										Dedicated support, guaranteed uptime, periodic backups, and any customization you need.
									</p>
									<a className='btn btn-dark' href='#enterprise-modal'>
										<span className='btn-txt'>Let&apos;s Connect</span>
									</a>
								</div>
							</div>
							{/* <!-- Enterprise Modal --> */}
							<div id='enterprise-modal' className='overlay light'>
								<a tabIndex={-1} className='cancel' href='#'></a>
								<div className='popup bg-dark'>
									<h3 className='light'>Enterprise Solutions</h3>
									<a className='modal-close' href='#'>
										<svg className='svg-icon tooltip-icon'>
											<use href='#times' />
										</svg>
									</a>
									<p className='light'>
										Dedicated support, guaranteed uptime, periodic backups, and any customization you need.
									</p>
									<div className='modal-content'>
										<form action='https://fractalnetworks.co/api/enterprise/interest/' method='post'>
											<div className='modal-grid'>
												<div className='flex-column'>
													<label htmlFor='first_name' className='modal-label'>
														<h5 className='light'>First Name</h5>
													</label>
													<input
														required
														type='text'
														name='first_name'
														id='first_name'
														placeholder='First Name'
														className='alpha-code-input modal-input'
													/>
												</div>
												<div className='flex-column'>
													<label htmlFor='last_name' className='modal-label'>
														<h5 className='light'>Last Name</h5>
													</label>
													<input
														required
														type='text'
														name='last_name'
														id='last_name'
														placeholder='Last Name'
														className='alpha-code-input modal-input'
													/>
												</div>
												<div className='flex-column'>
													<label htmlFor='company_name' className='modal-label'>
														<h5 className='light'>Company</h5>
													</label>
													<input
														required
														type='text'
														name='company_name'
														id='company_name'
														placeholder='Company'
														className='alpha-code-input modal-input'
													/>
												</div>
												<div className='flex-column'>
													<label htmlFor='email' className='modal-label'>
														<h5 className='light'>Email Address</h5>
													</label>
													<input
														required
														type='email'
														name='email'
														id='email'
														placeholder='Email Address'
														className='alpha-code-input modal-input'
													/>
												</div>
												<div className='flex-column text-area'>
													<label htmlFor='message' className='modal-label'>
														<h5 className='light'>Message</h5>
													</label>
													<textarea
														name='message'
														id='message'
														placeholder='Message(optional)'
														cols={30}
														rows={10}
														className='alpha-code-input'></textarea>
												</div>
											</div>
											<button type='submit' className='btn btn-dark'>
												<span className='btn-txt'>Send Message</span>
											</button>
										</form>
									</div>
								</div>
							</div>
							{/* <!-- Success Modal --> */}
							<div id='success' className='overlay light'>
								<a tabIndex={-1} className='cancel' href='#'></a>
								<div className='popup bg-dark'>
									<h3 className='light'>Thank you for your interest.</h3>
									<a className='modal-close' href='#'>
										<svg className='svg-icon tooltip-icon'>
											<use href='#times' />
										</svg>
									</a>
								</div>
							</div>
						</section>
					</main>
				</div>
				<MonoMobileNav currentDomain='Home' />
			</div>
		</>
	)
}

export default Home
