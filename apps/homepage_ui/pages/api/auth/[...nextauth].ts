import NextAuth from 'next-auth'
import {nextAuthConfig} from 'auth/auth-handler'

const handler = NextAuth(
	nextAuthConfig(
		process.env.NEXTAUTH_URL,
		process.env.REDIS_PORT as unknown as number,
		process.env.REDIS_HOST as string,
		process.env.REDIS_PASSWORD as string,
		process.env.KEYCLOAK_ID as string,
		process.env.KEYCLOAK_SECRET as string,
		process.env.KEYCLOAK_ISSUER as string,
		process.env.TOP_LEVEL_DOMAIN as string
	)
)

export default handler
