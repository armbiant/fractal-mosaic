from src import utils
from unittest.mock import patch, MagicMock
from cryptography.hazmat.primitives.asymmetric.ed25519 import Ed25519PrivateKey
import os
import docker
import platform
import aiofiles
import base64
import pytest


STORAGE_PLUGIN = os.environ.get("STORAGE_PLUGIN")

@pytest.mark.skip(reason="refactor to not make requests")
async def test_register_with_backend(mock_hive_request):
    '''
    Ensures that the device correctly makes an HTTP request to
    the Hive Backend to register itself. Expect to receive a
    Device Token back in the response.
    '''
    api_token = 'abcdefhijklmnopqrstuvwxyz'
    device_token = utils.register_with_backend(api_token)
    assert device_token

def test_get_device_name():
    '''
    Ensures that device name is set based on the value of the
    DEVICE_NAME environment variable.
    '''
    import socket

    os.environ['DEVICE_NAME'] = 'test_device'
    name = utils.get_device_name()
    assert name == 'test_device'

    os.environ.pop('DEVICE_NAME')
    hostname = socket.gethostname()
    name = utils.get_device_name()
    assert name == hostname

@pytest.mark.skip(reason="mock docker interactions")
async def test_install_plugin():
    '''
    Ensure that the correct storage plugin is installed based on the
    architecture of the currently running device.
    '''
    client = docker.from_env()
    utils.ensure_storage_plugin_ready(client)
    architecture = platform.machine()
    if architecture == "x86_64":
        STORAGE_PLUGIN = "fractalnetworks/storage:main"
    elif architecture in {'arm64', 'aarch64'}:
        STORAGE_PLUGIN = "fractalnetworks/storage:arm64"
    else:
        STORAGE_PLUGIN = "fractalnetworks/storage:main"
    plugin = client.plugins.get(STORAGE_PLUGIN)
    assert plugin.enabled == True

async def test_generate_env(generate_payload):
    '''
    Ensures that a call to aiofiles.write is made when generate_env is called
    '''
    # https://github.com/Tinche/aiofiles#writing-tests-for-aiofiles
    aiofiles.threadpool.wrap.register(MagicMock)(
        lambda *args, **kwargs: aiofiles.threadpool.AsyncBufferedIOBase(*args, **kwargs))

    env = generate_payload('SchedulerAction.start')
    mock_file = MagicMock()
    with patch('aiofiles.threadpool.sync_open', return_value=mock_file) as mock_open:
        await utils.generate_env(env)
        mock_file.write.assert_called()

async def test_get_app_path_from_env():
    '''
    Ensures that an app path is returned from get_app_path_from_env
    '''
    file_contents = ['HIVE_APP_NAME=bitwarden', 'HIVE_APP_VERSION=latest']
    expected_path = '/apps/bitwarden_latest.yaml'
    with patch('src.utils._read_file', return_value=file_contents) as mock_read:
        path = await utils.get_app_path_from_env('/test/')
        assert path == expected_path

def test_generate_storage_keys():
    '''
    Ensure that generate_storage_keys returns a dictionary of
    base64 encoded Ed25519PrivateKeys.
    '''
    storage_apikeys = utils.generate_storage_keys('test1', 'test2')
    assert type(storage_apikeys) == dict
    assert len(storage_apikeys) == 2

    for value in storage_apikeys.values():
        private_bytes = base64.b64decode(value)
        priv_key = Ed25519PrivateKey.from_private_bytes(private_bytes)
        assert priv_key
