from unittest.mock import patch
from src.crypto import PassphraseEncryptionXSalsa20
import pytest
import os


@pytest.fixture(scope='session')
def test_device_token():
    return "636ece46826db6deb29bd544602613ef0bc16b9a"

@pytest.fixture(scope='session')
def device_secret():
    return os.environ.get("DEVICE_SECRET")

@pytest.fixture(scope='session')
def test_key(device_secret):
    key = PassphraseEncryptionXSalsa20(device_secret)
    return key

@pytest.fixture(scope='session')
def test_storage_keys():
    keys = {
        'storage_apikeys': {
            'app': 'eTMu3If9r4HeTSVDv+MGLbKfZni9hmzpBKwFJ0EbCYk=',
            'link': '1Ni3vfZjVS+VBNp0KysCbB3RVP+FBtWn8XO12X4HxFk='
        }
    }
    return keys

@pytest.fixture
def mocked_run_command():
    def patched_func(retval):
        with patch('src.compose._run_command', return_value=retval) as patched:
            print(f'patched: {patched}')
            return patched
    return patched_func

@pytest.fixture
def mock_hive_request(requests_mock, test_device_token):
    FRACTAL_HIVE_API = os.environ.get("FRACTAL_HIVE_API")
    requests_mock.post(
        f'{FRACTAL_HIVE_API}/api/v1/device/',
        json={"token": test_device_token},
        status_code=200)

@pytest.fixture
def generate_payload():

    def payload(command, encrypted=False, **kwargs):
        msg = {
            'account': '78877f78-f15d-46af-be55-0e8f8b0196b0',
            'application': {
                'appstore': "Fractal App Store",
                'name': 'bitwarden',
                'version': 'latest'
            },
            'command': command,
            'device': 'f9b236df-c344-4d69-817e-bfa862c3fa79',
            'instance': '11cfd78a-d541-4077-a786-4aea6285f8d0',
            'links': {
                'default': {
                    'domain': 'sway-crusher.fractalnetworks.co',
                    'token': '3336a31e-dfa0-4681-ae4e-5f8c466f7024'
                }
            },
            'request': 'a45e3f41-cf6d-4903-9680-ba32a9bcceb5',
            'service': 'hive-device',
            'storage_apikeys': {
                'app': 'eTMu3If9r4HeTSVDv+MGLbKfZni9hmzpBKwFJ0EbCYk=',
                'link': '1Ni3vfZjVS+VBNp0KysCbB3RVP+FBtWn8XO12X4HxFk='
            },
            'encrypted_storage_apikeys': None,
            'type': 'device-request',
            **kwargs
        }

        if encrypted:
            msg['encrypted_storage_apikeys'] = 'eawe0ennf+6h/xA3S0LC5+c9UkelhAMMtIF9AK1J3pJc+SVx4tFC+/WP73NinvWO6kCsV77iftMNwCK0UhfRoY5kAGQNXJu15fc/RY2aIRCB8qj908aGN9HLOKOrdUphD4Cxo5EjzvWn1L9zV+3COZr0YyGjckbnDvkZznpxqud4/amE5hbpHxwmEW0VkP4ejhU4qc1/Wg=='

        return msg

    return payload

@pytest.fixture
def run_app_message_decrypted(generate_payload):
    msg = generate_payload('SchedulerAction.start')
    return msg

@pytest.fixture
def run_app_message_encrypted(generate_payload):
    msg = generate_payload('SchedulerAction.start', encrypted=True)
    return msg
