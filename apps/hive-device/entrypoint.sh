#!/bin/bash

# only create python venv if ENV is set
if [[ "$ENV" ]]; then
    [ -d /code/DevEnv ] || python3 -m venv --copies /code/DevEnv
    . /code/DevEnv/bin/activate
    pip3 install -r /code/requirements.txt
fi

python3 src/main.py
