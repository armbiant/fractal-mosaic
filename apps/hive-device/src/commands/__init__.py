import logging

from src.compose import run_device_command

class Command():

    def __init__(self):
        self.command = ''
        self.response = {
            "type": "device-state"
        }

    async def handle_result(self, result) -> dict:
        raise NotImplementedError()

    async def run(self):
        '''
        Runs command in subprocess
        '''
        return_code, (stdout, stderr) = await run_device_command(self.command)
        if return_code != 0:
            logging.error(f"Error {return_code} running command: {self.command}\n{stderr}")
            return

        return await self.handle_result(stdout)
