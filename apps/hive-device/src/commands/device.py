import logging
from enum import Enum

from src.commands import Command


class DeviceCommand(Command):

    def __init__(self):
        self.response = {
            "type": "device-state"
        }

class CheckDiskSpace(DeviceCommand):
    '''
    Check Device Disk Space. If disk space is running low, then a message is
    sent to the Hive Backend.
    '''

    def __init__(self):
        super().__init__()
        self.command = "df -h / | grep overlay | awk '{print $5}'"

    async def handle_result(self, raw_percentage: str) -> dict:
        '''
        Checks if the percent of disk used is greater than DISK_USAGE_THRESHOLD.
        '''
        DISK_USAGE_THRESHOLD = 90

        # attempt to convert percentage from comamnd into a Python int
        try:
            percent_disk_used = int(raw_percentage.rstrip().strip('%'))
        except ValueError:
            logging.error(f"Error while trying to get percentage from string: {raw_percentage}")
            return

        # only send message to backend if percent used is higher than threshold
        if percent_disk_used > DISK_USAGE_THRESHOLD:
            logging.warning(f"Disk space is running low: {percent_disk_used}% used")
            self.response["payload"] = {
                "event": "low_disk_space"
            }
            return self.response

class DeviceCommands(Enum):
    '''
    All supported Device Commands are defined here.
    '''
    check_disk_space = CheckDiskSpace()
