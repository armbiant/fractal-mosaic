import asyncio
import logging
import os
import sys

import docker
import websockets
from fractal.controllers.auth.util import login

from src import tasks, utils


async def task_manager(websocket, client):
    '''
    Handles running all async tasks.

    Params:
    - client: Docker client to use
    - websocket: Websocket connection to use
    '''
    logging.debug("Starting task_manager")

    run_loop_task = asyncio.create_task(
        tasks.run_loop(client, websocket))
    send_instances_task = asyncio.create_task(
        tasks.send_instances(client, websocket))
    send_version_task = asyncio.create_task(
        tasks.send_version(websocket))

    await utils.spawn_background_thread(tasks.publish_volume_statuses, websocket)

    while True:
        done, pending = await asyncio.wait(
            [run_loop_task, send_instances_task, send_version_task],
            return_when=asyncio.FIRST_COMPLETED)

        # display error message and exit for any finished task that has returned an error
        for task in done:
            try:
                await task
                logging.debug(f'task {task} finished')
            except Exception as e:
                logging.error(f'Task {task} raised an exception: {e}')
                sys.exit(1)

        for task in pending:
            logging.debug(f"Cancelling task ({task}) this round")
            task.cancel()

async def run():
    '''
    Connects to the Hive Backend via websocket. Upon initial connection,
    registers itself with Hive backend, sends out all currently running services,
    then launches the task manager.
    '''
    # if running in debug mode, use DEVICE_TOKEN from environment variables
    if DEBUG:
        DEVICE_TOKEN = os.environ.get("DEVICE_TOKEN")
        if not DEVICE_TOKEN:
            logging.error("DEVICE_TOKEN not provided")
            sys.exit(1)

    # not running in debug mode, register this device with the backend
    else:
        DEVICE_TOKEN = utils.register_with_backend(API_TOKEN)
        os.environ['DEVICE_TOKEN'] = DEVICE_TOKEN

    # login with CLI using api token
    login(api_token=API_TOKEN)

    # instantiate docker client & ensure storage plugin ready
    try:
        client = docker.from_env()
    except docker.errors.DockerException as e:
        logging.error(f"Docker Error: {e}")
        sys.exit(1)

    utils.create_fractal_dir(client)
    utils.ensure_storage_plugin_ready(client)

    # connect & authenticate to websocket
    headers = {"Authorization": f"Bearer {DEVICE_TOKEN}"}
    async for websocket in websockets.connect(HIVE_WEBSOCKET, extra_headers=headers, ping_interval=10):
        try:
            # send version and instances event on startup
            await tasks.send_version(websocket, 0)
            await tasks.send_instances(client, websocket, 0)
            await task_manager(websocket, client)

        except websockets.ConnectionClosed:
            loop = asyncio.get_event_loop()
            loop.close()

if __name__ == '__main__':
    # capture environment variables and configure logging
    logging.basicConfig(level=os.environ.get("LOG_LEVEL", "INFO"), format="\n%(asctime)s %(levelname)s - %(message)s")
    HIVE_WEBSOCKET = os.environ.get("HIVE_WEBSOCKET", "wss://ws.fractalnetworks.co")
    DEBUG = bool(os.environ.get("DEBUG", False))
    API_TOKEN = os.environ.get("API_TOKEN")
    DEVICE_SECRET = os.environ.get("DEVICE_SECRET")

    if not DEBUG and not API_TOKEN:
        logging.error("API TOKEN not provided.")
        sys.exit(1)
    if not DEVICE_SECRET:
        logging.error("DEVICE SECRET not provided.")
        sys.exit(1)

    asyncio.run(run())
