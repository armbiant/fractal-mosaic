from nacl import pwhash, secret, encoding, exceptions
import logging


class CryptoBaseClass():

    def __init__(self):
        '''
        Subclass to provide functionality.
        '''
        raise NotImplementedError()

    def encrypt(self, message):
        '''
        Should encrypt data based on the encryption used.
        '''
        raise NotImplementedError()

    def decrypt(self, message):
        '''
        Should decrypt data based on the encryption used.
        '''
        raise NotImplementedError()


class PassphraseEncryptionXSalsa20(CryptoBaseClass):
    '''
    Encrypts / decrypts data using passphrase provided using
    XSalsa20 stream cipher. Returns all data as base64 decoded strings
    '''

    def __init__(self, passphrase):
        self.key = self._derive_passphrase(passphrase)
        self.box = secret.SecretBox(self.key)

    def encrypt(self, message) -> str:
        '''
        Encrypts a given message.

        Returns:
        - encrypted_msg(str): base64 encoded encrypted message.
        '''
        encrypted_msg = self.box.encrypt(message.encode('utf-8'),
                                         encoder=encoding.Base64Encoder)
        return encrypted_msg.decode()

    def decrypt(self, message) -> str:
        '''
        Decrypts a given message.

        Returns:
        - decrypted_msg(str): Decrypted message.
        '''
        try:
            decrypted_msg = self.box.decrypt(message,
                                            encoder=encoding.Base64Encoder)
        except exceptions.CryptoError as error:
            logging.error(f"Error Decrypting Storage Keys. Potentially incorrect password. Error: {error}")
        return decrypted_msg.decode()

    def _derive_passphrase(self, passphrase: str) -> bytes:
        '''
        Derives passphrase using the PyNaCl argon2i kdf.
        '''
        kdf = pwhash.argon2i.kdf
        salt = b'fractalnetworks!'
        password = passphrase.encode('utf-8')

        # https://pynacl.readthedocs.io/en/latest/password_hashing/#module-level-constants-for-operation-and-memory-cost-tweaking
        ops = pwhash.argon2i.OPSLIMIT_MODERATE
        mem = pwhash.argon2i.MEMLIMIT_MODERATE
        key = kdf(secret.SecretBox.KEY_SIZE, password,
                  salt, opslimit=ops, memlimit=mem)
        return key
