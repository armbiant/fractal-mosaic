# August 16, 2022

# Merge Request [generate-env-on-stop](https://gitlab.com/fractalnetworks/components/hive-device/-/merge_requests/7)

# Description: Device now makes sure environment file generated for every command

# What's New

Device now generates environment file before running any docker-compose command.

`Makefile`

- `devices` target that just runs device1 and device2.

# What's Changed

Updated `check_volume_exists` to return True / False for if volume exists.

`docker_events` task no longer sends docker events to Hive Backend. Task still monitors events to see if app has gone unhealthy.

# August 12, 2022

# Merge Request [app/vaultwarden](https://gitlab.com/fractalnetworks/components/hive-device/-/merge_requests/5)

# Description: Device can now run vaultwarden. Also correctly acks back events to Device Service

# What's New

`vaultwarden_latest.yaml`

- Vaultwarden docker compose file. This allows the device to run a vaultwarden!

Device now acks back messages received from backend.

# What's Changed

Logging has been updated to also print the time in each message.

# August 8, 2022

# Merge Request [register-with-backend](https://gitlab.com/fractalnetworks/components/hive-device/-/merge_requests/4)

# Description: Device now registers itself with backend using API_TOKEN

# What's New

Device now registers itself with the Hive Backend by making a `POST <hive_backend>/api/v1/device/`. The Device authenticates with the specified endpoint by using the `API_TOKEN` environment variable. This `API_TOKEN` is the same token that is generated from the Mosaic UI.

# What's Changed

Event Loop Handling

- Now using `asyncio.run` in order to manage exits from tasks. This allows the device to correctly exit in the case of an error. Exiting allows docker to easily restart the device. Before, the Device would hang and no exit.

Docker Client

- Now using `from_env()` in order to use `DOCKER_` environment varaibles. This also supports sharing the docker socket (`/var/run/docker.sock`).

Docker Compose Files

- Device 1 has been moved to its own Docker Compose file.
- Renames `docker-compose.yml` -> `docker-compose.backend.yml` and now only contains backend containers.

Makefile

Uses variable set at top of the file to reduce duplication in targets.

# August 3, 2022

# Merge Request [device/fractal-volume](https://gitlab.com/fractalnetworks/components/hive-device/-/merge_requests/3)

# Description: Device now creates Fractal Storage Volumes! Disables handling standby event for now due to storage plugin bug

# What's New

Device creates Fractal Storage Volumes for the app and the Fractal Link.

Device can now handle being given multiple storage private keys.

Disables standby event for now until the read-only related bug for the storage plugin is fixed.

WikiJS Compose file updated to now use fractal storage volumes

WikiJS app (`wikijs_2.5.283.yaml`) now uses a storage volume for the Fractal Link. This allows us to store the certificate the Link gets!

#  July 26, 2022

# Merge Request: [device/standby](https://gitlab.com/fractalnetworks/components/hive-device/-/merge_requests/2)

# What's New

Hive Device can now handle `standby` events from the Hive Backend.

- Hive Device when receiving a standby event runs `docker-compose up --no-start` and sets its storage volume mode to `read-only`.

Adds README.md with same content as this merge request.
Hive Device now sets storage volume mode to `read-write` when starting an app.

I've copied the same information from the last merge request in here because I refer to some of those sections in the tests for the things added in this merge request.

# How to Run

1. `git clone git@gitlab.com:fractalnetworks/hive-device`
2. `make dev` - this will start a local backend and a device. This may take a second since the device waits on the `docker` service to be started before it will start.
3. Open two terminals.
4. In the first terminal, type `make device-logs` This gets the logs for the device 1.
5. In the second terminal, type `make device2-logs` This gets the logs for the device 2.
6. (Optional) Open another terminal and type `make hive-logs`. This displays the logs for the Hive Backend. While not necessary, these logs will show you all device requests that are incoming from the backend.

# Testing device-alive

After launching the device successfully, you should see a message in either of your device terminals that looks like the follow (should happen about every 30 seconds):

```text
INFO - Got a message from (hive-websocket): {'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'address': '172.21.0.4:57328', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'service': 'hive-websocket', 'type': 'device-alive'}
```

This message means that the device is correctly connected to backend. When a device is connected to the `Hive Websocket`, the `Hive Websocket` will send a `device-alive` message into the websocket. This message is then received by the `Hive Device Service` which forwards this message onto the `Hive Backend` in the form of an **HTTP Request**.

To verify that the devices are correctly connected to the backend:

1. Open your browser and go to `http://localhost:8005/admin`.
2. Login with the email: `admin@admin.com` and password `admin`
3. You should be able to navigate to the Devices table (`http://localhost:8005/admin/api/device/`) and click on either `test device` or `test device 2`. Either device's `health` should be `green`.

# Testing app start

1. Open your browser and go to `http://localhost:8005/admin`
2. Login with the email: `admin@admin.com` and password `admin`
3. Click `App Instances` (should be towards the bottom left)
4. Click `WikiJS - bfe664ff-db7e-454d-a038-cdf31f68c353 (AppInstance) - testuser1@testuser1.com - 78877f78-f15d-46af-be55-0e8f8b0196b0 (User)`.
5. You'll notice that the app's `state` is `not-installed`. In order to start the app on a device, we must first `install` it. Therefore, change `state` to `installed`. Then at the bottom, click `Save and continue editing`.
6. After a few seconds, back in your terminal, you should see that the device got a few messages. One of them should like something like this:

    ```text
    INFO - Got a message from (hive-backend): {'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'application': {'appstore': None, 'name': 'wikijs', 'version': '2.5.283'}, 'command': 'start', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'instance': 'bfe664ff-db7e-454d-a038-cdf31f68c353', 'links': {'default': {'domain': 'pueblo-earring.fractalnetworks.co', 'token': '3336a31e-dfa0-4681-ae4e-5f8c466f7024'}}, 'request': '245b15b0-87c6-4a82-b0ad-1a76138276c8', 'service': 'hive-backend', 'storage_apikeys': 'TSj+xvfgMNr6fEu3cskQ0plmN4EKDRkaGtSH53QUR4M=', 'type': 'device-request'}
    ```

    This message is the start event being sent from the `Hive Backend` to the `Hive Device Service`, which then forwards the message into the websocket.

    After the above message, more messages should follow:

    Writable Volume Creation:

    ```text
    INFO - Volume (bfe664ff-db7e-454d-a038-cdf31f68c353_wikijs) didn't exist. Creating

    INFO - Sent message: {'instance': None, 'event': {'Type': 'volume', 'Action': 'create', 'Actor': {'ID': 'bfe664ff-db7e-454d-a038-cdf31f68c353_wikijs', 'Attributes': {'driver': 'local'}}, 'scope': 'local', 'time': 1658846779, 'timeNano': 1658846779351198900}, 'type': 'docker', 'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'service': 'hive-device'}
    ```

    Compose Up (will be various containers being pulled, volumes mounted, etc):

    ```text
    {'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'address': '172.21.0.4:54040', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'event': {'Action': 'start', 'Actor': {'Attributes': {'com.docker.compose.config-hash': 'a5062d8ac08fbe594daf20886e2f4c0eef2559958c4bb8d0a2cca4bf3e485016', 'com.docker.compose.container-number': '1', 'com.docker.compose.depends_on': '', 'com.docker.compose.image': 'sha256:aae079a1d46b84dc2101d60ac020c3101313308c413df4eb3c7cc7a83c3f52f2', 'com.docker.compose.oneoff': 'False', 'com.docker.compose.project': 'bfe664ff-db7e-454d-a038-cdf31f68c353', 'com.docker.compose.project.config_files': '/apps/wikijs_2.5.283.yaml', 'com.docker.compose.project.environment_file': '/apps/bfe664ff-db7e-454d-a038-cdf31f68c353.env', 'com.docker.compose.project.working_dir': '/apps', 'com.docker.compose.service': 'wiki', 'com.docker.compose.version': '2.6.1', 'image': 'requarks/wiki:2.5.283', 'maintainer': 'requarks.io', 'name': 'bfe664ff-db7e-454d-a038-cdf31f68c353-wiki-1'}, 'ID': '838b7786f421c756e62afb82187c4ca2e438f582f3048b726cbf3e892f5750c3'}, 'Type': 'container', 'from': 'requarks/wiki:2.5.283', 'id': '838b7786f421c756e62afb82187c4ca2e438f582f3048b726cbf3e892f5750c3', 'scope': 'local', 'status': 'start', 'time': 1657918286.0, 'timeNano': 1.6579182863475044e+18}, 'instance': 'bfe664ff-db7e-454d-a038-cdf31f68c353-wiki-1', 'service': 'hive-device', 'type': 'docker'}
    ```

7. One of the last messages you should see should be:

    ```text
    INFO - Sent message: {'instance': 'bfe664ff-db7e-454d-a038-cdf31f68c353', 'state': 'running', 'type': 'instance-state', 'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'service': 'hive-device'}
    ```

    or

    ```text
    INFO - Got a message from (hive-device): {'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'address': '172.21.0.4:54040', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'instance': 'bfe664ff-db7e-454d-a038-cdf31f68c353', 'service': 'hive-device', 'state': 'running', 'type': 'instance-state'}
    ```

8. Back in your webbrowser refresh the page (`F5`). You should see `Current State` has changed to `running`.
9. Back in your terminal your device should be displaying the following message every 15 seconds or so:

    ```text
    INFO - Sent message: {'type': 'instances', 'instances': ['bfe664ff-db7e-454d-a038-cdf31f68c353'], 'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'service': 'hive-device'}
    ```

    Notice the UUID ('bfe664ff-db7e-454d-a038-cdf31f68c353') inside the `'instances': ['bfe664ff-db7e-454d-a038-cdf31f68c353']`. This UUID should match the one in your web browser at the top of the page. This means that the device knows that it is running the application. It knows this by asking docker for currently running containers.

10. You can test that the app's Fractal Link by going back into your web browser, refreshing (`F5`), then scrolling down to the `Links` section for the app. If you copy the domain from inside the text (should look something like `pueblo-earring.fractalnetworks.co`) and paste it into your web browser, you should see the wiki's installation web page. This means that things are working correctly!

    Note: There is the case where you've ran a lot of Fractal Links and are now rate limited by LetsEncrypt. In this case, when you navigate to the Fractal Link, you'll see an `SSL Protocol Error`.

11. Verify that the app is running correctly by running `cat apps/bfe664ff-db7e-454d-a038-cdf31f68c353.env`. You should see the output:

    ```text
    HIVE_APP_NAME=wikijs
    HIVE_APP_VERSION=2.5.283
    HIVE_STORAGE_KEYS=PevmOC5h+M8GF+76ePeFAU37k39BYtPwgxY7red2SHI=
    STORAGE_VOLUME_NAME=bfe664ff-db7e-454d-a038-cdf31f68c353_wikijs
    LINK_TOKEN_DEFAULT=3336a31e-dfa0-4681-ae4e-5f8c466f7024
    LINK_DOMAIN_DEFAULT=pueblo-earring.fractalnetworks.co
    ```

# Stopping the app

1. Back in your web browser, change `Target state` to `stopped`, then at the bottom, click `Save and continue editing`.
2. Switch to your device terminals. After a few seconds, you should see the message (there may be a few that go by really fast. This is because there are multiple containers that are stopping, etc):

    ```text
    INFO - Sent message: {'instance': 'bfe664ff-db7e-454d-a038-cdf31f68c353-wiki-1', 'event': {'status': 'kill', 'id': '838b7786f421c756e62afb82187c4ca2e438f582f3048b726cbf3e892f5750c3', 'from': 'requarks/wiki:2.5.283', 'Type': 'container', 'Action': 'kill', 'Actor': {'ID': '838b7786f421c756e62afb82187c4ca2e438f582f3048b726cbf3e892f5750c3', 'Attributes': {'com.docker.compose.config-hash': 'a5062d8ac08fbe594daf20886e2f4c0eef2559958c4bb8d0a2cca4bf3e485016', 'com.docker.compose.container-number': '1', 'com.docker.compose.depends_on': '', 'com.docker.compose.image': 'sha256:aae079a1d46b84dc2101d60ac020c3101313308c413df4eb3c7cc7a83c3f52f2', 'com.docker.compose.oneoff': 'False', 'com.docker.compose.project': 'bfe664ff-db7e-454d-a038-cdf31f68c353', 'com.docker.compose.project.config_files': '/apps/wikijs_2.5.283.yaml', 'com.docker.compose.project.environment_file': '/apps/bfe664ff-db7e-454d-a038-cdf31f68c353.env', 'com.docker.compose.project.working_dir': '/apps', 'com.docker.compose.service': 'wiki', 'com.docker.compose.version': '2.6.1', 'image': 'requarks/wiki:2.5.283', 'maintainer': 'requarks.io', 'name': 'bfe664ff-db7e-454d-a038-cdf31f68c353-wiki-1', 'signal': '15'}}, 'scope': 'local', 'time': 1657918675, 'timeNano': 1657918675741408000}, 'type': 'docker', 'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'service': 'hive-device'}
    ```

3. After a few more seconds, you should see this message:

    ```text
    INFO - Got a message from (hive-device): {'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'address': '172.21.0.4:57328', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'instance': 'bfe664ff-db7e-454d-a038-cdf31f68c353', 'service': 'hive-device', 'state': 'stopped', 'type': 'instance-state'}
    ```

4. Back in your web browser, refresh the page. You should see that `Current state` has changed to `stopped`.
5. In your device terminal, you should see the instances message look something like this:

    ```text
    INFO - Sent message: {'type': 'instances', 'instances': [], 'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'service': 'hive-device'}
    ```

# Testing App Not Found

1. In your web browser, navigate to `http://localhost:8005/admin`.
1. Login with the email `admin@admin.com` and password `admin`.
1. Click `App Instances` (should be towards the bottom left)
1. Click `Element - e813132e-7c4a-4ee7-8646-a6511176f0d8 (AppInstance) - testuser1@testuser1.com - 78877f78-f15d-46af-be55-0e8f8b0196b0 (User)`
1. Change `state` to `installed`, then at the bottom, click `Save and continue editing`.
1. After a few seconds, back in your terminal, you should see that the device got a few messages. One of them should like something like this:

    ```text
    INFO - Got a message from (hive-backend): {'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'application': {'appstore': None, 'name': 'element', 'version': '2.5.283'}, 'command': 'standby', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'instance': 'e813132e-7c4a-4ee7-8646-a6511176f0d8', 'links': {'default': {'domain': 'https://element.test-link.fractal.pub', 'token': 'my_link_token_2'}}, 'request': '09c62d1f-a52c-48e8-841d-35705801408c', 'service': 'hive-backend', 'storage_apikeys': 'my_super_secret_storage_key_2', 'type': 'device-request'}
    ```

1. After seeing that message you should see another few messages go by:

    ```text
    ERROR - App does not exist: /apps/element_2.5.283.yaml
    ```

# Testing Standby Event

After running `make dev` in one of your terminals,

1. In your web browser, navigate to `http://localhost:8005/admin`.
1. Login with the email `admin@admin.com` and password `admin`.
1. Click `App Instances` (should be towards the bottom left)
1. Click `WikiJS - bfe664ff-db7e-454d-a038-cdf31f68c353 (AppInstance) - testuser1@testuser1.com - 78877f78-f15d-46af-be55-0e8f8b0196b0 (User)`
1. Change `state` to `installed`, then at the bottom, click `Save and continue editing`.
1. The app should be picked up by the scheduler. Keep refreshing until you see a `Device` is chosen (may take about 10 seconds. The scheduler runs at a 10 second interval).
1. Based on the device that's chosen, look in the opposite device's terminal (if `test device` is running the app, look at `test device 2`'s terminal)
1. In that device's terminal, you should see the message like this:

    ```text
    INFO - Got a message from (hive-backend): {'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'application': {'appstore': None, 'name': 'wikijs', 'version': '2.5.283'}, 'command': 'standby', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'instance': 'bfe664ff-db7e-454d-a038-cdf31f68c353', 'links': {'default': {'domain': 'https://wiki.test-link.fractal.pub', 'token': 'my_link_token_1'}}, 'request': '09bc45d9-5f29-43ee-9331-35efec67a2be', 'service': 'hive-backend', 'storage_apikeys': 'my_super_secret_storage_key_1', 'type': 'device-request'}
    ```

If you missed it no stress, you should see it about every 30 seconds. The Hive Backend currently sends out `standby` events to all of the user's Devices that are currently healthy, excluding the one that is currently running the app.

The `standby` event means that the Hive Backend has asked the device to run the app in standby. This will trigger the device to run `docker volume create <INSTANCE_UUID>_<APP_NAME>...`, Upon receiving the above event, and finishing the **volume create** command, the device will send a message into the websocket:

```text
INFO - Sent message: {'instance': 'bfe664ff-db7e-454d-a038-cdf31f68c353', 'state': 'standby', 'type': 'instance-state', 'account': '78877f78-f15d-46af-be55-0e8f8b0196b0', 'device': '6c7fdf8e-23d7-4048-b6f4-197a40485795', 'service': 'hive-device'}
```

The purpose of the running standby is so devices not currently running the app can begin pulling in the storage snapshots for the app. This allows the app to start up more quickly in the event of failover since the device will already have the data necessary to start the app.
