// React
import React from 'react'

// Util
import { DataProps } from 'types'
import { ActionsType } from 'types'


// takes child component and adds data to them as a prop
const Group = ({ children, uuid, ...rest }: DataProps) => {

	// TODO: Actions for groups should work the same as actions for other data components
	const actions: ActionsType[] | {}[] = [{}]

	// success
	return React.cloneElement(children, { actions, ...rest })

}

export default Group