// Bootstrap
import Button from 'react-bootstrap/Button'

// Recoil
import {useRecoilState} from 'recoil'
import {inviteMember} from '../../../recoil/modals/InviteMember'

const MemberAdd = () => {
	const [state, setInviteMemberShow] = useRecoilState(inviteMember)
	const {show, uuid} = state

	const handleToggle = () => {
		setInviteMemberShow((state) => ({
			uuid: 'f93c9d21-8a5f-44a5-9adc-217b0be3aae2',
			show: !state.show,
		}))
	}

	return (
		<Button onClick={handleToggle} size='sm' className='w-100'>
			Invite Member
		</Button>
	)
}

export default MemberAdd
