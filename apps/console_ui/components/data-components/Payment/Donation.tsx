import React, {useState} from 'react'
import {useSession} from 'next-auth/react'

// Bootstrap
import Button from 'react-bootstrap/Button'

// Stripe
import {Elements} from '@stripe/react-stripe-js'
import {loadStripe} from '@stripe/stripe-js'
import {BILLING_PAYMENT_INTENT} from 'util/endpoints'
import CheckoutForm from './CheckoutForm'
import {appearance} from '../../../util/stripeAppearance'

// css
import css from './payment.module.css'

// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe(`${process.env.NEXT_PUBLIC_STRIPE_PUBLISH_KEY}`)

const Donation = () => {
	const {data: session} = useSession()
	const [clientSecret, setClientSecret] = useState('')

	// Create PaymentIntent when user selects amount
	function handleDonateClick() {
		// Only call billing service if user has selected a payment intent
		if (donationAmount) {
			setIsDonateClicked(true)

			fetch(`${BILLING_PAYMENT_INTENT}`, {
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({item: donationAmount, uuid: session?.uuid}),
			})
				.then((res) => res.json())
				.then((data) => {
					setClientSecret(data.clientSecret)
				})
				.catch((error) => {
					setErrorMessage((currState) => ({
						show: true,
						message: 'Failed to handle payment, refresh and try again.',
					}))
				})
		} else {
			// Show error message to user if no amount was selected
			setErrorMessage((currState) => ({...currState, show: !currState.show}))
		}
	}

	function handleDonationAmount(amount: number) {
		// Remove error message text if user selects an amount
		if (errorMessage.show) setErrorMessage((currState) => ({...currState, show: !currState.show}))
		setDonationAmount(amount)
	}

	const options = {
		appearance,
		clientSecret,
	}

	const [donationAmount, setDonationAmount] = useState<null | number>(null)

	// State to keep track of disabling donate now button
	const [isDonateClicked, setIsDonateClicked] = useState(false)

	// Client Side Validation
	interface ErrorMessage {
		show: boolean
		message: string
	}
	const [errorMessage, setErrorMessage] = useState<ErrorMessage>({
		show: false,
		message: 'Please Select a donation amount.',
	})

	return (
		<div className={`p-1 my-5 ${css.donationContainer}`}>
			<div className='text-center '>
				<h1 className='fs-1'>Support Fractal Network OS</h1>
				<p className='fs-3'>
					Fractal Network OS is an open source, free software project that empowers the user to own your own data.
					Please consider donating to help support our mission to create a better internet for all.
				</p>
			</div>
			<div className={`mt-5 ${css.donationGrid}`}>
				<h3 className={css.donationHeading}>Pay What You Can:</h3>
				<div className={`p-0 d-flex justify-content-around ${css.donationBtn}`}>
					<Button onClick={() => handleDonationAmount(5)} variant='secondary'>
						$ 5
					</Button>
					<Button onClick={() => handleDonationAmount(10)} variant='secondary'>
						$ 10
					</Button>
					<Button onClick={() => handleDonationAmount(20)} variant='secondary'>
						$ 20
					</Button>
					<span className={css.donationInputDollar}>
						<input
							type='number'
							step='0.01'
							min='0'
							max='999999.99'
							onChange={(e) => handleDonationAmount(parseInt(e.target.value))}
							className={`btn btn-secondary ${css.donationInput}`}
							placeholder='Custom'
						/>
					</span>
				</div>
				<Button
					disabled={isDonateClicked}
					onClick={() => {
						handleDonateClick()
					}}
					className={css.donationSubmit}
					variant='primary'>
					Donate Now
				</Button>
			</div>
			{errorMessage.show && <span className='text-danger'>{errorMessage.message}</span>}
			{clientSecret && (
				<div className='mt-4'>
					<Elements stripe={stripePromise} options={options}>
						<CheckoutForm />
					</Elements>
				</div>
			)}
		</div>
	)
}

export default Donation
