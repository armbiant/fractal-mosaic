import {useState} from 'react'

// Bootstrap
import Button from 'react-bootstrap/Button'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

const PersonalSettings = () => {
	// Shows Modal
	const [show, setShow] = useState(false)

	return (
		<>
			<Button
				className='d-none d-sm-flex flex-fill flex-grow-0 flex-shrink-0'
				variant='secondary'
				onClick={() => setShow(!show)}>
				<span className='d-none d-sm-block'>Contact Fractal Support</span>
			</Button>

			<Button className='d-flex d-sm-none px-1' variant='link' onClick={() => setShow(!show)}>
				<FontAwesomeIcon icon={['fas', 'gear']} fixedWidth />
			</Button>
		</>
	)
}

export default PersonalSettings
