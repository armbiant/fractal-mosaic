// React
import React from 'react'

// Util
import useIsActive from 'hooks/useIsActive'
import {DataProps, IcnType, ImgType, LinkToType, TitleType} from 'types'

import useSwrAuth from 'hooks/useSwrAuth'
import {useSession} from 'next-auth/react'

const MyProfile = ({children}: DataProps) => {
	const {data: session} = useSession()
	const {data, error} = useSwrAuth(`${process.env.NEXT_PUBLIC_MOSAIC_API}member/${session?.uuid}/`)

	const linkTo: LinkToType = 'home'
	const title: TitleType = 'My Profile'
	const img: string | null = data && data.profile_image ? data.profile_image : null
	let icn: IcnType | undefined

	// Show image, otherwise show icon by default
	if (img) {
	} else {
		icn = ['fas', 'circle-user']
	}

	const isActive = useIsActive()

	return React.cloneElement(children, {title, icn, img, linkTo, isActive})
}

export default MyProfile
