import React from 'react'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

// recoil
import {useRecoilState} from 'recoil'
import {removeMember as removeMemberState} from '../../recoil/modals/RemoveMember'

// Fractal Components
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'
import Member from 'data/members/Member'
import Group from 'data/groups/Group'
import CardSm from 'components/Card/CardSm'

// Hooks
import useModalToggle from 'hooks/useModalToggle'

const ModalMemberRemove = () => {
	const [removeMember, setRemoveMember] = useRecoilState(removeMemberState)
	const {show, uuid} = removeMember

	const toggleModal = useModalToggle(setRemoveMember)

	return (
		<Modal show={show} onHide={toggleModal} centered>
			<LayoutModalHeader>
				Remove &ensp;
				<Member uuid={uuid} children={<CardSm />} />
				&ensp; from &ensp;
				<Group uuid={'f93c9d21-8a5f-44a5-9adc-217b0be3aae2'} children={<CardSm />} />
			</LayoutModalHeader>
			<LayoutModalFooter>
				<Button onClick={toggleModal} variant='secondary'>
					Cancel
				</Button>
				<Button onClick={toggleModal} variant='primary'>
					Remove
				</Button>
			</LayoutModalFooter>
		</Modal>
	)
}

export default ModalMemberRemove
