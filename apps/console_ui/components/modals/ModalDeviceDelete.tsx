import React from 'react'

import {useRouter} from 'next/router'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

// Fractal Components
import CardSm from 'components/Card/CardSm'
import Device from 'data/devices/Device'
import Group from 'data/groups/Group'

// Recoil
import {useRecoilState} from 'recoil'
import {deleteDeviceState} from '../../recoil/modals/DeleteDeviceState'

// Hooks
import useModalToggle from 'hooks/useModalToggle'
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'

// Fractal Api
import useMosaicApi from 'hooks/useMosaicApi'

const ModalDeviceDelete = () => {
	const [state, setState] = useRecoilState(deleteDeviceState)
	const {show, uuid} = state

	const {deleteDevice} = useMosaicApi()

	const toggleModal = useModalToggle(setState)

	const router = useRouter()

	const handleDelete = async () => {
		await deleteDevice(uuid)
		toggleModal()
	}

	return (
		<Modal show={show} onHide={toggleModal} centered>
			<LayoutModalHeader>
				Remove &ensp;
				<Device uuid={uuid} children={<CardSm />} />
				{/* &ensp; from &ensp;
				<Group uuid={'f93c9d21-8a5f-44a5-9adc-217b0be3aae2'} children={<CardSm />} /> */}
			</LayoutModalHeader>
			<LayoutModalFooter>
				<Button onClick={toggleModal} variant='secondary'>
					Cancel
				</Button>
				<Button onClick={handleDelete} variant='primary'>
					Remove
				</Button>
			</LayoutModalFooter>
		</Modal>
	)
}

export default ModalDeviceDelete
