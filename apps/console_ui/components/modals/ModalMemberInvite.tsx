import React, {useState} from 'react'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import FormGroup from 'react-bootstrap/FormGroup'

// Recoil
import {useRecoilState} from 'recoil'
import {inviteMember as inviteMemberState} from '../../recoil/modals/InviteMember'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Fractal
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import AppList from 'data/apps/AppList'
import Group from 'data/groups/Group'
import CardSm from 'components/Card/CardSm'
import TextInput from 'components/Input/TextInput'

// Fractal Hook
import useModalToggle from 'hooks/useModalToggle'

const ModalMemberInvite = () => {
	const [inviteMember, setInviteMember] = useRecoilState(inviteMemberState)
	const {show, uuid} = inviteMember

	const toggleModal = useModalToggle(setInviteMember)

	const [email, setEmail] = useState('')

	const handleEmailInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setEmail(e.target.value)
	}

	return (
		<Modal show={show} onHide={toggleModal} onExited={() => setEmail('')} centered>
			<LayoutModalHeader>
				Invite new member to &ensp;
				<Group uuid={uuid} children={<CardSm />} />
			</LayoutModalHeader>
			<Modal.Body className='rounded-0 rounded-bottom'>
				<FormGroup className='mb-3'>
					<Form.Label>Email</Form.Label>
					<TextInput value={email} onChange={(e: any) => handleEmailInputChange(e as any)}>
						<Button>
							Send Invite
							<FontAwesomeIcon icon={['fas', 'paper-plane']} size='sm' fixedWidth />
						</Button>
					</TextInput>
				</FormGroup>
				<div className='text-warning mb-2'>These are the apps they will have permission to host:</div>
				<div className='d-flex flex-wrap gap-2'>
					<AppList children={<CardSm noCircle />} />
				</div>
			</Modal.Body>
		</Modal>
	)
}

export default ModalMemberInvite
