/* eslint-disable react/no-children-prop */
import React, {useState} from 'react'

// Recoil
import {useRecoilState} from 'recoil'
import {rescheduleApp} from '../../recoil/modals/RescheduleAppState'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

// Fractal Hooks
import useModalToggle from 'hooks/useModalToggle'

// Fractal Components
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'
import {DeviceSelectButton} from 'components/Modals/SelectDevice'

import DeviceList from 'data/devices/DeviceList'

import CardSm from 'components/Card/CardSm'
import CardMd from 'components/Card/CardMd'

import {IInstallAppProps} from 'components/Modals/SelectDevice'
import Device from 'data/devices/Device'
import App from 'data/apps/App'
import ModalDeviceInstall from 'components/Modals/ModalDeviceInstall'

// SWR
import {useSWRConfig} from 'swr'
import {APP_LIST_ENDPOINT, APP_ENDPOINT} from 'util/endpoints'
import {useSession} from 'next-auth/react'
import useMosaicApi from 'hooks/useMosaicApi'

const ModalAppReschedule = () => {
	const {data: session} = useSession()
	const {rescheduleApp: appReschedule} = useMosaicApi()
	const {mutate} = useSWRConfig()
	const [rescheduleAppState, setRescheduleAppState] = useRecoilState(rescheduleApp)

	const toggleModal = useModalToggle(setRescheduleAppState)

	const [deviceSelected, setDeviceSelected] = useState<IInstallAppProps>({
		title: 'Fractal Cloud Device',
		appUuid: rescheduleAppState.uuid,
		deviceUuid: '41e35656-349f-4d69-9e5d-6bbc735cf85e',
	})

	const ref = React.createRef()

	const handleOnExited = () => {
		setDeviceSelected({
			title: 'Fractal Cloud Device',
			appUuid: rescheduleAppState.uuid,
			deviceUuid: '41e35656-349f-4d69-9e5d-6bbc735cf85e',
		})
	}

	const [showDeviceModal, setShowDeviceModal] = useState(false)

	// Disable app reschedule http request
	const [isFetching, setIsFetching] = useState(false)

	const handleOnDone = async () => {
		setIsFetching(true)
		const response = await appReschedule(deviceSelected.appUuid, deviceSelected.deviceUuid)
		if (window.location.href.includes('console')) {
			if (response?.status == 200) {
				mutate([APP_LIST_ENDPOINT, session?.accessToken])
				mutate([`${APP_ENDPOINT}${deviceSelected.appUuid}/`, session?.accessToken])
			}
		}
		setIsFetching(false)
		toggleModal()
	}

	return (
		<>
			<Modal show={rescheduleAppState.show} onHide={toggleModal} onExit={handleOnExited} centered>
				<LayoutModalHeader>
					Deploy &ensp; <App uuid={rescheduleAppState.uuid} children={<CardSm noCircle />} /> &ensp; to: &ensp;
					{deviceSelected.deviceUuid && deviceSelected.title != 'Fractal Cloud Device' ? (
						<Device uuid={deviceSelected.deviceUuid} children={<CardSm />} />
					) : (
						<CardSm title='Fractal Cloud Device' health='green' />
					)}
				</LayoutModalHeader>
				<Modal.Body className='border-top-0 rounded-0 d-flex flex-column'>
					<DeviceSelectButton
						ref={ref}
						setDeviceSelected={setDeviceSelected}
						appUuid={rescheduleAppState.uuid}
						deviceUuid='41e35656-349f-4d69-9e5d-6bbc735cf85e'
						title='Fractal Cloud Device'
						health='green'
					/>
					<DeviceList>
						<DeviceSelectButton setDeviceSelected={setDeviceSelected} appUuid={rescheduleAppState.uuid} />
					</DeviceList>
					<Button className='px-0' variant='secondary' onClick={() => setShowDeviceModal(!showDeviceModal)}>
						<CardMd addDeviceButton title={'Add device'} />
					</Button>
				</Modal.Body>
				<LayoutModalFooter>
					<Button onClick={() => toggleModal()} variant='secondary'>
						Cancel
					</Button>
					<Button disabled={isFetching && true} onClick={() => handleOnDone()} variant='primary'>
						Done
					</Button>
				</LayoutModalFooter>
			</Modal>
			<ModalDeviceInstall show={showDeviceModal} setShow={setShowDeviceModal} />
		</>
	)
}

export default ModalAppReschedule
