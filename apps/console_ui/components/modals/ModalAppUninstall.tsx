// Recoil
import {useRecoilState} from 'recoil'
import {uninstallApp} from '../../recoil/modals/UninstallApp'

// Bootstrap
import Modal from 'react-bootstrap/Modal'

// Fractal Hooks
import useModalToggle from 'hooks/useModalToggle'

// Fractal Components
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import App from 'data/apps/App'
import CardSm from 'components/Card/CardSm'
import FormAppUninstall from '../forms/FormAppUninstall'

const ModalAppUninstall = () => {
	const [state, setState] = useRecoilState(uninstallApp)
	const {show, uuid} = state

	const toggleModal = useModalToggle(setState)

	return (
		<Modal show={show} onHide={toggleModal} centered>
			<LayoutModalHeader>
				Uninstall &ensp;
				<App uuid={uuid} children={<CardSm noCircle />} />
			</LayoutModalHeader>
			<Modal.Body className='rounded-0 rounded-bottom'>
				<App uuid={uuid}>
					<FormAppUninstall uuid={uuid} toggleModal={toggleModal} />
				</App>
			</Modal.Body>
		</Modal>
	)
}

export default ModalAppUninstall
