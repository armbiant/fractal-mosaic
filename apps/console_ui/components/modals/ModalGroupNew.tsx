// React
import {useState} from 'react'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

// Fractal
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'
import CardSm from 'components/Card/CardSm'
import TextInput from 'components/Input/TextInput'

// Recoil
import {useRecoilState} from 'recoil'
import {newGroupState} from '../../recoil/modals/NewGroupState'

// Icon library helpers
import PictureChooser from 'components/PictureChooser/PictureChooser'

const ModalGroupNew = () => {
	// Group Name State
	const [groupName, setGroupName] = useState('')
	const handleGroupNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setGroupName(e.target.value)
	}

	// Show / Hide Modal State
	const [show, setShow] = useRecoilState(newGroupState)
	const toggleShow = () => {
		setShow(false)
	}

	//
	const resetState = () => {
		setGroupName('')
	}

	return (
		<Modal show={show} onHide={() => toggleShow()} onExited={resetState} centered enforceFocus={false}>
			<LayoutModalHeader>
				Setup new group &ensp;
				{groupName !== '' && <CardSm title={groupName} />}
			</LayoutModalHeader>
			<Modal.Body className='rounded-0'>
				<Form>
					<Form.Group className='mb-3' controlId='formBasicEmail'>
						<Form.Label>Set a Group Name</Form.Label>
						<TextInput
							type='text'
							value={groupName}
							onChange={(e: any) => handleGroupNameChange(e as any)}
							placeholder='Work Stuff'
						/>
					</Form.Group>
				</Form>

				<Form.Label>Choose a Group Icon</Form.Label>
				<PictureChooser />
			</Modal.Body>
			<LayoutModalFooter>
				<Button onClick={() => toggleShow()} variant='secondary'>
					Cancel
				</Button>
				<Button onClick={() => toggleShow()} variant='primary'>
					Save
				</Button>
			</LayoutModalFooter>
		</Modal>
	)
}

export default ModalGroupNew
