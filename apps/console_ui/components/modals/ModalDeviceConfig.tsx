import React, {useState} from 'react'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'

// Fractal Components
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'
import Device from 'data/devices/Device'
import TextInput from 'components/Input/TextInput'
import CardSm from 'components/Card/CardSm'

// Recoil
import {useRecoilState} from 'recoil'
import {editConfigState} from '../../recoil/modals/EditConfigState'

// Fractal Hooks
import useModalToggle from 'hooks/useModalToggle'
import useMosaicApi from 'hooks/useMosaicApi'

const ModalDeviceConfig = () => {
	const {updateDeviceName} = useMosaicApi()
	const [configShow, setConfigShow] = useRecoilState(editConfigState)
	const {show, uuid} = configShow

	const [editedDeviceName, setEditedDeviceName] = useState('')

	const toggleModal = useModalToggle(setConfigShow)

	const handleSave = () => {
		if (editedDeviceName) {
			updateDeviceName(uuid, editedDeviceName)
		}
		toggleModal()
	}

	const handleDeviceNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		setEditedDeviceName(e.target.value)
	}
	const handleExited = () => {
		setEditedDeviceName('')
	}

	// Storage functionality in teh futre
	// const [storage, setStorage] = useState('106')
	// const handleRangeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
	// 	setStorage(e.target.value)
	// }

	return (
		<Modal show={show} onHide={toggleModal} onExited={handleExited} centered>
			<LayoutModalHeader>
				Edit &ensp;
				<Device uuid={uuid} children={<CardSm />} />
				&ensp; configuration
			</LayoutModalHeader>
			<Modal.Body className='rounded-0'>
				<Form>
					{/* Name Input Field */}
					<Form.Group className='mb-3' controlId='formBasicEmail'>
						<Form.Label>What should this device be called?</Form.Label>
						<Device uuid={uuid}>
							<TextInput type='text' value={editedDeviceName} onChange={(e: any) => handleDeviceNameChange(e as any)} />
						</Device>
					</Form.Group>
					{/* Storage Input */}
					{/* <Form.Group className='mb-0' controlId='formBasicEmail'>
						<Form.Label className='fw-bold my-0'>Storage Limit</Form.Label>
						<Form.Label className='d-block'>How much storage should Mosaic be able to use?</Form.Label>
						<div className='d-flex align-items-baseline gap-2'>
							<Form.Group className='d-flex flex-column flex-grow-1'>
								<Form.Range className='flex-grow-3 p-0' value={storage} onChange={(e) => handleRangeChange(e)} />
								<div className='d-flex justify-content-between'>
									<Form.Label className='m-0'>
										<small>0 GB</small>
									</Form.Label>
									<Form.Label className='m-0'>
										<small>{storage} GB</small>
									</Form.Label>
								</div>
							</Form.Group>
							<Form.Control
								placeholder={storage}
								value={storage}
								aria-label=''
								aria-describedby=''
								className='flex-shrink-1 text-end pe-2'
								style={{width: '8ch'}}
								onChange={(e) => handleRangeChange(e as any)}
							/>
							<Form.Label className='m-0'>GB</Form.Label>
						</div>
					</Form.Group> */}
				</Form>
			</Modal.Body>
			<LayoutModalFooter>
				<Button onClick={toggleModal} variant='secondary'>
					Cancel
				</Button>
				<Button onClick={handleSave} variant='primary'>
					Save
				</Button>
			</LayoutModalFooter>
		</Modal>
	)
}

export default ModalDeviceConfig
