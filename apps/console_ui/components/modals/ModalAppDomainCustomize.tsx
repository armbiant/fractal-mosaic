//React
import {useState} from 'react'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

// Recoil
import {useRecoilState} from 'recoil'
import {customDomainState} from '../../recoil/modals/CustomDomainState'

//Fractal Hooks
import useModalToggle from 'hooks/useModalToggle'

// Fractal Components
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'
import App from 'data/apps/App'
import CardSm from 'components/Card/CardSm'
import TextInput from 'components/Input/TextInput'
import TextCopy from 'components/Input/TextCopy'

const ModalAppDomainCustomize = () => {
	// Show/Hide Modal
	const [state, setShow] = useRecoilState(customDomainState)
	const {show, uuid} = state
	const toggleModal = useModalToggle(setShow)

	// Are we on Step 1 or 2
	const [step, setStep] = useState(1)
	const increment = () => setStep(step + 1)
	const decrement = () => setStep(step - 1)

	// Contents of input field from step 1
	const [input, setInput] = useState('')

	return (
		<Modal show={show} onHide={toggleModal} onExited={() => setStep(1)} centered>
			<LayoutModalHeader>
				Change Domain of &ensp;
				<App uuid={uuid} children={<CardSm title='App Name' noCircle />} />
			</LayoutModalHeader>

			{step === 1 && (
				<Modal.Body>
					<Form>
						<p className='text-muted mb-2'>Enter the domain or subdomain you want to give to this app.</p>
						<Form.Group className='mb-3' controlId='formBasicEmail'>
							<Form.Label>Domain or Subdomain</Form.Label>
							<TextInput
								type='text'
								value={input}
								onChange={(e: any) => setInput(e.target.value)}
								placeholder='subdomain.domain.com'
							/>
						</Form.Group>
					</Form>
				</Modal.Body>
			)}

			{step === 2 && <StepTwo domain={input} />}

			<LayoutModalFooter>
				{step === 1 ? (
					<>
						<Button onClick={toggleModal} variant='secondary'>
							Cancel
						</Button>
						<Button onClick={increment} variant='primary'>
							Next
						</Button>
					</>
				) : (
					<>
						<Button onClick={decrement} variant='secondary'>
							Back
						</Button>
						<Button onClick={toggleModal} variant='primary'>
							Save
						</Button>
					</>
				)}
			</LayoutModalFooter>
		</Modal>
	)
}

const StepTwo = ({domain}: any) => {
	// returns true if value of domain contains two periods
	const regex = /(\..*){2,}/
	let twoPeriods = regex.test(domain)

	return (
		<Modal.Body>
			<p className='text-muted'>Create these records in your domain provider&apos;s DNS configuration</p>
			<Form>
				<Row className='text-muted'>
					<Col sm={5}>
						<h5>Host Name</h5>
					</Col>
					<Col sm={2}>
						<h5>Type</h5>
					</Col>
					<Col sm={5}>
						<h5>Data</h5>
					</Col>
				</Row>

				{twoPeriods ? <SubDomainStuff /> : <DomainStuff />}
			</Form>
		</Modal.Body>
	)
}

const DomainStuff = () => {
	return (
		<>
			<Row className='mb-3 align-items-baseline'>
				<Col sm={5}>
					<TextCopy type='text' title={'@'} />
				</Col>
				<Col sm={2}>
					<Form.Label>A</Form.Label>
				</Col>
				<Col sm={5}>
					<TextCopy type='text' title={'16.45.32.53'} />
				</Col>
			</Row>
			<Row className='align-items-baseline'>
				<Col sm={5}>
					<TextCopy type='text' title={'@'} />
				</Col>
				<Col sm={2}>
					<Form.Label>AAAA</Form.Label>
				</Col>
				<Col sm={5}>
					<TextCopy type='text' title={'2bad:323k:gfdk'} />
				</Col>
			</Row>
		</>
	)
}

const SubDomainStuff = () => {
	return (
		<Row className='align-items-baseline'>
			<Col sm={5}>
				<TextCopy type='text' title={'chat'} />
			</Col>
			<Col sm={2}>
				<Form.Label>CNAME</Form.Label>
			</Col>
			<Col sm={5}>
				<TextCopy type='text' title={'tiger-hop.fractal.pub'} />
			</Col>
		</Row>
	)
}

export default ModalAppDomainCustomize
