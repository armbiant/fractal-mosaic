// Bootstrap
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import InputGroup from 'react-bootstrap/InputGroup'

// Font Awesome
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

// Types
import {TextInputProps} from 'types'

// React Hook Forms
import {useForm, SubmitHandler} from 'react-hook-form'
import {ErrorMessage} from '@hookform/error-message'

// Mosaic Api
import useMosaicApi from 'hooks/useMosaicApi'

type Inputs = {
	uninstallApp: string
}

const FormAppUninstall = ({title, uuid, toggleModal}: TextInputProps & {uuid: string} & {toggleModal: () => void}) => {
	// React Hook Forms
	const {
		register,
		handleSubmit,
		formState: {errors},
	} = useForm<Inputs>({
		criteriaMode: 'all',
	})

	const onSubmit: SubmitHandler<Inputs> = (data) => {
		console.log(data)
		return formSubmit()
	}

	const formSubmit = () => {
		uninstallApp(`${uuid}`)
		toggleModal()
	}

	// Mosaic Api Uninstall Method
	const {uninstallApp} = useMosaicApi()

	// Title to RegExp for pattern matching
	const patternMatch = new RegExp(title as string)

	return (
		<>
			<InputGroup className='mb-3'>
				<Form.Control
					isInvalid={Object.keys(errors).length > 0 && true}
					type='text'
					placeholder={title}
					data-testid='input field'
					{...register('uninstallApp', {
						required: 'This input is required',
						pattern: {value: patternMatch, message: 'Input did not match'},
					})}
				/>
				<Button onClick={handleSubmit(onSubmit)}>
					<FontAwesomeIcon icon={['fas', 'trash-can']} fixedWidth />
					Uninstall App
				</Button>
			</InputGroup>
			<ErrorMessage
				errors={errors}
				name='uninstallApp'
				render={({messages}) =>
					messages &&
					Object.entries(messages).map(([type, message]) => {
						console.log('type: ', type, 'message: ', message)
						return (
							<span className='text-danger d-flex align-items-center' key={type}>
								{' '}
								<FontAwesomeIcon icon={['fas', 'triangle-exclamation']} fixedWidth />
								{message}
							</span>
						)
					})
				}
			/>
		</>
	)
}

export default FormAppUninstall
