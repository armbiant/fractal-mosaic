// Type
import { ActionsType } from "types";

const deviceActions: ActionsType[] = [
  {
    // bad practice using icons like this, need to standardize
    icn: ["fas", "pen-to-square"],
    title: "Edit Config",
    func: () => {
      return;
    },
  },
  {
    icn: ["fas", "trash-can"],
    title: "Delete Device",
    func: () => {
      return;
    },
  },
];

export default deviceActions;
