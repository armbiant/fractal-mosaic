// Recoil
import { atom } from "recoil";

export const navMobile = atom<boolean>({
  key: "navMobile",
  default: false,
});