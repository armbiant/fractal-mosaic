// Recoil
import { atom } from "recoil";

export const deviceInstallState = atom({
  key: "deviceInstallState",
  default: {
    show: false,
    uuid: "",
  },
});
