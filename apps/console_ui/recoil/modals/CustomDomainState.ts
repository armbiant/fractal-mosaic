// Recoil
import { atom } from "recoil";
import { ModalProps } from 'types'

export const customDomainState = atom<ModalProps>({
  key: "customDomainState",
  default: {
    show: false,
    uuid: "",
  },
});
