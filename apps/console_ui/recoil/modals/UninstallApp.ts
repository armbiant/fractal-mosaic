// Recoil
import { atom } from "recoil";
import { ModalProps } from "types";

export const uninstallApp = atom<ModalProps>({
  key: "uninstallApp",
  default: {
    show: false,
    uuid: "",
  },
});
