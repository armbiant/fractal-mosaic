// Recoil
import { atom } from "recoil";
import { ModalProps } from "types";

export const removeMember = atom<ModalProps>({
  key: "removeMember",
  default: {
    show: false,
    uuid: "",
  },
});
