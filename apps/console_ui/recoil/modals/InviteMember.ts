// Recoil
import { atom } from "recoil";
import { ModalProps } from "types";

export const inviteMember = atom<ModalProps>({
  key: "inviteMember",
  default: {
    show: false,
    uuid: "",
  },
});
