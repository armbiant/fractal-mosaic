// Recoil
import { atom } from "recoil";
import { ModalProps } from "types";

export const deleteDeviceState = atom<ModalProps>({
  key: "deleteDeviceState",
  default: {
    show: false,
    uuid: "",
  },
});
