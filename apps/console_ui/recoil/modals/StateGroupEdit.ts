// Recoil
import { atom } from "recoil";
import { ModalProps } from "types";

export const StateGroupEdit = atom<ModalProps>({
  key: "stateGroupEdit",
  default: {
    show: false,
    uuid: "",
  },
});
