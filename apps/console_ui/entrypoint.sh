#!/bin/sh
cd apps/console_ui
if [ "$NODE_ENV" == "dev" ]; then
	npm install --verbose
	npm run dev
else
	npm run build
	npm run start
fi
