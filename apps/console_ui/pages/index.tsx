/* eslint-disable react/no-children-prop */
// React
import {useEffect, useState} from 'react'

// Next Js
import type {NextPage} from 'next'
import Head from 'next/head'
import {useSession} from 'next-auth/react'

// Global Nav
import MonoNav from 'components/MonoNav/MonoNav'
import MonoMobileNav from 'components/MonoNav/MonoMobileNav'
import MonoMobileNavItem from 'components/MonoNav/MonoMobileNavItem'

// Presentation
import Layout from 'components/Layout/Layout'
import LayoutDashboard from 'components/Layout/LayoutDashboard'

import LayoutNav from 'components/Layout/LayoutNav'
import LayoutCenter from 'components/Layout/LayoutCenter'
import LayoutSidebar from 'components/Layout/LayoutSidebar'

import NavHeader from 'components/Layout/Nav/NavHeader'
import NavContent from 'components/Layout/Nav/NavContent'
import NavFooter from 'components/Layout/Nav/NavFooter'

import CenterGridHeader from 'components/Layout/Center/CenterGridHeader'
import CenterGrid from 'components/Layout/Center/CenterGrid'

import SidebarHeader from 'components/Layout/Sidebar/SidebarHeader'
import SidebarContent from 'components/Layout/Sidebar/SidebarContent'
import SidebarFooter from 'components/Layout/Sidebar/SidebarFooter'

import SidebarMobile from 'components/Layout/Sidebar/SidebarMobile'
import SidebarMobileToggle from 'components/Layout/Sidebar/SidebarMobileToggle'

// Business Logic
import withAuth from 'auth/withAuth'

import MyProfile from '../components/data-components/MyProfile'
import Member from 'data/members/Member'
import GroupList from 'data/groups/GroupList'
import GroupAdd from '../components/data-components/groups/GroupAdd'
import PersonalSettings from '../components/data-components/PersonalSettings'

import AppAdd from 'data/apps/AppAdd'
import AppActions from '../components/data-components/apps/AppActions'
import AppList from 'data/apps/AppList'
import App from 'data/apps/App'

import DeviceActions from '../components/data-components/devices/DeviceActions'
import DeviceList from 'data/devices/DeviceList'
import DeviceAdd from '../components/data-components/devices/DeviceAdd'

// Fractal Style Components
import CenterTitle from 'components/Title/CenterTitle'
// import CenterTitle from 'components/CenterTitle'
import NavItemDesktop from 'components/SideNav/NavItemDesktop'
import CardMd from 'components/Card/CardMd'
import CardXl from 'components/Card/CardXl'

// Fractal Modals
import ModalDeviceDelete from '../components/modals/ModalDeviceDelete'
import ModalDeviceConfig from '../components/modals/ModalDeviceConfig'
import ModalGroupNew from '../components/modals/ModalGroupNew'
import ModalAppDomainCustomize from '../components/modals/ModalAppDomainCustomize'
import ModalAppDomainRemove from '../components/modals/ModalAppDomainRemove'
import ModalAppUninstall from '../components/modals/ModalAppUninstall'
import ModalDeviceInstall from 'components/Modals/ModalDeviceInstall'
import {deviceInstallState} from '../recoil/modals/DeviceInstallState'

// Bootstrap
import Tabs from 'react-bootstrap/Tabs'
import Tab from 'react-bootstrap/Tab'

// Recoil
import {useRecoilState} from 'recoil'
import FeaturedApps from '../components/data-components/apps/FeaturedApps'
import ModalAppReschedule from '../components/modals/ModalAppReschedule'

// Chatterbox widget
import Script from 'next/script'
import Donation from '../components/data-components/Payment/Donation'

const sidebarTitle = 'My Devices'
const testUser = '987ffb95-bbbe-4e92-acb4-7cb3223efdbd'

const Home: NextPage = () => {
	// Sidebar that can be opened from Mobile
	const [sidebar, setSidebar] = useState(false)
	const toggleSidebar = () => {
		setSidebar(!!!sidebar)
	}

	const [deviceInstall, setDeviceInstall] = useRecoilState(deviceInstallState)

	const [featuredServicesShow, setFeaturedServicesShow] = useState(true)

	// Init chatterbox
	useEffect(() => {
		if (typeof window !== 'undefined') {
			window.CHATTERBOX_CONFIG_LOCATION = '/chatterbox/config.json'
		}
	}, [])

	return (
		<>
			<Layout>
				<MonoNav uuid={testUser} useSession={useSession} currentDomain='Console'>
					<MyProfile children={<MonoMobileNavItem />} />
					{/* <GroupList children={<MonoMobileNavItem />} />
					<GroupAdd children={<MonoMobileNavItem />} /> */}
				</MonoNav>

				<SidebarMobile sidebar={sidebar} setSidebar={toggleSidebar}>
					<SidebarHeader>{sidebarTitle}</SidebarHeader>
					<SidebarContent>
						<DeviceList>
							<DeviceActions>
								<CardMd />
							</DeviceActions>
						</DeviceList>
					</SidebarContent>
					<SidebarFooter>
						<DeviceAdd />
					</SidebarFooter>
				</SidebarMobile>

				<LayoutDashboard>
					<LayoutNav>
						<NavHeader>
							<MyProfile children={<NavItemDesktop />} />
						</NavHeader>
						{/* <NavContent>
							<GroupList children={<NavItemDesktop />} />
						</NavContent>
						<NavFooter>
							<GroupAdd children={<NavItemDesktop />} />
						</NavFooter> */}
					</LayoutNav>
					<LayoutCenter>
						<CenterGridHeader>
							<CenterTitle title='Personal Space' />
							{/* <Member uuid={testUser} children={<CenterTitle />} /> */}
							<PersonalSettings />
							<SidebarMobileToggle icn={['fas', 'laptop']} setSidebar={toggleSidebar} />
							{/* <AppAdd /> */}
						</CenterGridHeader>
						<div className='px-3'>
							<Tabs defaultActiveKey='Services'>
								<Tab eventKey='Services' title='Services'>
									{featuredServicesShow && <FeaturedApps setShow={setFeaturedServicesShow} />}
									<CenterGrid>
										<AppList>
											<AppActions>
												<CardXl />
											</AppActions>
										</AppList>
									</CenterGrid>
								</Tab>
								<Tab eventKey='Donate' title='Support Us'>
									<Donation />
								</Tab>
							</Tabs>
						</div>
					</LayoutCenter>
					<LayoutSidebar>
						<SidebarHeader>{sidebarTitle}</SidebarHeader>
						<SidebarContent>
							<DeviceList>
								<DeviceActions>
									<CardMd />
								</DeviceActions>
							</DeviceList>
						</SidebarContent>
						<SidebarFooter>
							<DeviceAdd uuid={testUser} />
						</SidebarFooter>
					</LayoutSidebar>
				</LayoutDashboard>

				<MonoMobileNav currentDomain='Console' />
			</Layout>

			{/* Device Modals */}
			<ModalDeviceDelete />
			<ModalDeviceConfig />
			<ModalDeviceInstall show={deviceInstall.show} setShow={setDeviceInstall} />

			{/* Group Modals */}
			<ModalGroupNew />

			{/* App Modals */}
			<ModalAppDomainCustomize />
			<ModalAppDomainRemove />
			<ModalAppUninstall />
			<ModalAppReschedule />

			<Script
				src='/assets/parent.js'
				type='module'
				id='chatterbox-script'
				strategy='lazyOnload'
				onLoad={() => console.log(`script loaded correctly, window.FB has been populated`)}
			/>
		</>
	)
}

// export default Home
export default withAuth(Home)
