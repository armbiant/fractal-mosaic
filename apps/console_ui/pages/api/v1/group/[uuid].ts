import type {NextApiRequest, NextApiResponse} from 'next'

const handler = (req: NextApiRequest, res: NextApiResponse) => {
	const {uuid} = req.query
	switch (uuid) {
		case 'f93c9d21-8a5f-44a5-9adc-217b0be3aae2':
			res.status(200).json({
				displayName: 'Minecraft Group',
				img: '',
				icn: ['fas', 'gamepad'],
			})
			break
		case '987ffb95-bbbe-4e92-acb4-7cb3223efdbd':
			res.status(200).json({
				displayName: 'Team Fractal',
				img: '',
				icn: ['fas', 'business-time'],
			})
			break
		default:
			res.status(404)
			throw new Error(`requested app that doesn't exist`)
	}
}

export default handler
