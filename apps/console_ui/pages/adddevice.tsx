/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable react/no-unescaped-entities */
/* eslint-disable react/no-children-prop */
import {useState, useEffect} from 'react'
import {useSession} from 'next-auth/react'
import {useRouter} from 'next/router'

// Global Nav
import MonoNav from 'components/MonoNav/MonoNav'
import MonoMobileNav from 'components/MonoNav/MonoMobileNav'
import MonoMobileNavItem from 'components/MonoNav/MonoMobileNavItem'
import MyProfile from '../components/data-components/MyProfile'

// Fractal
import LayoutModalHeader from 'components/Layout/Modal/LayoutModalHeader'
import LayoutModalFooter from 'components/Layout/Modal/LayoutModalFooter'
import CardSm from 'components/Card/CardSm'
import Member from 'data/members/Member'
import TextInput from 'components/TextInput'
import TextCopy from 'components/TextCopy'
import LoadingSpinner from 'components/Spinners/LoadingSpinner'

// Fractal Client Api
import useMosaicApi from 'hooks/useMosaicApi'

// Bootstrap
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import axios from 'axios'
import {DEVICE_LIST_ENDPOINT} from 'util/endpoints'

const AddDevice = () => {
	const {data: session} = useSession()
	const router = useRouter()

	useEffect(() => {
		handleTokenGeneration()

		return () => {
			setDeviceName('')
			setDeviceToken(undefined)
		}
	}, [session?.accessToken])

	const [deviceToken, setDeviceToken] = useState<number | null | undefined>(undefined)
	const {generateDeviceToken} = useMosaicApi()
	const handleTokenGeneration = async () => {
		const token = await generateDeviceToken()
		if (token === null) {
			// console.log('IS NULL')
			setDeviceToken(null)
		} else {
			setDeviceToken(token.token)
		}
	}

	// Device name state
	const [deviceName, setDeviceName] = useState<string>('')
	const handleDeviceNameChange = (e: any) => {
		if (parseInt(e.target.value.length) !== 0) setDeviceName(e.target.value)
		if (parseInt(e.target.value.length) === 0) setDeviceName('')
	}
	let mosaicImage = 'fractalnetworks/hive-device:alpha'
	let dockerCommand: string = `docker run -it -v /var/run/docker.sock:/var/run/docker.sock -v /etc/hostname:/etc/hostname --env API_TOKEN=${deviceToken} --restart unless-stopped -d`
	let nameOption: string = `-e DEVICE_NAME="${deviceName}"`
	let dockerCommandWithName: string = `${dockerCommand} ${nameOption} ${mosaicImage}`

	const errorText: string = 'ERROR PLEASE TRY AGAIN'

	const handleDockerCommandText = (): string => {
		if (!deviceToken) return errorText
		if (deviceName == '') return `${dockerCommand} ${mosaicImage}`
		return dockerCommandWithName
	}

	const [isLoading, setIsLoading] = useState(false)
	const [isError, setIsError] = useState(false)

	let num = 5
	const handleAddDeviceDoneClick = async () => {
		try {
			const response = await axios.get(`${DEVICE_LIST_ENDPOINT}`, {
				headers: {Authorization: `Bearer ${session?.accessToken}`},
			})
			if (response.data.length > 0) {
				setIsLoading(false)
				router.push('/')
			} else {
				if (num) {
					setTimeout(() => {
						num--
						handleAddDeviceDoneClick()
					}, 2500)
				} else {
					setIsLoading(false)
					setIsError(true)
				}
			}
		} catch (error) {
			console.log(error)
			setIsLoading(false)
			setIsError(true)
		}
	}

	return (
		<>
			<MonoNav uuid={session?.uuid} useSession={useSession} currentDomain='Console'>
				<MyProfile children={<MonoMobileNavItem />} />
			</MonoNav>
			<div className='m-5'>
				<h1 className='text-center my-5 text-decoration-underline'>Add A Device to get started</h1>
				<LayoutModalHeader>
					Add
					{deviceName ? (
						<>
							<span>&ensp;</span>
							<CardSm title={deviceName} />
							<span>&ensp;</span>
						</>
					) : (
						<span>&nbsp;</span>
					)}
					device to &ensp;
					<Member uuid={session?.uuid} children={<CardSm />} />
				</LayoutModalHeader>

				<Modal.Body className='border-top-0 rounded-0'>
					<h3 className='mb-0'>Add One Device</h3>
					<Form>
						<Form.Group className='mb-3' controlId='formBasicEmail'>
							<Form.Label>Give this device a name (optional)</Form.Label>
							{/* @ts-ignore */}
							<TextInput title='Device Name' onChange={(e) => handleDeviceNameChange(e)} autoComplete='off' />
						</Form.Group>
						<Form.Group className='mb-3' controlId='formBasicEmail'>
							<Form.Label>Run this code in your device's terminal</Form.Label>
							<TextCopy
								title={handleDockerCommandText()}
								rtl={deviceName == '' ? false : true}
								code
								disabled={!deviceToken}
								loading={deviceToken === undefined}
							/>
						</Form.Group>
					</Form>
				</Modal.Body>

				<LayoutModalFooter>
					<Button
						variant='primary'
						className='ms-auto'
						onClick={() => {
							setIsLoading(true)
							handleAddDeviceDoneClick()
						}}>
						Done
					</Button>
				</LayoutModalFooter>

				<Modal.Body className='mt-3'>
					<h3>Add Multiple Devices</h3>
					<Form>
						<Form.Group className='mb-3' controlId='formBasicEmail'>
							<TextCopy
								title={handleDockerCommandText()}
								code
								disabled={!deviceToken}
								loading={deviceToken === undefined}
							/>
						</Form.Group>
					</Form>
				</Modal.Body>
			</div>

			{/* POLLING MODAL */}
			<Modal show={isLoading} centered>
				<LayoutModalHeader>
					<h1>Please wait while we get your device running.</h1>
				</LayoutModalHeader>
				<Modal.Body>
					<LoadingSpinner />
				</Modal.Body>
			</Modal>

			{/* ERROR OCCURED MODAL */}
			<Modal show={isError} onHide={() => setIsError(false)} centered>
				<LayoutModalHeader>
					<h1>Error creating device. Please try again.</h1>
				</LayoutModalHeader>
				<LayoutModalFooter>
					<Button variant='primary' className='ms-auto' onClick={() => setIsError(false)}>
						Cancel
					</Button>
				</LayoutModalFooter>
			</Modal>
		</>
	)
}

export default AddDevice
