// React
import {useState} from 'react'

// Next Js
import type {NextPage} from 'next'
import Head from 'next/head'
import {useRouter} from 'next/router'
import {useSession} from 'next-auth/react'

// Global Fractal Components
import MonoNav from 'components/MonoNav/MonoNav'
import MonoMobileNav from 'components/MonoNav/MonoMobileNav'
import MonoMobileNavItem from 'components/MonoNav/MonoMobileNavItem'

// Presentation
import Layout from 'components/Layout/Layout'
import LayoutDashboard from 'components/Layout/LayoutDashboard'

import LayoutNav from 'components/Layout/LayoutNav'
import LayoutCenter from 'components/Layout/LayoutCenter'
import LayoutSidebar from 'components/Layout/LayoutSidebar'

import NavHeader from 'components/Layout/Nav/NavHeader'
import NavContent from 'components/Layout/Nav/NavContent'
import NavFooter from 'components/Layout/Nav/NavFooter'

import CenterGridHeader from 'components/Layout/Center/CenterGridHeader'
import CenterGrid from 'components/Layout/Center/CenterGrid'

import SidebarHeader from 'components/Layout/Sidebar/SidebarHeader'
import SidebarContent from 'components/Layout/Sidebar/SidebarContent'
import SidebarFooter from 'components/Layout/Sidebar/SidebarFooter'

import SidebarMobile from 'components/Layout/Sidebar/SidebarMobile'
import SidebarMobileToggle from 'components/Layout/Sidebar/SidebarMobileToggle'

// Business Logic
import withAuth from 'auth/withAuth'

import MyProfile from '../../components/data-components/MyProfile'
import Group from 'data/groups/Group'
import GroupList from 'data/groups/GroupList'
import GroupAdd from '../../components/data-components/groups/GroupAdd'
import GroupSettings from '../../components/data-components/GroupSettings'

import AppAdd from 'data/apps/AppAdd'
import AppActions from '../../components/data-components/apps/AppActions'
import AppList from 'data/apps/AppList'

import MemberActions from '../../components/data-components/members/MemberActions'
import MemberList from 'data/members/MemberList'
import MemberAdd from '../../components/data-components/members/MemberAdd'

// Fractal Style Components
import CenterTitle from 'components/Title/CenterTitle'
import NavItemDesktop from 'components/SideNav/NavItemDesktop'
import CardMd from 'components/Card/CardMd'
import CardXl from 'components/Card/CardXl'

// Fractal Modals
import ModalGroupNew from '../../components/modals/ModalGroupNew'
import ModalAppDomainCustomize from '../../components/modals/ModalAppDomainCustomize'
import ModalAppDomainRemove from '../../components/modals/ModalAppDomainRemove'
import ModalAppUninstall from '../../components/modals/ModalAppUninstall'
import ModalMemberInvite from '../../components/modals/ModalMemberInvite'
import ModalMemberRemove from '../../components/modals/ModalMemberRemove'
import ModalGroupEdit from '../../components/modals/ModalGroupEdit'

const GroupPage: NextPage = () => {
	// Sidebar that can be opened from Mobile
	const [sidebar, setSidebar] = useState(false)
	const toggleSidebar = () => {
		setSidebar(!!!sidebar)
	}
	const sidebarFalse = () => {
		setSidebar(false)
	}

	const router = useRouter()
	const uuid = String(router.query.uuid)

	const sidebarTitle = 'Group Members'

	const testUser = '987ffb95-bbbe-4e92-acb4-7cb3223efdbd'

	return (
		<>
			<Layout>
				<MonoNav uuid={testUser} useSession={useSession} currentDomain='Console'>
					<MyProfile children={<MonoMobileNavItem />} />
					<GroupList children={<MonoMobileNavItem />} />
					<GroupAdd children={<MonoMobileNavItem />} />
				</MonoNav>

				<SidebarMobile sidebar={sidebar} setSidebar={sidebarFalse}>
					<SidebarHeader>{sidebarTitle}</SidebarHeader>
					<SidebarContent>
						<MemberList>
							<MemberActions>
								<CardMd />
							</MemberActions>
						</MemberList>
					</SidebarContent>
					<SidebarFooter>
						<MemberAdd />
					</SidebarFooter>
				</SidebarMobile>

				<LayoutDashboard>
					<LayoutNav>
						<NavHeader>
							<MyProfile children={<NavItemDesktop />} />
						</NavHeader>
						<NavContent>
							<GroupList children={<NavItemDesktop />} />
						</NavContent>
						<NavFooter>
							<GroupAdd children={<NavItemDesktop />} />
						</NavFooter>
					</LayoutNav>
					<LayoutCenter>
						<CenterGridHeader>
							<Group uuid={uuid} children={<CenterTitle />} />
							<GroupSettings />
							<SidebarMobileToggle icn={['fas', 'user-group']} setSidebar={toggleSidebar} />
							{/* <AppAdd/> */}
						</CenterGridHeader>
						<CenterGrid>
							<AppList>
								<AppActions>
									<CardXl />
								</AppActions>
							</AppList>
						</CenterGrid>
					</LayoutCenter>
					<LayoutSidebar>
						<SidebarHeader>{sidebarTitle}</SidebarHeader>
						<SidebarContent>
							<MemberList>
								<MemberActions>
									<CardMd />
								</MemberActions>
							</MemberList>
						</SidebarContent>
						<SidebarFooter>
							<MemberAdd />
						</SidebarFooter>
					</LayoutSidebar>
				</LayoutDashboard>

				<MonoMobileNav currentDomain='Console' />
			</Layout>

			{/* Group Modals */}
			<ModalGroupNew />
			<ModalGroupEdit />

			{/* App Modals */}
			<ModalAppDomainCustomize />
			<ModalAppDomainRemove />
			<ModalAppUninstall />

			{/* Member Modals */}
			<ModalMemberInvite />
			<ModalMemberRemove />
		</>
	)
}

export default withAuth(GroupPage)
