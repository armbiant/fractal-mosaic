// Next JS
import type {AppProps} from 'next/app'

// Next Auth
import {SessionProvider} from 'next-auth/react'

// Plausible Analytics
import PlausibleProvider from 'next-plausible'

// Font Awesome
import {library} from '@fortawesome/fontawesome-svg-core'
import {fas} from '@fortawesome/free-solid-svg-icons'
import {fab} from '@fortawesome/free-brands-svg-icons'
library.add(fas, fab)

// Recoil
import {RecoilRoot} from 'recoil'

// Styles
import '@fortawesome/fontawesome-svg-core/styles.css'
import 'components/styles/fn-bootsrap/bootstrap.scss'
import 'components/styles/fonts.scss'
import 'components/styles/globals.css'
import 'photoswipe/dist/photoswipe.css'

function MyApp({Component, pageProps: {session, ...pageProps}}: AppProps) {
	return (
		<SessionProvider session={session} refetchInterval={5 * 60}>
			<PlausibleProvider domain='fractalnetworks.co'>
				<RecoilRoot>
					<Component {...pageProps} />
				</RecoilRoot>
			</PlausibleProvider>
		</SessionProvider>
	)
}

export default MyApp
