//Next JS
export default function Custom404() {
	return (
		<div className='h-100 overflow-hidden'>
			<div className='w-100 h-100 d-flex align-items-center justify-content-center'>
				<div>
					<h1 className='text-center'>404</h1>
					<h2 className='text-center text-muted'>Page Not Found</h2>
				</div>
			</div>
		</div>
	)
}
