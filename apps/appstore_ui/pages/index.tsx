import {useEffect, useState} from 'react'

// Next
import type {NextPage} from 'next'
import {useSession} from 'next-auth/react'
import {useRouter} from 'next/router'

// Library
import MonoNav from 'components/MonoNav/MonoNav'
import MonoMobileNav from 'components/MonoNav/MonoMobileNav'

// Presentation
import Layout from 'components/Layout/Layout'
import LayoutDashboard from 'components/Layout/LayoutDashboard'

import LayoutCenter from 'components/Layout/LayoutCenter'
import LayoutSidebar from 'components/Layout/LayoutSidebar'

import CenterListHeader from 'components/Layout/Center/CenterListHeader'
import CenterList from 'components/Layout/Center/CenterList'

import SidebarHeader from 'components/Layout/Sidebar/SidebarHeader'
import SidebarContent from 'components/Layout/Sidebar/SidebarContent'
import SidebarFooter from 'components/Layout/Sidebar/SidebarFooter'

import SidebarMobile from 'components/Layout/Sidebar/SidebarMobile'
import SidebarMobileToggle from 'components/Layout/Sidebar/SidebarMobileToggle'

// Business Logic
import CategoryList from '../components/Categories/CategoryList'

// Fractal Style Components
import CardXl from 'components/Card/CardXl'

// Bootstrap
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'

// Icons
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {faMagnifyingGlass} from '@fortawesome/free-solid-svg-icons'

// recoil
import {useRecoilState} from 'recoil'
import {categoriesState} from '../recoil/categories'
import GithubStars from 'components/GithubStars/GithubStars'

// Appstore client api
import useMosaicApi from 'hooks/useMosaicApi'

export type TSortByName = 'A-Z' | 'Z-A' | 'Popularity' | ''

const Home: NextPage = () => {
	// Api client methods
	const {appCatalogSearch} = useMosaicApi()
	const router = useRouter()

	const [queryState, setQueryState] = useState('')
	const [filter, setFilter] = useRecoilState(categoriesState)
	const [sort, setSort] = useState<'asc' | 'desc'>('asc')
	const [sortByName, setSortByName] = useState<TSortByName>('')
	const [appData, setAppData] = useState<any[]>([])

	const handleInputChange = (e: any) => {
		setQueryState(e.target.value)
	}

	useEffect(() => {
		if (router.isReady) {
			const {search, sort, categories} = router.query
			search && setQueryState(search as string)
			sort && setSort(sort as 'asc' | 'desc')
			if (Array.isArray(categories)) {
				let tempArr = [...categories]
				for (let category of tempArr) {
					setFilter((prevState) => [...prevState, category])
				}
			}
			if (typeof categories === 'string') {
				setFilter((prevState) => [...prevState, categories])
			}
		}
	}, [router.isReady])

	useEffect(() => {
		router.push({
			pathname: '/',
			query: {
				search: queryState,
				categories: filter,
				sort: sort,
			},
		})
		;(async () => {
			const response = await appCatalogSearch(queryState && queryState, filter && filter, sort && sort)
			await setAppData(response)
		})()
	}, [queryState, filter, sort])

	const [sidebar, setSidebar] = useState(false)
	const toggleSidebar = () => {
		setSidebar(!!!sidebar)
	}
	const sidebarFalse = () => {
		setSidebar(false)
	}
	const {data: session} = useSession()
	const sidebarTitle = 'Categories'
	const testUser = '987ffb95-bbbe-4e92-acb4-7cb3223efdbd'

	return (
		<>
			<Layout>
				<MonoNav uuid={testUser} useSession={useSession} currentDomain='Store' />
				<SidebarMobile sidebar={sidebar} setSidebar={sidebarFalse}>
					<SidebarHeader>{sidebarTitle}</SidebarHeader>
					<SidebarContent>
						<CategoryList />
					</SidebarContent>
					{/* <SidebarFooter></SidebarFooter> */}
				</SidebarMobile>
				<LayoutDashboard>
					<LayoutSidebar>
						<SidebarHeader>{sidebarTitle}</SidebarHeader>
						<SidebarContent>
							<CategoryList />
						</SidebarContent>
						{/* <SidebarFooter></SidebarFooter> */}
					</LayoutSidebar>
					<LayoutCenter>
						<CenterListHeader>
							<InputGroup className='rounded mb-3'>
								<InputGroup.Text className='bg-white'>
									<FontAwesomeIcon icon={['fas', 'magnifying-glass']} />
								</InputGroup.Text>
								<FormControl
									placeholder='Search'
									aria-label='Search'
									onChange={(e) => handleInputChange(e)}
									value={queryState}
									className='bg-white rounded-end'
								/>
								<SidebarMobileToggle icn={['fas', 'filter']} setSidebar={toggleSidebar} />
							</InputGroup>
							<div className='d-flex justify-content-between align-items-center'>
								<h3 className='fw-bold m-0'>{queryState ? `Results for ${queryState}` : 'All Apps'}</h3>
								<DropdownButton
									align='end'
									id='dropdown-basic-button'
									title={sortByName ? sortByName : 'Sort by ...'}
									variant='secondary'
									size='sm'>
									<Dropdown.Item
										onClick={() => {
											setSort('asc')
											setSortByName('Popularity')
										}}>
										Popularity
									</Dropdown.Item>
									<Dropdown.Item
										onClick={() => {
											setSort('asc')
											setSortByName('A-Z')
										}}>
										A-Z
									</Dropdown.Item>
									<Dropdown.Item
										onClick={() => {
											setSort('desc')
											setSortByName('Z-A')
										}}>
										Z-A
									</Dropdown.Item>
								</DropdownButton>
							</div>
						</CenterListHeader>
						<CenterList>
							{appData.map((item) => {
								return (
									<div className='mb-3' key={item.uuid}>
										<CardXl
											img={item.image}
											title={item.name}
											subtitle={item.subtitle}
											description={item.description}
											detailPage={`/app/${item.name.toLowerCase()}`}
											github={<GithubStars stars={item.cached_github_stars} url={item.github_url} />}
										/>
									</div>
								)
							})}
						</CenterList>
					</LayoutCenter>
				</LayoutDashboard>
				<MonoMobileNav currentDomain='Store' />
			</Layout>
		</>
	)
}

export default Home
