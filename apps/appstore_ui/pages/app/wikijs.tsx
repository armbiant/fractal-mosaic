// Next Js
import type {NextPage} from 'next'
import {useSession, signIn} from 'next-auth/react'

// Bootstrap
import Button from 'react-bootstrap/Button'

// Global Fractal Components
import MonoNav from 'components/MonoNav/MonoNav'
import MonoMobileNav from 'components/MonoNav/MonoMobileNav'
import CardXXl from 'components/Card/CardXXl'
import InstallAlert from 'components/Alerts/InstallAlert'
import PictureLg from 'components/Picture/PictureLg'
import PictureXl from 'components/Picture/PictureXl'

// Presentation
import Layout from 'components/Layout/Layout'
import LayoutDashboard from 'components/Layout/LayoutDashboard'
import LayoutCenter from 'components/Layout/LayoutCenter'
import CenterList from 'components/Layout/Center/CenterList'

// Local Fractal Components
import Carousel from '../../components/Carousel/Carousel'

import useSWR from 'swr'

// Photoswipe Gallery
import {Gallery, Item} from 'react-photoswipe-gallery'

// SSR IMPORTS
import {GetServerSideProps} from 'next'
import {useIsAppInstalledSSR} from 'hooks/useIsAppInstalledSSR'

const APP_UUID = process.env.NEXT_PUBLIC_WIKIJS_APP_UUID

interface IsInstalled {
	isInstalled: boolean
}

const AppPage: NextPage<IsInstalled> = ({isInstalled}) => {
	const testUser = '78877f78-f15d-46af-be55-0e8f8b0196b0'

	const {data: session, status: loading} = useSession()

	const fetcher = (arg: string) => fetch(arg).then((res) => res.json())

	const {data, error} = useSWR(`${process.env.NEXT_PUBLIC_MOSAIC_API}catalog/app/${APP_UUID}/`, fetcher)

	const wikiJsLinks = [
		{
			linkName: 'Homepage',
			linkUrl: 'https://js.wiki/',
		},
		{
			linkName: 'Docs',
			linkUrl: 'https://docs.requarks.io/',
		},
		{
			linkName: 'Support',
			linkUrl: 'https://js.wiki/support',
		},
		{
			linkName: 'Github',
			linkUrl: 'https://github.com/Requarks/wiki',
		},
	]

	const wikiJsCategories = [
		{
			category: 'Wiki',
		},
	]

	const carouselApps = [
		{
			appName: 'NextCloud',
		},
		{
			appName: 'BitWarden',
		},
	]

	const screenshots = [
		{
			src: '/test-img/wikijs_screenshot-interface.png',
			width: '2550',
			height: '1200',
		},
		{
			src: '/test-img/wikijs_screenshot-newpage.png',
			width: '849',
			height: '625',
		},
		{
			src: '/test-img/wikijs_screenshot-page-metadata.png',
			width: '1498',
			height: '1116',
		},
		{
			src: '/test-img/wikijs_screenshot-selecteditor.png',
			width: '700',
			height: '637',
		},
		{
			src: '/test-img/wikijs-screenshot_media.png',
			width: '1839',
			height: '776',
		},
		{
			src: '/test-img/wikijs-screenshot_nav.png',
			width: '2431',
			height: '1185',
		},
	]

	return (
		<Layout>
			<MonoNav uuid={testUser} useSession={useSession} currentDomain='Store' />
			<LayoutDashboard>
				<LayoutCenter>
					<CenterList>
						<CardXXl
							isInstalled={isInstalled}
							uuid={APP_UUID}
							useSession={useSession}
							title={'WikiJs'}
							subtitle={'Open source Wiki software'}
							description={
								"The most powerful and extensible open source Wiki software. Make documentation a joy to write using Wiki.js's beautiful and intuitive interface!"
							}
							githubStars={data && data.cached_github_stars ? data.cached_github_stars : 17.9}
							githubUrl={'https://github.com/Requarks/wiki'}
							logo={`${process.env.NEXT_PUBLIC_API_BASE_URL}/media/app_catalog/logo_icon_wikijs.svg`}
							img={'/test-img/hero_wikijs.png'}
							categories={wikiJsCategories}
							links={wikiJsLinks}
						/>
						{loading == 'unauthenticated' && <InstallAlert title='WikiJs' />}
						<div>
							<PictureLg img='/test-img/wikijs_icon-protected.svg' circle={false} />
							<h3 className='m-0 mt-1'>Protected</h3>
							<p className='mt-1 mb-5'>Make your wiki public, completely private or a mix of both.</p>

							<PictureLg img='/test-img/wikijs_icon-administration.svg' circle={false} />
							<h3 className='m-0 mt-1'>Administration</h3>
							<p className='mt-1 mb-5'>Manage all aspects of your wiki using the extensive and intuitive admin area.</p>

							<PictureLg img='/test-img/wikijs_icon-customizable.svg' circle={false} />
							<h3 className='m-0 mt-1'>Customizable</h3>
							<p className='mt-1 mb-5'>Fully customize the appearance of your wiki, including a light and dark mode.</p>
						</div>
						<h3>Screenshots</h3>
						<Gallery>
							{screenshots.map((screenshot, index) => {
								return (
									<Item
										key={index}
										original={screenshot.src}
										thumbnail={screenshot.src}
										width={screenshot.width}
										height={screenshot.height}>
										{/* @ts-ignore */}
										{({ref, open}) => <img ref={ref} onClick={open} src={screenshot.src} style={{width: '100%'}} />}
									</Item>
								)
							})}
						</Gallery>
					</CenterList>
					{/* <Carousel title={'Similar apps'} items={carouselApps} /> */}
				</LayoutCenter>
			</LayoutDashboard>
			<MonoMobileNav currentDomain='Store' />
		</Layout>
	)
}

export const getServerSideProps: GetServerSideProps = async (context) => {
	return useIsAppInstalledSSR(context, APP_UUID as string)
}

export default AppPage
