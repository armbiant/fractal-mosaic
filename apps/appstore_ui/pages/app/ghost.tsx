// Next Js
import type {NextPage} from 'next'
import {useSession, signIn} from 'next-auth/react'

// Global Fractal Components
import MonoNav from 'components/MonoNav/MonoNav'
import MonoMobileNav from 'components/MonoNav/MonoMobileNav'
import CardXXl from 'components/Card/CardXXl'
import InstallAlert from 'components/Alerts/InstallAlert'
import PictureAppDetailIcon from 'components/Picture/PictureAppDetailIcon'

// Presentation
import Layout from 'components/Layout/Layout'
import LayoutDashboard from 'components/Layout/LayoutDashboard'
import LayoutCenter from 'components/Layout/LayoutCenter'
import CenterList from 'components/Layout/Center/CenterList'

// Photoswipe Gallery
import {Gallery, Item} from 'react-photoswipe-gallery'
import useSWR from 'swr'

// SSR IMPORTS
import {GetServerSideProps} from 'next'
import {useIsAppInstalledSSR} from 'hooks/useIsAppInstalledSSR'

const APP_UUID = process.env.NEXT_PUBLIC_GHOST_APP_UUID

interface IsInstalled {
	isInstalled: boolean
}

const AppPage: NextPage<IsInstalled> = ({isInstalled}) => {
	const testUser = '78877f78-f15d-46af-be55-0e8f8b0196b0'

	const {data: session, status: loading} = useSession()

	const fetcher = (arg: string) => fetch(arg).then((res) => res.json())

	const {data, error} = useSWR(`${process.env.NEXT_PUBLIC_MOSAIC_API}catalog/app/${APP_UUID}/`, fetcher)

	const ghostLinks = [
		{
			linkName: 'Homepage',
			linkUrl: 'https://ghost.org/',
		},
		{
			linkName: 'Help',
			linkUrl: 'https://ghost.org/help/',
		},
		{
			linkName: 'Github',
			linkUrl: 'https://github.com/TryGhost/Ghost',
		},
	]

	const ghostCategories = [
		{
			category: 'Collaboration',
		},
		{
			category: 'Media',
		},
	]

	const screenshots = [
		{
			src: `${process.env.NEXT_PUBLIC_API_BASE_URL}/media/app_catalog/logo_icon_ghost.png`,
			width: '556',
			height: '204',
		},
	]

	return (
		<Layout>
			<MonoNav uuid={testUser} useSession={useSession} currentDomain='Store' />
			<LayoutDashboard>
				<LayoutCenter>
					<CenterList>
						<CardXXl
							isInstalled={isInstalled}
							uuid={APP_UUID}
							useSession={useSession}
							title='Ghost'
							subtitle={'Turn your audience into a business'}
							description={
								'Ghost is a powerful app for new-media creators to publish, share, and grow a business around their content. It comes with modern tools to build a website, publish content, send newsletters & offer paid subscriptions to members.'
							}
							githubStars={data && data.cached_github_stars ? data.cached_github_stars : 41.0}
							githubUrl={'https://github.com/TryGhost/Ghost'}
							logo={`${process.env.NEXT_PUBLIC_API_BASE_URL}/media/app_catalog/logo_icon_ghost.png`}
							img={'/test-img/ghostHero.png'}
							categories={ghostCategories}
							links={ghostLinks}
						/>
						{loading == 'unauthenticated' && <InstallAlert title='Ghost' />}
						<div>
							<div className='mt-3 mb-4'>
								<h2>
									Don’t settle for another basic profile that looks just like everyone else. Make it <em>yours</em>.
								</h2>
								<ul>
									<li>
										Launch your website with a selection of beautiful free themes and tweak the design settings to
										perfectly match your brand and style.
									</li>
									<li>
										Go even further with hundreds of custom themes in our marketplace, or build your own completely
										custom design from scratch.
									</li>
								</ul>
							</div>

							<h3 className='m-0'>Rich media & dynamic cards.</h3>
							<h4>
								Modern publishing requires more than just words. Expand your story with image galleries, gifs, video,
								audio, products, info boxes, accordion toggles, downloadable files, bookmarks, and so much more.
							</h4>
							<PictureAppDetailIcon img='/test-img/richMediaGhost.png' />

							<h3 className='m-0'>Newsletters built-in.</h3>
							<h4>
								Deliver posts by email newsletter to your audience, so they’ll be in the loop whenever something new
								goes live. Segment your audience and send multiple different newsletters based on preference.
							</h4>
							<PictureAppDetailIcon img='/test-img/newsletterGhost.png' />

							<h3 className='m-0'>Offers & promotions.</h3>
							<h4>
								Entice new subscribers with offers and promotions to grow your business. Run a 30% discount for your
								first 100 subscribers, an 80%-off Black Friday sale, or a special promotion for a live event.
							</h4>
							<PictureAppDetailIcon img='/test-img/offersPromotions.png' />
						</div>
						{/* <h3>Screenshots</h3> */}
						{/* <Gallery>
							{screenshots.map((screenshot, index) => {
								return (
									<Item
										key={index}
										original={screenshot.src}
										thumbnail={screenshot.src}
										width={screenshot.width}
										height={screenshot.height}>
										{({ref, open}) => <img ref={ref} onClick={open} src={screenshot.src} style={{width: '100%'}} />}
									</Item>
								)
							})}
						</Gallery> */}
					</CenterList>
					{/* <Carousel title={'Similar apps'} items={carouselApps} /> */}
				</LayoutCenter>
			</LayoutDashboard>
			<MonoMobileNav currentDomain='Store' />
		</Layout>
	)
}

export const getServerSideProps: GetServerSideProps = async (context) => {
	return useIsAppInstalledSSR(context, APP_UUID as string)
}

export default AppPage
