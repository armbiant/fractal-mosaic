// Next Js
import type {NextPage} from 'next'
import {useSession, signIn} from 'next-auth/react'

// Bootstrap
import Button from 'react-bootstrap/Button'

// Global Fractal Components
import MonoNav from 'components/MonoNav/MonoNav'
import MonoMobileNav from 'components/MonoNav/MonoMobileNav'
import CardXXl from 'components/Card/CardXXl'
import InstallAlert from 'components/Alerts/InstallAlert'
import PictureLg from 'components/Picture/PictureLg'

// Presentation
import Layout from 'components/Layout/Layout'
import LayoutDashboard from 'components/Layout/LayoutDashboard'
import LayoutCenter from 'components/Layout/LayoutCenter'
import CenterList from 'components/Layout/Center/CenterList'

// Local Fractal Components
import Carousel from '../../components/Carousel/Carousel'

// Photoswipe Gallery
import {Gallery, Item} from 'react-photoswipe-gallery'
import useSWR from 'swr'

// SSR IMPORTS
import {GetServerSideProps} from 'next'
import {useIsAppInstalledSSR} from 'hooks/useIsAppInstalledSSR'

const APP_UUID = process.env.NEXT_PUBLIC_BITWARDEN_APP_UUID

interface IsInstalled {
	isInstalled: boolean
}

const AppPage: NextPage<IsInstalled> = ({isInstalled}) => {
	const testUser = '78877f78-f15d-46af-be55-0e8f8b0196b0'

	const {data: session, status: loading} = useSession()

	const fetcher = (arg: string) => fetch(arg).then((res) => res.json())

	const {data, error} = useSWR(`${process.env.NEXT_PUBLIC_MOSAIC_API}catalog/app/${APP_UUID}/`, fetcher)

	const bitWardenLinks = [
		{
			linkName: 'Homepage',
			linkUrl: 'https://bitwarden.com/',
		},
		{
			linkName: 'Help',
			linkUrl: 'https://bitwarden.com/help/',
		},
		{
			linkName: 'Github',
			linkUrl: 'https://github.com/bitwarden/server',
		},
	]

	const bitWardenCategories = [
		{
			category: 'Security',
		},
	]

	const carouselApps = [
		{
			appName: 'WikiJs',
		},
		{
			appName: 'NextCloud',
		},
	]

	const screenshots = [
		{
			src: '/test-img/bitwarden-screenshot-1.png',
			width: '2968',
			height: '1670',
		},
	]

	return (
		<Layout>
			<MonoNav uuid={testUser} useSession={useSession} currentDomain='Store' />
			<LayoutDashboard>
				<LayoutCenter>
					<CenterList>
						<CardXXl
							isInstalled={isInstalled}
							uuid={APP_UUID}
							useSession={useSession}
							title={'BitWarden'}
							subtitle={'The password manager trusted by millions'}
							description={
								'Move fast and securely with the password manager trusted by millions. Drive collaboration, boost productivity, and experience the power of open source with Bitwarden, the easiest way to secure all your passwords and sensitive information.'
							}
							githubStars={data && data.cached_github_stars ? data.cached_github_stars : 10.0}
							githubUrl={'https://github.com/bitwarden/server'}
							logo={`${process.env.NEXT_PUBLIC_API_BASE_URL}/media/app_catalog/logo_icon_bitwarden.svg`}
							img={'/test-img/bitwardenHero.png'}
							categories={bitWardenCategories}
							links={bitWardenLinks}
						/>
						{loading == 'unauthenticated' && <InstallAlert title='BitWarden' />}
						<div>
							<h2 className='mt-3 mb-4'>Everything you need out of a password manager</h2>

							<h3 className='m-0'>Easy</h3>
							<h4>Powerful security within minutes</h4>
							<PictureLg img='/test-img/bitwarden_icon-easy.svg' circle={false} />
							<p className='mt-1 mb-5'>
								For those who want to do more, secure more, and collaborate more, Bitwarden is fast and easy to set up
								for both individuals and businesses.
							</p>

							<h3 className='m-0'>Convenient</h3>
							<h4>Unlimited passwords, unlimited devices</h4>
							<PictureLg img='/test-img/bitwarden_icon-convenient.svg' circle={false} />
							<p className='mt-1 mb-5'>
								Cross platform access for mobile, browser, and desktop apps. Supported in over 40 languages.
							</p>

							<h3 className='m-0'>Secure</h3>
							<h4>Protect what's important to you</h4>
							<PictureLg img='/test-img/bitwarden_icon-secure.svg' circle={false} />
							<p className='mt-1'>
								Zero knowledge, end-to-end encryption guides the Bitwarden open source approach to trust,
								accountability, and security.
							</p>
						</div>
						<h3>Screenshots</h3>
						<Gallery>
							{screenshots.map((screenshot, index) => {
								return (
									<Item
										key={index}
										original={screenshot.src}
										thumbnail={screenshot.src}
										width={screenshot.width}
										height={screenshot.height}>
										{/* @ts-ignore */}
										{({ref, open}) => <img ref={ref} onClick={open} src={screenshot.src} style={{width: '100%'}} />}
									</Item>
								)
							})}
						</Gallery>
					</CenterList>
					{/* <Carousel title={'Similar apps'} items={carouselApps} /> */}
				</LayoutCenter>
			</LayoutDashboard>
			<MonoMobileNav currentDomain='Store' />
		</Layout>
	)
}

export const getServerSideProps: GetServerSideProps = async (context) => {
	return useIsAppInstalledSSR(context, APP_UUID as string)
}

export default AppPage
