// Next Js
import type {NextPage} from 'next'
import {useSession, signIn} from 'next-auth/react'

// Bootstrap
import Button from 'react-bootstrap/Button'

// Global Fractal Components
import MonoNav from 'components/MonoNav/MonoNav'
import MonoMobileNav from 'components/MonoNav/MonoMobileNav'
import CardXXl from 'components/Card/CardXXl'
import InstallAlert from 'components/Alerts/InstallAlert'
import PictureLg from 'components/Picture/PictureLg'

// Presentation
import Layout from 'components/Layout/Layout'
import LayoutDashboard from 'components/Layout/LayoutDashboard'
import LayoutCenter from 'components/Layout/LayoutCenter'
import CenterList from 'components/Layout/Center/CenterList'

// Local Fractal Components
import Carousel from '../../components/Carousel/Carousel'

// Photoswipe Gallery
import {Gallery, Item} from 'react-photoswipe-gallery'

import useSWR from 'swr'

// SSR IMPORTS
import {GetServerSideProps} from 'next'
import {useIsAppInstalledSSR} from 'hooks/useIsAppInstalledSSR'

const APP_UUID = process.env.NEXT_PUBLIC_NEXTCLOUD_APP_UUID

interface IsInstalled {
	isInstalled: boolean
}

const AppPage: NextPage<IsInstalled> = ({isInstalled}) => {
	const testUser = '78877f78-f15d-46af-be55-0e8f8b0196b0'

	const {data: session, status: loading} = useSession()

	const fetcher = (arg: string) => fetch(arg).then((res) => res.json())

	const {data, error} = useSWR(`${process.env.NEXT_PUBLIC_MOSAIC_API}catalog/app/${APP_UUID}/`, fetcher)

	const nextCloudLinks = [
		{
			linkName: 'Homepage',
			linkUrl: 'https://nextcloud.com/',
		},
		{
			linkName: 'Community Support',
			linkUrl: 'https://help.nextcloud.com/',
		},
		{
			linkName: 'Github',
			linkUrl: 'https://github.com/nextcloud/server',
		},
	]

	const nextCloudCategories = [
		{
			category: 'File Sharing',
		},
	]

	const carouselApps = [
		{
			appName: 'WikiJs',
		},
		{
			appName: 'BitWarden',
		},
	]

	const screenshots = [
		{
			src: '/test-img/nextcloud-screenshot-1.png',
			width: '1600',
			height: '899',
		},
		{
			src: '/test-img/nextcloud-screenshot-2.png',
			width: '2560',
			height: '1440',
		},
		{
			src: '/test-img/nextcloud-screenshot-3.png',
			width: '2560',
			height: '1440',
		},
		{
			src: '/test-img/nextcloud-screenshot-4.png',
			width: '2560',
			height: '1440',
		},
		{
			src: '/test-img/nextcloud-screenshot-5.jpg',
			width: '1920',
			height: '1080',
		},
		{
			src: '/test-img/nextcloud-screenshot-6.png',
			width: '1920',
			height: '1080',
		},
	]

	return (
		<Layout>
			<MonoNav uuid={testUser} useSession={useSession} currentDomain='Store' />
			<LayoutDashboard>
				<LayoutCenter>
					<CenterList>
						<CardXXl
							isInstalled={isInstalled}
							uuid={APP_UUID}
							useSession={useSession}
							title={'NextCloud'}
							subtitle={'A safe home for all your data.'}
							description={
								'Nextcloud is a suite of client-server software for creating and using file hosting services.'
							}
							githubStars={data && data.cached_github_stars ? data.cached_github_stars : 19.5}
							githubUrl={'https://github.com/nextcloud/server'}
							logo={`${process.env.NEXT_PUBLIC_API_BASE_URL}/media/app_catalog/logo_icon_nextcloud.png`}
							img={'/test-img/NextcloudHero.png'}
							categories={nextCloudCategories}
							links={nextCloudLinks}
						/>
						{loading == 'unauthenticated' && <InstallAlert title='NextCloud' />}
						<div>
							<h2 className='mt-4 mb-1'>Powering collaboration</h2>
							<p className='mb-5'>
								The most popular self-hosted collaboration solution for tens of millions of users at thousands of
								organizations across the globe
							</p>

							<PictureLg img='/test-img/nextcloud_icon-productivity.svg' circle={false} />
							<h3 className='mt-2 mb-1'>Productivity</h3>
							<p className='mb-5'>
								Enable productivity across any platform, whether in the office or on the road, to share, collaborate and
								communicate across organizational boundaries. Nextcloud provides transparent access to data on any
								storage.
							</p>

							<PictureLg img='/test-img/nextcloud_icon-control.svg' circle={false} />
							<h3 className='mt-2 mb-1'>Control</h3>
							<p className='mb-5'>
								Protect, control and monitor data and communication across your company. Guarantee compliance with
								business and legal requirements. Keep your data on servers you own, at all times. Nothing leaks, not
								even metadata.
							</p>

							<PictureLg img='/test-img/nextcloud_icon-community.svg' circle={false} />
							<h3 className='mt-2 mb-1'>Community</h3>
							<p className='mb-3'>
								Enjoy constant improvements from a thriving and transparent, entirely open-source community development
								model, free of lockins or paywalls. Enjoy the benefits of enterprise support when you need it.
							</p>
						</div>
						<h3>Screenshots</h3>
						<Gallery>
							{screenshots.map((screenshot, index) => {
								return (
									<Item
										key={index}
										original={screenshot.src}
										thumbnail={screenshot.src}
										width={screenshot.width}
										height={screenshot.height}>
										{/* @ts-ignore */}
										{({ref, open}) => <img ref={ref} onClick={open} src={screenshot.src} style={{width: '100%'}} />}
									</Item>
								)
							})}
						</Gallery>
					</CenterList>
					{/* <Carousel title={'Similar apps'} items={carouselApps} /> */}
				</LayoutCenter>
			</LayoutDashboard>
			<MonoMobileNav currentDomain='Store' />
		</Layout>
	)
}

export const getServerSideProps: GetServerSideProps = async (context) => {
	return useIsAppInstalledSSR(context, APP_UUID as string)
}

export default AppPage
