// Next Js
import type {NextPage} from 'next'
import {useSession, signIn} from 'next-auth/react'

// Bootstrap
import Button from 'react-bootstrap/Button'

// Global Fractal Components
import MonoNav from 'components/MonoNav/MonoNav'
import MonoMobileNav from 'components/MonoNav/MonoMobileNav'
import CardXXl from 'components/Card/CardXXl'
import InstallAlert from 'components/Alerts/InstallAlert'
import PictureAppDetailIcon from 'components/Picture/PictureAppDetailIcon'

// Presentation
import Layout from 'components/Layout/Layout'
import LayoutDashboard from 'components/Layout/LayoutDashboard'
import LayoutCenter from 'components/Layout/LayoutCenter'
import CenterList from 'components/Layout/Center/CenterList'

// Photoswipe Gallery
import {Gallery, Item} from 'react-photoswipe-gallery'
import useSWR from 'swr'

// SSR IMPORTS
import {GetServerSideProps} from 'next'
import {useIsAppInstalledSSR} from 'hooks/useIsAppInstalledSSR'

const APP_UUID = process.env.NEXT_PUBLIC_PHOTOPRISM_APP_UUID

interface IsInstalled {
	isInstalled: boolean
}

const AppPage: NextPage<IsInstalled> = ({isInstalled}) => {
	const {data: session, status: loading} = useSession()

	const fetcher = (arg: string) => fetch(arg).then((res) => res.json())

	const {data, error} = useSWR(`${process.env.NEXT_PUBLIC_MOSAIC_API}catalog/app/${APP_UUID}/`, fetcher)

	const photoprismLinks = [
		{
			linkName: 'Homepage',
			linkUrl: 'https://photoprism.app/',
		},
		{
			linkName: 'Contact',
			linkUrl: 'https://photoprism.app/contact',
		},
		{
			linkName: 'Github',
			linkUrl: 'https://github.com/photoprism/photoprism',
		},
	]

	const photoprismCategories = [
		{
			category: 'Media',
		},
	]

	const screenshots = [
		{
			src: '/test-img/element-screenshot-2.png',
			width: '888',
			height: '531',
		},
		{
			src: '/test-img/element-screenshot-1.png',
			width: '350',
			height: '625',
		},
	]

	return (
		<Layout>
			{console.log('DATA: ', data)}
			<MonoNav useSession={useSession} currentDomain='Store' />
			<LayoutDashboard>
				<LayoutCenter>
					<CenterList>
						<CardXXl
							isInstalled={isInstalled}
							uuid={APP_UUID}
							useSession={useSession}
							title={'Photoprism'}
							subtitle={'User- and privacy-friendly solution to keep your pictures organized and accessible.'}
							description={
								'PhotoPrism is an AI-Powered Photos App for the Decentralized Web. It makes use of the latest technologies to tag and find pictures automatically without getting in your way. You can run it at home, on a private server, or in the cloud.'
							}
							githubStars={data && data.cached_github_stars ? data.cached_github_stars : 10.0}
							githubUrl={data && data.github_url ? data.github_url : 'https://github.com/photoprism/photoprism'}
							logo={`${process.env.NEXT_PUBLIC_API_BASE_URL}/media/app_catalog/logo_icon_photoprism.png`}
							img={'/test-img/photoprismHero.png'}
							categories={photoprismCategories}
							links={photoprismLinks}
						/>
						{loading == 'unauthenticated' && <InstallAlert title='Element' />}
						<div>
							<div className='mb-5'>
								<h3 className='m-0'>Runs Everywhere.</h3>
								<h4>
									Enjoy browsing your photo collection with our intuitive Progressive Web App — whether it’s on a phone,
									tablet, or desktop computer. It provides a native app-like experience, and you can conveniently
									install it on the home screen of all major operating systems and mobile devices.
								</h4>
								<PictureAppDetailIcon img='/test-img/photoprism_runs_everywhere.png' width='35' />
							</div>

							<div className='mb-5'>
								<h3 className='m-0'>Powerful Search.</h3>
								<h4>
									Easily find specific photos and videos using powerful search filters. Your pictures are automatically
									classified based on their content and location. Many more image properties like colors, chroma, and
									quality can be searched as well. Pictures marked as private, archived, or under review do not appear
									in regular search results..
								</h4>
							</div>

							<div className='mb-5'>
								<h3 className='m-0'>Facial Recognition.</h3>
								<h4>
									Easily find specific photos and videos using powerful search filters. Your pictures are automatically
									classified based on their content and location. Many more image properties like colors, chroma, and
									quality can be searched as well. Pictures marked as private, archived, or under review do not appear
									in regular search results.
								</h4>
							</div>

							<div className='mb-5'>
								<h3 className='m-0'>Maps & Places.</h3>
								<h4>
									The app includes four high-resolution world maps to bring back the memories of your favorite trips.
									Our privacy-preserving backend services provide worldwide location information to enrich your pictures
									with details such as state, city, and category.
								</h4>
								<PictureAppDetailIcon img='/test-img/photoprism_maps.png' width='35' />
							</div>

							<div className='mb-5'>
								<h3 className='m-0'>Album Sharing.</h3>
								<h4>
									Secret links make it easy to share albums with your loved ones. You can create multiple links for each
									album and optionally set an expiration date. No additional apps need to be installed and no
									registration is required.
								</h4>
							</div>

							<div className='mb-5'>
								<h3 className='m-0'>Advanced Metadata Extraction.</h3>
								<h4>
									Original media and sidecar files are scanned for Exif and XMP data as well as proprietary metadata,
									including Google Photos JSON. The combined information is normalized and merged.
								</h4>
								<PictureAppDetailIcon img='/test-img/photoprism_metadata.png' width='35' />
							</div>
						</div>
					</CenterList>
				</LayoutCenter>
			</LayoutDashboard>
			<MonoMobileNav currentDomain='Store' />
		</Layout>
	)
}

export const getServerSideProps: GetServerSideProps = async (context) => {
	return useIsAppInstalledSSR(context, APP_UUID as string)
}

export default AppPage
