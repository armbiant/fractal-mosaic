// Next Js
import type {NextPage} from 'next'
import {useSession, signIn} from 'next-auth/react'

// Bootstrap
import Button from 'react-bootstrap/Button'

// Global Fractal Components
import MonoNav from 'components/MonoNav/MonoNav'
import MonoMobileNav from 'components/MonoNav/MonoMobileNav'
import CardXXl from 'components/Card/CardXXl'
import InstallAlert from 'components/Alerts/InstallAlert'
import PictureAppDetailIcon from 'components/Picture/PictureAppDetailIcon'

// Presentation
import Layout from 'components/Layout/Layout'
import LayoutDashboard from 'components/Layout/LayoutDashboard'
import LayoutCenter from 'components/Layout/LayoutCenter'
import CenterList from 'components/Layout/Center/CenterList'

// Local Fractal Components
import Carousel from '../../components/Carousel/Carousel'

// Photoswipe Gallery
import {Gallery, Item} from 'react-photoswipe-gallery'
import useSWR from 'swr'

// SSR IMPORTS
import {GetServerSideProps} from 'next'
import {useIsAppInstalledSSR} from 'hooks/useIsAppInstalledSSR'

const APP_UUID = process.env.NEXT_PUBLIC_ELEMENT_APP_UUID

interface IsInstalled {
	isInstalled: boolean
}

const AppPage: NextPage<IsInstalled> = ({isInstalled}) => {
	const {data: session, status: loading} = useSession()

	const fetcher = (arg: string) => fetch(arg).then((res) => res.json())

	const {data, error} = useSWR(`${process.env.NEXT_PUBLIC_MOSAIC_API}catalog/app/${APP_UUID}/`, fetcher)

	const elementLinks = [
		{
			linkName: 'Homepage',
			linkUrl: 'https://element.io/',
		},
		{
			linkName: 'Help',
			linkUrl: 'https://element.io/help',
		},
		{
			linkName: 'Github',
			linkUrl: 'https://github.com/vector-im/element-web',
		},
	]

	const elementCategories = [
		{
			category: 'Collaboration',
		},
	]

	const carouselApps = [
		{
			appName: 'WikiJs',
		},
		{
			appName: 'NextCloud',
		},
	]

	const screenshots = [
		{
			src: '/test-img/element-screenshot-2.png',
			width: '888',
			height: '531',
		},
		{
			src: '/test-img/element-screenshot-1.png',
			width: '350',
			height: '625',
		},
	]

	return (
		<Layout>
			{console.log('DATA: ', data)}
			<MonoNav useSession={useSession} currentDomain='Store' />
			<LayoutDashboard>
				<LayoutCenter>
					<CenterList>
						<CardXXl
							isInstalled={isInstalled}
							uuid={APP_UUID}
							useSession={useSession}
							title={'Element'}
							subtitle={'Own your conversations.'}
							description={
								'Secure and independent communication, connected via Matrix.  Free end-to-end encrypted messaging, with unlimited voice and video.'
							}
							githubStars={data && data.cached_github_stars ? data.cached_github_stars : 10.0}
							githubUrl={data && data.github_url ? data.github_url : 'https://github.com/vector-im/element-web'}
							logo={`${process.env.NEXT_PUBLIC_API_BASE_URL}/media/app_catalog/logo_icon_element.svg`}
							img={'/test-img/elementHero.png'}
							categories={elementCategories}
							links={elementLinks}
						/>
						{loading == 'unauthenticated' && <InstallAlert title='Element' />}
						<div>
							<h2 className='mt-3 mb-4'>Own your conversations.</h2>

							<div className='mb-5'>
								<h3 className='m-0'>Personal Messaging.</h3>
								<h4>
									Free end-to-end encrypted messaging, with unlimited voice and video. Choose where your messages are
									stored, keeping you in control of your data.
								</h4>
								<PictureAppDetailIcon img='/test-img/element_personal_messaging.png' width='10' />
							</div>

							<div className='mb-5'>
								<h3 className='m-0'>Enterprise collaboration.</h3>
								<h4>
									Secure and end-to-end encrypted. Keep ownership and control of your data, on-premise or hosted by us.
									Interoperable for easy connectivity.
								</h4>
								<PictureAppDetailIcon img='/test-img/enterpriseCollabElement.png' width='20' />
							</div>

							<div className='mb-5'>
								<h3 className='m-0'>Vibrant communities.</h3>
								<h4>Safe, inclusive and open with plenty of room for everyone.</h4>
								<PictureAppDetailIcon img='/test-img/element_vibrant_communities.png' width='10' />
							</div>

							<div className='mb-5'>
								<h3 className='m-0'>Matrix Services.</h3>
								<h4>Infrastructure you can trust. Add Matrix VoIP and chat to your app or website.</h4>
								<PictureAppDetailIcon img='/test-img/element_matrix_services.png' width='16' />
							</div>
						</div>
						<h3>Screenshots</h3>
						<Gallery>
							{screenshots.map((screenshot, index) => {
								return (
									<Item
										key={index}
										original={screenshot.src}
										thumbnail={screenshot.src}
										width={screenshot.width}
										height={screenshot.height}>
										{/* @ts-ignore */}
										{({ref, open}) => <img ref={ref} onClick={open} src={screenshot.src} style={{width: '100%'}} />}
									</Item>
								)
							})}
						</Gallery>
					</CenterList>
					{/* <Carousel title={'Similar apps'} items={carouselApps} /> */}
				</LayoutCenter>
			</LayoutDashboard>
			<MonoMobileNav currentDomain='Store' />
		</Layout>
	)
}

export const getServerSideProps: GetServerSideProps = async (context) => {
	return useIsAppInstalledSSR(context, APP_UUID as string)
}

export default AppPage
