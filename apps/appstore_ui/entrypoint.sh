#!/bin/sh
cd apps/appstore_ui
if [ "$NODE_ENV" == "dev" ]; then
	npm install --verbose
	npm run dev
else
	npm run build
	npm run start
fi
