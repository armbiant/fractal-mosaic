// Recoil
import {atom} from 'recoil'

export const alphabeticalState = atom<string>({
	key: 'alphabeticalState',
	default: '',
})
