// Recoil
import {atom} from 'recoil'

export const categoriesState = atom<string[]>({
	key: 'categoriesState',
	default: [],
})
