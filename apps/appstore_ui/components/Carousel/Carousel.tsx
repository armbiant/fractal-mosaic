import BSCarousel from 'react-bootstrap/Carousel'
import CardXl from 'components/Card/CardXl'

interface CarouselProps {
	title: string
	items: {
		appName: string
	}[]
}

const Carousel = ({title, items}: CarouselProps) => {
	return (
		<div className='bg-white py-3'>
			<h1 className='text-center mb-3'>{title}</h1>
			<div className='d-flex align-items-center justify-content-center gap-3'>
				{items.map((app) => {
					return <CardXl title={app.appName} uuid={app.appName} />
				})}
			</div>
		</div>
	)
}

export default Carousel
